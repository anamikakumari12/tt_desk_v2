CREATE TABLE tt_SPL_Quotes
(
ID INT IDENTITY(1,1),
Ref_ID INT,
Ref_number VARCHAR(100),
Item_Type VARCHAR(100),
Item_Code VARCHAR(100),
Item_Desc VARCHAR(MAX),
Cust_Type VARCHAR(10),
Customer_number VARCHAR(100),
CPCustomer_number VARCHAR(100),
Category_ID VARCHAR(10),
Qty INT,
Offer_Price Float,
Order_validity INT,
Item_File VARCHAR(MAX),
Common_File VARCHAR(MAX),
RequestedBy VARCHAR(10),
RequestedBy_Flag VARCHAR(100),
Requested_Date Datetime,
Quotation_number VARCHAR(100)
)

CREATE TABLE tt_SPL_Items
(
ID INT IDENTITY(1,1),
Item_Code VARCHAR(100),
Item_Desc VARCHAR(MAX),
Category_ID VARCHAR(100)
)
CREATE TABLE tt_SPL_QuoteStatusLog 
(
ID	int IDENTITY(1,1),
Quote_ID	int,
Ref_Number	varchar(100),
Item_code	varchar(100),
Status	varchar(100),
StatusChangeBy_Flag	varchar(100),
StatusChangeBy_Id	varchar(100),
StatusChangeDate	datetime,
StatusChange_Comment	varchar(MAX),
Price_added	varchar(100),
End_cust_num	varchar(100),
End_cust_name	varchar(MAX),
Qty_added	varchar(100),
Internal_Remarks	varchar(MAX)
)


CREATE TYPE [dbo].[SplQuoteTableType] AS TABLE(
ID INT null,
Ref_number VARCHAR(100) null,
Item_Type VARCHAR(100) null,
Item_Code VARCHAR(100) null,
Item_Desc VARCHAR(MAX) null,
Cust_Type VARCHAR(10) null,
Customer_number VARCHAR(100) null,
CPCustomer_number VARCHAR(100) null,
Category_ID VARCHAR(10) null,
Qty INT null,
Offer_Price Float null,
Order_validity INT null,
Item_File VARCHAR(MAX) null,
Common_File VARCHAR(MAX) null,
RequestedBy VARCHAR(10) null,
RequestedBy_Flag VARCHAR(100) null,
Requested_Date Datetime null,
Quotation_number VARCHAR(100) null
)
GO



CREATE SEQUENCE [dbo].[sq_SPL_Quotes] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 CACHE 
GO



CREATE Procedure [dbo].[GetMaxQuoteId]
(
@quote_id INT OUTPUT
)
AS
BEGIN 
	Select @quote_id= NEXT VALUE FOR [dbo].[sq_SPL_Quotes]
END


