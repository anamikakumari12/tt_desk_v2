USE [Taegutec_Sales_Budget]
GO
/****** Object:  StoredProcedure [dbo].[sp_GET_SPL_PaymentTerms]    Script Date: 10-06-2021 15:11:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROC [dbo].[sp_GET_SPL_PaymentTerms]
(
@CustNumber VARCHAR(100),
@SourceType VARCHAR(100)
)
AS
BEGIN
IF(@SourceType='GAL')
BEGIN
	Select T1KEY No_Of_Days, T1DSC Payment_Terms from tt_Customer_paymentTerms cp JOIN tt_customer_master cm ON cp.T1KEY=cm.CreditDays
	Where cm.customer_number=@CustNumber
END
ELSE
BEGIN
	Select Payment_Terms, No_Of_Days from tt_SPL_PaymentTerms
END
END


Select * from tt_customer_master where customer_name like '%AEIP%'

exec sp_GET_SPL_PaymentTerms '1068','GAL'