Create Table tbl_special_catagories(
ID int identity(1,1),
Unique_Id INT,
Cat_ID INT,
Cat_Name VARCHAR(MAX),
Cat_Value VARCHAR(MAX),
Ref_Id INT,
Status INT)

INSERT INTO tbl_special_catagories (Cat_ID,Unique_Id,Cat_Name,Cat_Value,Ref_Id,Status)
VALUES
 (1,1,'Family','T CLAMP SPL INSERT',0,1)
,(2,1,'Family','TGUX',0,1)
,(3,1,'Family','TTG',0,1)
,(4,1,'Family','QR',0,1)


INSERT INTO tbl_special_catagories (Cat_ID,Cat_Name,Cat_Value,Ref_Id,Status)
VALUES
 (5,'Sub-Category','PROFILE GRINDING E CODE',1,1)
,(6,'Sub-Category','WIRECUT W CODE',1,1)

INSERT INTO tbl_special_catagories (Cat_ID,Cat_Name,Cat_Value,Ref_Id,Status)
VALUES
 (7,'Code','EA',5,1)
, (8,'Code','EB',5,1)
, (9,'Code','EC',5,1)
, (10,'Code','WA',6,1)
, (11,'Code','WB',6,1)
, (12,'Code','WC',6,1)
, (13,'Code','WD',6,1)


INSERT INTO tbl_special_catagories (Cat_ID,Cat_Name,Cat_Value,Ref_Id,Status)
VALUES
 (14,'BLANK','B TDBG 2.6',5,1)         
,(15,'BLANK','B TDBT 3.4',5,1)         
,(16,'BLANK','B TDBG 4.4',5,1)         
,(17,'BLANK','B TDBT 5.4',5,1)         
,(18,'BLANK','B TDBT 6.4',5,1)         
,(19,'BLANK','B TDBT 8.5',5,1)   

INSERT INTO tbl_special_catagories (Cat_ID,Cat_Name,Cat_Value,Ref_Id,Status)
VALUES
 (20,'QTY','OFFER FOR 30 PCS',5,1)	
,(21,'QTY','OFFER FOR 50 PCS',5,1)	
,(22,'QTY','OFFER FOR 100 PCS',5,1)


INSERT INTO tbl_special_catagories (Cat_ID,Cat_Name,Cat_Value,Ref_Id,Status)
VALUES
 (23,'BLANK','B TDBG 2.6',6,1)         
,(24,'BLANK','B TDBT 3.4',6,1)         
,(25,'BLANK','B TDBG 4.4',6,1)         
,(26,'BLANK','B TDBT 5.4',6,1)         
,(27,'BLANK','B TDBT 6.4',6,1)         
,(28,'BLANK','B TDBT 8.5',6,1)   
,(29,'QTY','OFFER FOR 30 PCS',6,1)	
,(30,'QTY','OFFER FOR 50 PCS',6,1)	
,(31,'QTY','OFFER FOR 100 PCS',6,1)



INSERT INTO tbl_special_catagories (Cat_ID,Cat_Name,Cat_Value,Ref_Id,Status)
VALUES
 (32,'Sub-Category','CAT',2,1)
 INSERT INTO tbl_special_catagories (Cat_ID,Cat_Name,Cat_Value,Ref_Id,Status)
VALUES
  (32,'CAT','8490012',2,1)
 ,(33,'CAT','8490077',2,1)
 ,(34,'CAT','8490079',2,1)
 ,(35,'CAT','8490020',2,1)
 ,(36,'CAT','8490101',2,1)
 ,(37,'CAT','8490018',2,1)
 ,(38,'CAT','8490104',2,1)
 ,(39,'CAT','8490024',2,1)
 
  INSERT INTO tbl_special_catagories (Cat_ID,Cat_Name,Cat_Value,Ref_Id,Status)
VALUES
 (40,'BLANK','TTI BG TGUX 1004     P40A',2,1)
,(41,'BLANK','TTI BG TGUX 1004    UF100',2,1)
,(42,'BLANK','TTI BG TGUX 1504    P40A',2,1)
,(43,'BLANK','TTI BG TGUX 1504    UF100',2,1)
,(44,'BLANK','TTI BG TGUX 2006     P40A',2,1)
,(45,'BLANK','TTI BG TGUX 2006    UF100',2,1)
,(46,'BLANK','TTI BG TGUX 2506     P40A',2,1)
,(47,'BLANK','TTI BG TGUX 2506    UF100',2,1)
,(48,'BLANK','B FTB 3010           IC08',2,1)


  INSERT INTO tbl_special_catagories (Cat_ID,Cat_Name,Cat_Value,Ref_Id,Status)
VALUES
 (49,'BASE','P40A',2,1)
,(50,'BASE','OTHER GRADE',2,1)

INSERT INTO tbl_special_catagories (Cat_ID,Cat_Name,Cat_Value,Ref_Id,Status)
VALUES
 (51,'MOQ','20',40,1)
,(52,'MOQ','30',40,1)
,(53,'MOQ','50',40,1) 
,(54,'MOQ','20',41,1)
,(55,'MOQ','30',41,1)
,(56,'MOQ','50',41,1) 
,(57,'MOQ','20',42,1)
,(58,'MOQ','30',42,1)
,(59,'MOQ','50',42,1) 
,(60,'MOQ','20',43,1)
,(61,'MOQ','30',43,1)
,(62,'MOQ','50',43,1)
,(63,'MOQ','15',44,1)
,(64,'MOQ','30',44,1)
,(65,'MOQ','50',44,1)
,(66,'MOQ','15',45,1)
,(67,'MOQ','30',45,1)
,(68,'MOQ','50',45,1)
,(69,'MOQ','10',46,1)
,(70,'MOQ','30',46,1)
,(71,'MOQ','50',46,1)
,(72,'MOQ','10',47,1)
,(73,'MOQ','30',47,1)
,(74,'MOQ','50',47,1)
,(75,'MOQ','5',48,1)
,(76,'MOQ','20',48,1)




INSERT INTO tbl_special_catagories (Cat_ID,Cat_Name,Cat_Value,Ref_Id,Status)
VALUES
 (78,'SIZE','TTG 32',3,1)         
,(79,'SIZE','TTG 43',3,1)         
,(80,'BLANK','B TTBG 32',3,1)         
,(81,'BLANK','B TTBG 43',3,1)      
,(82,'QTY','OFFER FOR 30 PCS',3,1)	
,(83,'QTY','OFFER FOR 50 PCS',3,1)	
,(84,'QTY','OFFER FOR 100 PCS',3,1)


INSERT INTO tbl_special_catagories (Cat_ID,Cat_Name,Cat_Value,Ref_Id,Status)
VALUES
 (85,'QR SPL','QR SPL FOR 64 Q Blank has to be TQS',4,1)         
,(86,'QR SPL','QR SPL FOR 20 SIZE',4,1)         
,(87,'BLANK','BG TQBS 27-3.4     UF100',4,1)         
,(88,'BLANK','BG TQBS 20-4.0     UF100',4,1)      
,(89,'MOQ','30',4,1)	
,(90,'MOQ','50',4,1)	





UPDATE tbl_special_catagories SET Unique_Id=2 where Cat_Name='Sub-Category'
UPDATE tbl_special_catagories SET Unique_Id=3 where Cat_Name='Code'
UPDATE tbl_special_catagories SET Unique_Id=4 where Cat_Name='CAT'
UPDATE tbl_special_catagories SET Unique_Id=5 where Cat_Name='SIZE'
UPDATE tbl_special_catagories SET Unique_Id=6 where Cat_Name='QR SPL'
UPDATE tbl_special_catagories SET Unique_Id=7 where Cat_Name='BLANK'
UPDATE tbl_special_catagories SET Unique_Id=8 where Cat_Name='BASE'
UPDATE tbl_special_catagories SET Unique_Id=9 where Cat_Name='QTY'
UPDATE tbl_special_catagories SET Unique_Id=10 where Cat_Name='MOQ'
Select * from tbl_special_catagories where Unique_Id is null