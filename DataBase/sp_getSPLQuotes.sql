USE [Taegutec_Sales_Budget]
GO
/****** Object:  StoredProcedure [dbo].[sp_getSPLQuotes]    Script Date: 10-06-2021 14:44:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [dbo].[sp_getSPLQuotes]
(
@StartDate VARCHAR(100)=null,
@EndDate VARCHAR(100)=null,
@custtype VARCHAR(10),
@CustNumber VARCHAR(100)=NULL
)
AS
BEGIN

if(@StartDate is null OR @StartDate='')
SET @StartDate=null
else
SET @StartDate=Cast(@StartDate as datetime)

Print @StartDate

if(@EndDate is null OR @EndDate='')
SET @EndDate=null
else
SET @EndDate=Cast(@EndDate as datetime)

Select Ref_ID, Ref_number, Cust_Type, (Select Customer_name+'('+ Customer_number+')' from tt_customer_master where Customer_number= sq.Customer_number) CustCP_Name, CPCustomer_Name+'('+CPCustomer_number+')' CPCustomer_Name, Common_File, Requested_Date
, Ref_number+'.pdf' Report_File
,SUM(QTY*Offer_Price) Order_Value
,CASE WHEN DATEADD(DD,sq.Order_validity,sq.Requested_Date)<GETDATE() THEN 'Expired' ELSE 'Pending For Order' END Status
from tt_SPL_Quotes sq
WHERE Cust_Type=ISNULL(@custtype,Cust_Type) AND Customer_number=ISNULL(@CustNumber,Customer_number) 
			AND CONVERT(DATE,Requested_date) >=ISNULL(@StartDate,Requested_date) 
			AND CONVERT(DATE,Requested_date)<= ISNULL(@EndDate,Requested_date)
GROUP BY Ref_ID, Ref_number, Cust_Type, Customer_number, CPCustomer_number, CPCustomer_Name, Common_File, Requested_Date, Order_validity

END
