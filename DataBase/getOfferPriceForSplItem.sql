USE [Taegutec_Sales_Budget]
GO
/****** Object:  StoredProcedure [dbo].[getOfferPriceForSplItem]    Script Date: 21-05-2021 11:11:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[getOfferPriceForSplItem]
(
@Value1 VARCHAR(MAX)=null,
@Value2 VARCHAR(MAX)=null,
@Value3 VARCHAR(MAX)=null,
@Value4 VARCHAR(MAX)=null,
@Value5 VARCHAR(MAX)=null,
@Value6 VARCHAR(MAX)=null,
@Value7 VARCHAR(MAX)=null,
@Value8 VARCHAR(MAX)=null,
@Value9 VARCHAR(MAX)=null,
@Value10 VARCHAR(MAX)=null
)
AS
BEGIN
DECLARE @QUERY VARCHAR(MAX)

SET @QUERY='
SELECT ID, QTY, OFFER_PRICE FROM tt_SPL_Categories WHERE Value1='''+@Value1+''' AND '
IF(@Value2 is not null AND @Value2<>'')
SET @QUERY+=' Value2='''+@Value2+''' AND '
IF(@Value3 is not null AND @Value3<>'')
SET @QUERY+=' Value3='''+@Value3+''' AND '
IF(@Value4 is not null AND @Value4<>'')
SET @QUERY+=' Value4='''+@Value4+''' AND '
IF(@Value5 is not null AND @Value5<>'')
SET @QUERY+=' Value5='''+@Value5+''' AND '
IF(@Value6 is not null AND @Value6<>'')
SET @QUERY+=' Value6='''+@Value6+''' AND '
IF(@Value7 is not null AND @Value7<>'')
SET @QUERY+=' Value7='''+@Value7+''' AND '
IF(@Value8 is not null AND @Value8<>'')
SET @QUERY+=' Value8='''+@Value8+''' AND '
IF(@Value9 is not null AND @Value9<>'')
SET @QUERY+=' Value9='''+@Value9+''' AND '
IF(@Value10 is not null AND @Value10<>'')
SET @QUERY+=' Value10='''+@Value10+''' AND '
SET @QUERY+='1=1'

PRINT(@QUERY)
EXEC(@QUERY)
END




