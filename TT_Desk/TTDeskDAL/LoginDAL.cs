﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using TTDeskBO;

namespace TTDeskDAL
{
    public class LoginDAL
    {
        CommonFunctions objCom = new CommonFunctions();
        
        public LoginBO deskLoginDAL(LoginBO objLoginBO)
        {
            LoginBO objOutPut = new LoginBO();
            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection con = new SqlConnection(connstring);
            SqlDataAdapter sqlda;
            DataTable dtoutput = new DataTable();
            try
            {
                con.Open();
                SqlCommand command = new SqlCommand(ResourceDAL.sp_deskLogin, con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(ResourceDAL.email, SqlDbType.VarChar, -1).Value = objLoginBO.emailid;
                command.Parameters.Add(ResourceDAL.password, SqlDbType.VarChar, -1).Value = objLoginBO.password;
                sqlda = new SqlDataAdapter(command);
                sqlda.Fill(dtoutput);

                if (dtoutput.Rows.Count > 0)
                {
                    objOutPut.UserId = Convert.ToString(dtoutput.Rows[0]["ID"]);
                    objOutPut.FullName = Convert.ToString(dtoutput.Rows[0]["Full_name"]);
                    objOutPut.emailid = Convert.ToString(objLoginBO.emailid);
                    objOutPut.Error_msg = Convert.ToString(dtoutput.Rows[0]["Error_msg"]);
                    objOutPut.Error_code = Convert.ToInt32(dtoutput.Rows[0]["Error_code"]);
                    objOutPut.Menu_URL = Convert.ToString(dtoutput.Rows[0]["Menu_URL"]);
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
                objOutPut.Error_msg = ex.Message;
                objOutPut.Error_code = 1;
            }
            finally
            {
                con.Close();
            }
            return objOutPut;
        }

        public DataSet GetAllMenusDAL(LoginBO objLoginBO)
        {
            string Output = string.Empty;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda;
            DataSet dtoutput = new DataSet();
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand(ResourceDAL.sp_getMenusForDesk, connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(ResourceDAL.email, SqlDbType.VarChar, -1).Value = objLoginBO.emailid;
                sqlda = new SqlDataAdapter(command);
                sqlda.Fill(dtoutput);

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dtoutput;
        }

        public object GetConfiguredValueDAL(string module)
        {
            string Output = string.Empty;
            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection con = new SqlConnection(connstring);
            try
            {
                con.Open();
                SqlCommand command = new SqlCommand("select dbo.[fn_getSQLOTP]('" + module + "')", con);
                command.CommandType = CommandType.Text;
                Output = Convert.ToString(command.ExecuteScalar());
            }
            catch (Exception ex)
            {
                //LogFile("Error: Otp Page ", ex.Message.ToString(), ex.Data.Values.ToString(),
                  //  "-----------------------------------------------------------------------------------------------------------------");
            }
            finally
            {
                con.Close();
            }
            return Output;
        }

        /// <summary>
        /// Author: Monika M S
        /// Date: 29th July 2020
        /// </summary>
        /// <param name="objResetBO"></param>
        /// <returns></returns>
        public ResetBO changeOrresetpwd(ResetBO objResetBO)
        {
            ResetBO objreset = new ResetBO();
            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection con = new SqlConnection(connstring);
            DataTable dt = new DataTable();
            try
            {
                con.Open();
                SqlCommand command = new SqlCommand(ResourceDAL.ResetPasswordFortt_desk, con);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(ResourceDAL.EngineerId, SqlDbType.VarChar, 500).Value = objResetBO.Engid;
                command.Parameters.Add(ResourceDAL.password, SqlDbType.VarChar, 1000).Value = objResetBO.Newpassword;
                command.Parameters.Add(ResourceDAL.oldpassword, SqlDbType.VarChar, 1000).Value = objResetBO.OldPassword;
                SqlDataAdapter sa = new SqlDataAdapter(command);
                sa.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    if ((Convert.ToInt32(dt.Rows[0]["err_code"]) != 200))
                    {
                        objreset.Error_msg = "fail";
                        objreset.Error_code = Convert.ToInt32(dt.Rows[0]["err_code"]);
                    }
                    else
                    {
                        objreset.Error_msg = "Success";
                        objreset.Error_code = Convert.ToInt32(dt.Rows[0]["err_code"]);
                    }
                }
            }
            catch (Exception ex)
            {
                objreset.Error_msg = ex.Message;
                objreset.Error_code = 1;
            }
            finally
            {
                con.Close();
            }
            
            return objreset;

        }

        //public ResetBO ChangePasswordDAL(ResetBO objLoginBO)
        //{
        //    string Output = string.Empty;
        //    SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
        //    SqlDataAdapter sqlda;
        //    DataTable dtoutput = new DataTable();
        //    ResetBO objOutPut = new ResetBO();
        //    try
        //    {
        //        connection.Open();
        //        SqlCommand command = new SqlCommand(ResourceDAL.ResetPasswordFortt_desk, connection);
        //        command.CommandType = CommandType.StoredProcedure;
        //        command.Parameters.Add(ResourceDAL.EngineerId, SqlDbType.VarChar, -1).Value = objLoginBO.Engid;
        //        command.Parameters.Add(ResourceDAL.password, SqlDbType.VarChar, -1).Value = objLoginBO.Newpassword;
        //        command.Parameters.Add(ResourceDAL.oldpassword, SqlDbType.VarChar, -1).Value = objLoginBO.OldPassword;
        //        SqlDataAdapter sa = new SqlDataAdapter(command);
        //        sa.Fill(dtoutput);

        //        if ((Convert.ToInt32(dtoutput.Rows[0]["err_code"]) == 0))
        //        {
        //            objOutPut.Error_msg = "Success";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //    finally
        //    {
        //        if (connection.State == ConnectionState.Open)
        //        {
        //            connection.Close();
        //        }
        //    }
        //    return objOutPut;
        //}
    }
}