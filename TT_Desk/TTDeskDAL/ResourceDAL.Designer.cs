﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TTDeskDAL {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class ResourceDAL {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ResourceDAL() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("TTDeskDAL.ResourceDAL", typeof(ResourceDAL).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @assigned_salesengineer_id.
        /// </summary>
        internal static string assigned_salesengineer_id {
            get {
                return ResourceManager.GetString("assigned_salesengineer_id", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @CC.
        /// </summary>
        internal static string CC {
            get {
                return ResourceManager.GetString("CC", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @changeby.
        /// </summary>
        internal static string changeby {
            get {
                return ResourceManager.GetString("changeby", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @Component.
        /// </summary>
        internal static string Component {
            get {
                return ResourceManager.GetString("Component", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @cter.
        /// </summary>
        internal static string cter {
            get {
                return ResourceManager.GetString("cter", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @CustNumber.
        /// </summary>
        internal static string CustNumber {
            get {
                return ResourceManager.GetString("CustNumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @custtype.
        /// </summary>
        internal static string custtype {
            get {
                return ResourceManager.GetString("custtype", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @DDLNo.
        /// </summary>
        internal static string DDLNo {
            get {
                return ResourceManager.GetString("DDLNo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @DISTRIBUTOR.
        /// </summary>
        internal static string DISTRIBUTOR {
            get {
                return ResourceManager.GetString("DISTRIBUTOR", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @email.
        /// </summary>
        internal static string email {
            get {
                return ResourceManager.GetString("email", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @EndDate.
        /// </summary>
        internal static string EndDate {
            get {
                return ResourceManager.GetString("EndDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @EngineerId.
        /// </summary>
        internal static string EngineerId {
            get {
                return ResourceManager.GetString("EngineerId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @error_code.
        /// </summary>
        internal static string error_code {
            get {
                return ResourceManager.GetString("error_code", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @error_msg.
        /// </summary>
        internal static string error_msg {
            get {
                return ResourceManager.GetString("error_msg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @Family.
        /// </summary>
        internal static string Family {
            get {
                return ResourceManager.GetString("Family", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @flag.
        /// </summary>
        internal static string flag {
            get {
                return ResourceManager.GetString("flag", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to getCustomerDetails.
        /// </summary>
        internal static string getCustomerDetails {
            get {
                return ResourceManager.GetString("getCustomerDetails", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to GetMaxQuoteId.
        /// </summary>
        internal static string GetMaxQuoteId {
            get {
                return ResourceManager.GetString("GetMaxQuoteId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to getOfferPriceForSplItem.
        /// </summary>
        internal static string getOfferPriceForSplItem {
            get {
                return ResourceManager.GetString("getOfferPriceForSplItem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to GetProjMaxQuoteId.
        /// </summary>
        internal static string GetProjMaxQuoteId {
            get {
                return ResourceManager.GetString("GetProjMaxQuoteId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @ID.
        /// </summary>
        internal static string ID {
            get {
                return ResourceManager.GetString("ID", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @Industry.
        /// </summary>
        internal static string Industry {
            get {
                return ResourceManager.GetString("Industry", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @item.
        /// </summary>
        internal static string item {
            get {
                return ResourceManager.GetString("item", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @MailBody.
        /// </summary>
        internal static string MailBody {
            get {
                return ResourceManager.GetString("MailBody", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @Module.
        /// </summary>
        internal static string Module {
            get {
                return ResourceManager.GetString("Module", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @MOQ.
        /// </summary>
        internal static string MOQ {
            get {
                return ResourceManager.GetString("MOQ", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @MultiOrderFlag.
        /// </summary>
        internal static string MultiOrderFlag {
            get {
                return ResourceManager.GetString("MultiOrderFlag", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @OfferPrice.
        /// </summary>
        internal static string OfferPrice {
            get {
                return ResourceManager.GetString("OfferPrice", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @oldpassword.
        /// </summary>
        internal static string oldpassword {
            get {
                return ResourceManager.GetString("oldpassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @password.
        /// </summary>
        internal static string password {
            get {
                return ResourceManager.GetString("password", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @pdf_flag.
        /// </summary>
        internal static string pdf_flag {
            get {
                return ResourceManager.GetString("pdf_flag", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @Quotation_no.
        /// </summary>
        internal static string Quotation_no {
            get {
                return ResourceManager.GetString("Quotation_no", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @quote_id.
        /// </summary>
        internal static string quote_id {
            get {
                return ResourceManager.GetString("quote_id", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @Reason.
        /// </summary>
        internal static string Reason {
            get {
                return ResourceManager.GetString("Reason", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @ref_id.
        /// </summary>
        internal static string ref_id {
            get {
                return ResourceManager.GetString("ref_id", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @RefNumber.
        /// </summary>
        internal static string RefNumber {
            get {
                return ResourceManager.GetString("RefNumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ResetPasswordFortt_desk.
        /// </summary>
        internal static string ResetPasswordFortt_desk {
            get {
                return ResourceManager.GetString("ResetPasswordFortt_desk", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @role.
        /// </summary>
        internal static string role {
            get {
                return ResourceManager.GetString("role", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Desk.
        /// </summary>
        internal static string Role_flag {
            get {
                return ResourceManager.GetString("Role_flag", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @roleId.
        /// </summary>
        internal static string roleId {
            get {
                return ResourceManager.GetString("roleId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @SourceType.
        /// </summary>
        internal static string SourceType {
            get {
                return ResourceManager.GetString("SourceType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to sp_deskLogin.
        /// </summary>
        internal static string sp_deskLogin {
            get {
                return ResourceManager.GetString("sp_deskLogin", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to sp_GET_SPL_DeliveryWeek.
        /// </summary>
        internal static string sp_GET_SPL_DeliveryWeek {
            get {
                return ResourceManager.GetString("sp_GET_SPL_DeliveryWeek", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to sp_GET_SPL_Family.
        /// </summary>
        internal static string sp_GET_SPL_Family {
            get {
                return ResourceManager.GetString("sp_GET_SPL_Family", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to sp_GET_SPL_PaymentTerms.
        /// </summary>
        internal static string sp_GET_SPL_PaymentTerms {
            get {
                return ResourceManager.GetString("sp_GET_SPL_PaymentTerms", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to sp_getAcceptedQuotes.
        /// </summary>
        internal static string sp_getAcceptedQuotes {
            get {
                return ResourceManager.GetString("sp_getAcceptedQuotes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to sp_getAcceptedQuotesForDownload.
        /// </summary>
        internal static string sp_getAcceptedQuotesForDownload {
            get {
                return ResourceManager.GetString("sp_getAcceptedQuotesForDownload", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to sp_GetCustomersBasedOnDistributor.
        /// </summary>
        internal static string sp_GetCustomersBasedOnDistributor {
            get {
                return ResourceManager.GetString("sp_GetCustomersBasedOnDistributor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to sp_getEscalatedQuotes.
        /// </summary>
        internal static string sp_getEscalatedQuotes {
            get {
                return ResourceManager.GetString("sp_getEscalatedQuotes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to sp_getIndustryManager.
        /// </summary>
        internal static string sp_getIndustryManager {
            get {
                return ResourceManager.GetString("sp_getIndustryManager", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to sp_getItemDesc.
        /// </summary>
        internal static string sp_getItemDesc {
            get {
                return ResourceManager.GetString("sp_getItemDesc", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to sp_GetItemDetailsForQuote.
        /// </summary>
        internal static string sp_GetItemDetailsForQuote {
            get {
                return ResourceManager.GetString("sp_GetItemDetailsForQuote", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to sp_getMailDetailsForQuote.
        /// </summary>
        internal static string sp_getMailDetailsForQuote {
            get {
                return ResourceManager.GetString("sp_getMailDetailsForQuote", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to sp_getMenusForDesk.
        /// </summary>
        internal static string sp_getMenusForDesk {
            get {
                return ResourceManager.GetString("sp_getMenusForDesk", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to sp_getPendingQuotes.
        /// </summary>
        internal static string sp_getPendingQuotes {
            get {
                return ResourceManager.GetString("sp_getPendingQuotes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to sp_getQuoteDetails.
        /// </summary>
        internal static string sp_getQuoteDetails {
            get {
                return ResourceManager.GetString("sp_getQuoteDetails", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to sp_getQuoteFormat.
        /// </summary>
        internal static string sp_getQuoteFormat {
            get {
                return ResourceManager.GetString("sp_getQuoteFormat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to sp_getQuoteSummaryDesk.
        /// </summary>
        internal static string sp_getQuoteSummaryDesk {
            get {
                return ResourceManager.GetString("sp_getQuoteSummaryDesk", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to sp_GetSpecialCategories.
        /// </summary>
        internal static string sp_GetSpecialCategories {
            get {
                return ResourceManager.GetString("sp_GetSpecialCategories", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to sp_GetSplCategories.
        /// </summary>
        internal static string sp_GetSplCategories {
            get {
                return ResourceManager.GetString("sp_GetSplCategories", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to sp_getSPLItemsDetails.
        /// </summary>
        internal static string sp_getSPLItemsDetails {
            get {
                return ResourceManager.GetString("sp_getSPLItemsDetails", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to sp_getSPLQuotes.
        /// </summary>
        internal static string sp_getSPLQuotes {
            get {
                return ResourceManager.GetString("sp_getSPLQuotes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to sp_getStatusLog.
        /// </summary>
        internal static string sp_getStatusLog {
            get {
                return ResourceManager.GetString("sp_getStatusLog", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to sp_quoteStatusChange.
        /// </summary>
        internal static string sp_quoteStatusChange {
            get {
                return ResourceManager.GetString("sp_quoteStatusChange", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to sp_saveProjectQuotes.
        /// </summary>
        internal static string sp_saveProjectQuotes {
            get {
                return ResourceManager.GetString("sp_saveProjectQuotes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to sp_saveSplQuotes.
        /// </summary>
        internal static string sp_saveSplQuotes {
            get {
                return ResourceManager.GetString("sp_saveSplQuotes", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @StartDate.
        /// </summary>
        internal static string StartDate {
            get {
                return ResourceManager.GetString("StartDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @Status.
        /// </summary>
        internal static string Status {
            get {
                return ResourceManager.GetString("Status", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @Subject.
        /// </summary>
        internal static string Subject {
            get {
                return ResourceManager.GetString("Subject", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @tblQuote.
        /// </summary>
        internal static string tblQuote {
            get {
                return ResourceManager.GetString("tblQuote", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @TO.
        /// </summary>
        internal static string TO {
            get {
                return ResourceManager.GetString("TO", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @Unique_id.
        /// </summary>
        internal static string Unique_id {
            get {
                return ResourceManager.GetString("Unique_id", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @Value.
        /// </summary>
        internal static string Value {
            get {
                return ResourceManager.GetString("Value", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @Value1.
        /// </summary>
        internal static string Value1 {
            get {
                return ResourceManager.GetString("Value1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @Value10.
        /// </summary>
        internal static string Value10 {
            get {
                return ResourceManager.GetString("Value10", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @Value2.
        /// </summary>
        internal static string Value2 {
            get {
                return ResourceManager.GetString("Value2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @Value3.
        /// </summary>
        internal static string Value3 {
            get {
                return ResourceManager.GetString("Value3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @Value4.
        /// </summary>
        internal static string Value4 {
            get {
                return ResourceManager.GetString("Value4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @Value5.
        /// </summary>
        internal static string Value5 {
            get {
                return ResourceManager.GetString("Value5", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @Value6.
        /// </summary>
        internal static string Value6 {
            get {
                return ResourceManager.GetString("Value6", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @Value7.
        /// </summary>
        internal static string Value7 {
            get {
                return ResourceManager.GetString("Value7", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @Value8.
        /// </summary>
        internal static string Value8 {
            get {
                return ResourceManager.GetString("Value8", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to @Value9.
        /// </summary>
        internal static string Value9 {
            get {
                return ResourceManager.GetString("Value9", resourceCulture);
            }
        }
    }
}
