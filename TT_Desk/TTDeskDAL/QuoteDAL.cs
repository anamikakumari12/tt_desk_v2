﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using TTDeskBO;

namespace TTDeskDAL
{
    public class QuoteDAL
    {
        CommonFunctions objCom = new CommonFunctions();
        StreamWriter log;
        

        public DataTable GetPendingTasksDAL( QuoteBO objQuoteBO)
        {
            DataTable dtoutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceDAL.sp_getPendingQuotes, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceDAL.StartDate, SqlDbType.VarChar, 100).Value = objQuoteBO.start_date;
                sqlcmd.Parameters.Add(ResourceDAL.EndDate, SqlDbType.VarChar, 100).Value = objQuoteBO.end_date;
                sqlcmd.Parameters.Add(ResourceDAL.role, SqlDbType.VarChar, 10).Value = ResourceDAL.Role_flag;
                sqlcmd.Parameters.Add(ResourceDAL.cter, SqlDbType.VarChar, 10).Value = objQuoteBO.cter;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtoutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return dtoutput;
        }

        public DataTable GetAcceptedQuotesForDownloadDAL(QuoteBO objQuoteBO)
        {
            DataTable dtoutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceDAL.sp_getAcceptedQuotesForDownload, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceDAL.StartDate, SqlDbType.VarChar, 100).Value = objQuoteBO.start_date;
                sqlcmd.Parameters.Add(ResourceDAL.EndDate, SqlDbType.VarChar, 100).Value = objQuoteBO.end_date;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtoutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return dtoutput;
        }

       

        public DataTable GetEscalatedQuotesDAL(QuoteBO objQuoteBO)
        {
            DataTable dtoutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceDAL.sp_getEscalatedQuotes, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;

                sqlcmd.Parameters.Add(ResourceDAL.StartDate, SqlDbType.VarChar, 100).Value = objQuoteBO.start_date;
                sqlcmd.Parameters.Add(ResourceDAL.EndDate, SqlDbType.VarChar, 100).Value = objQuoteBO.end_date;
                sqlcmd.Parameters.Add(ResourceDAL.role, SqlDbType.VarChar, 10).Value = "Desk";
                sqlcmd.Parameters.Add(ResourceDAL.cter, SqlDbType.VarChar, 10).Value = objQuoteBO.cter;
                sqlcmd.Parameters.Add(ResourceDAL.assigned_salesengineer_id, SqlDbType.VarChar, 10).Value = null;
                sqlcmd.CommandTimeout = 1000000;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtoutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return dtoutput;
        }

       

        public DataTable GetItemDescDAL(SplQuoteBO objSplQuoteBO)
        {
            DataTable dtoutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceDAL.sp_getItemDesc, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceDAL.CustNumber, SqlDbType.VarChar, 100).Value = objSplQuoteBO.CustomerNumber;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtoutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtoutput;
        }

        public DataTable GetDeliveryWeekDAL()
        {
            DataTable dtoutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceDAL.sp_GET_SPL_DeliveryWeek, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtoutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return dtoutput;
        }

        public DataTable GetPaymentTermsDAL(SplQuoteBO objQuoteBO)
        {
            DataTable dtoutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceDAL.sp_GET_SPL_PaymentTerms, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceDAL.CustNumber, SqlDbType.VarChar, 100).Value = objQuoteBO.CustomerNumber;
                sqlcmd.Parameters.Add(ResourceDAL.SourceType, SqlDbType.VarChar, 100).Value = objQuoteBO.Payment_SourceType;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtoutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return dtoutput;
        }

        public DataTable GetFamilyDAL()
        {
            DataTable dtoutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceDAL.sp_GET_SPL_Family, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtoutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return dtoutput;
        }
        public DataTable GetEscalatedTasksDAL(QuoteBO objQuoteBO)
        {
            DataTable dtoutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceDAL.sp_getEscalatedQuotes, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;

                //objCom.IntoFile("startDate:" + objQuoteBO.start_date.ToString());
                //objCom.IntoFile("endDate:" + objQuoteBO.end_date.ToString());
                //objCom.IntoFile("flag:" + objQuoteBO.flag.ToString());
                //objCom.IntoFile("status:" + objQuoteBO.Status.ToString());

                sqlcmd.Parameters.Add(ResourceDAL.StartDate, SqlDbType.VarChar, 100).Value = objQuoteBO.start_date;
                sqlcmd.Parameters.Add(ResourceDAL.EndDate, SqlDbType.VarChar, 100).Value = objQuoteBO.end_date;
                sqlcmd.Parameters.Add(ResourceDAL.role, SqlDbType.VarChar, 10).Value = "HO";
                sqlcmd.Parameters.Add(ResourceDAL.cter, SqlDbType.VarChar, 10).Value = objQuoteBO.cter;
                sqlcmd.Parameters.Add(ResourceDAL.assigned_salesengineer_id, SqlDbType.VarChar, 10).Value = null;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtoutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return dtoutput;
        }

        public List<QuoteBO> updateQuoteStatusDAL(DataTable dtQuote)
        {
            List<QuoteBO> objList = new List<QuoteBO>();
            QuoteBO objOutput = new QuoteBO();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlCommand sqlcmd = null;
            SqlDataAdapter sqlda = null;
            DataSet ds = new DataSet();
            DataTable dtoutput = new DataTable();
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceDAL.sp_quoteStatusChange, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue(ResourceDAL.tblQuote, dtQuote);
                //sqlcmd.Parameters.Add(ResourceDAL.pdf_flag, SqlDbType.Int).Direction = ParameterDirection.Output;
                //sqlcmd.Parameters.Add(ResourceDAL.error_code, SqlDbType.Int).Direction = ParameterDirection.Output;
                //sqlcmd.Parameters.Add(ResourceDAL.error_msg, SqlDbType.VarChar, -1).Direction = ParameterDirection.Output;
                
                //sqlcmd.ExecuteNonQuery();
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(ds);

                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            ds.Tables[0].Columns.Remove("Item");
                            ds.Tables[0].AcceptChanges();
                            DataView view = new DataView(ds.Tables[0]);
                            string[] param = { "Ref_Number", "Quotation_no", "MailTo", "MAilCC", "Subject", "Body", "error_code", "error_msg", "PDF_Flag" };
                            DataTable distinctValues = view.ToTable(true,param);

                            var row = distinctValues.AsEnumerable()
                                .Where(o => o.Field<string>("MailTo")!= null || o.Field<string>("MailTo")!="")
                                .Select(o => new QuoteBO { Ref_Number = o.Field<string>("Ref_Number")
                                    , Quotation_no=o.Field<string>("Quotation_no")
                                    , to=o.Field<string>("MailTo")
                                    , cc=o.Field<string>("MAilCC")
                                    , subject=o.Field<string>("Subject")
                                    , message=o.Field<string>("Body")
                                    , Error_code=o.Field<Int32>("error_code")
                                    , Error_msg = o.Field<string>("error_msg")
                                    , pdf_flag=o.Field<Int32>("PDF_Flag")
                                }).Distinct().ToList();
                            objList = row;

                            //string[] param_pdf = { "Ref_Number", "Quotation_no", "Item" };
                            //DataTable distinctValues_pdf = view.ToTable(true, param_pdf);
                           
                            //foreach(QuoteBO obj in objList)
                            //{
                            // var row_pdf= distinctValues.AsEnumerable()
                            //    .Where(o => o.Field<string>("Ref_Number") != obj.Ref_Number)
                            //    .Select(o => new pdf_detail
                            //    {
                            //        Ref_Number = o.Field<string>("Ref_Number")
                            //        , Quotation_no=o.Field<string>("Quotation_no")
                            //        , Item_Number=o.Field<string>("Item")
                                    
                            //    }).Distinct().ToList();
                            //    obj.pdf_detail = row_pdf;
                            //}

                        }
                    }
                }
                //objList = ConvertDataTable<QuoteBO>(dtoutput);
                //objOutput.pdf_flag = Convert.ToInt32(sqlcmd.Parameters[ResourceDAL.pdf_flag].Value);
                //objOutput.Error_code = Convert.ToInt32(sqlcmd.Parameters[ResourceDAL.error_code].Value);
                //objOutput.Error_msg = Convert.ToString(sqlcmd.Parameters[ResourceDAL.error_msg].Value);
                
                //sqlcmd = new SqlCommand(ResourceDAL.sp_getMailDetailsForQuote, sqlconn);
                //sqlcmd.CommandType = CommandType.StoredProcedure;
                //sqlcmd.Parameters.Add(ResourceDAL.Module, SqlDbType.VarChar, -1).Value = "Quote";
                //sqlcmd.Parameters.Add(ResourceDAL.Component, SqlDbType.VarChar, -1).Value = "StatusChange";
                //sqlcmd.Parameters.Add(ResourceDAL.RefNumber, SqlDbType.VarChar, -1).Value = objQuoteBO.Ref_Number;
                //sqlcmd.Parameters.Add(ResourceDAL.item, SqlDbType.VarChar, 100).Value = objQuoteBO.Item_Number;
                //sqlda = new SqlDataAdapter(sqlcmd);
                //sqlda.Fill(dtoutput);

                //if (dtoutput != null)
                //{
                //    if (dtoutput.Rows.Count > 0)
                //    {
                //        objOutput = new QuoteBO();
                //        objOutput.to = Convert.ToString(dtoutput.Rows[0]["MailTo"]);
                //        objOutput.cc = Convert.ToString(dtoutput.Rows[0]["MailCC"]);
                //        objOutput.subject = Convert.ToString(dtoutput.Rows[0]["Subject"]);
                //        objOutput.message = Convert.ToString(dtoutput.Rows[0]["Body"]);
                //        objList.Add(objOutput);
                //    }
                //}
            }
            catch (Exception ex)
            {
                objOutput.Error_code = 1;
                objOutput.Error_msg = "There is some error in status update. Please try again.";
                objList.Add(objOutput);
                objCom.ErrorLog(ex);
            }
            return objList;
        }

        private static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        } 

        public DataTable GetQuoteSummaryDAL(QuoteBO objQuoteBO)
        {
            DataTable dtoutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceDAL.sp_getQuoteSummaryDesk, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;

                //objCom.IntoFile("startDate:" + objQuoteBO.start_date.ToString());
                //objCom.IntoFile("endDate:" + objQuoteBO.end_date.ToString());
                //objCom.IntoFile("flag:" + objQuoteBO.flag.ToString());
                //objCom.IntoFile("status:" + objQuoteBO.Status.ToString());
           
                sqlcmd.Parameters.Add(ResourceDAL.StartDate, SqlDbType.VarChar, 100).Value = objQuoteBO.start_date;
                sqlcmd.Parameters.Add(ResourceDAL.EndDate, SqlDbType.VarChar, 100).Value = objQuoteBO.end_date;
                sqlcmd.Parameters.Add(ResourceDAL.flag, SqlDbType.VarChar, 100).Value = objQuoteBO.flag;
                sqlcmd.Parameters.Add(ResourceDAL.Status, SqlDbType.VarChar, 100).Value = objQuoteBO.Status;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtoutput);
                objCom.IntoFile("Datatable is full");
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return dtoutput;
        }

        public DataTable GetQuoteDetailsDAL(QuoteBO objquoteBO)
        {
            DataTable dtoutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            //QuoteBO objquoteBO = new QuoteBO();
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceDAL.sp_getQuoteDetails, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceDAL.ID, SqlDbType.Int, 100).Value = objquoteBO.QuoteID;
                sqlcmd.Parameters.Add(ResourceDAL.item, SqlDbType.VarChar, 100).Value = objquoteBO.Item_Number;
                sqlcmd.Parameters.Add(ResourceDAL.RefNumber, SqlDbType.VarChar, 100).Value = objquoteBO.Ref_Number;
                sqlcmd.Parameters.Add(ResourceDAL.flag, SqlDbType.VarChar, 100).Value = objquoteBO.flag;
                sqlcmd.Parameters.Add(ResourceDAL.Status, SqlDbType.VarChar, 100).Value = objquoteBO.Status;

                sqlcmd.Parameters.Add(ResourceDAL.StartDate, SqlDbType.VarChar, 100).Value = objquoteBO.start_date;
                sqlcmd.Parameters.Add(ResourceDAL.EndDate, SqlDbType.VarChar, 100).Value = objquoteBO.end_date;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtoutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return dtoutput;
        }

        public DataTable getQuoteFormatDAL(QuoteBO objQuoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceDAL.sp_getQuoteFormat, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceDAL.RefNumber, SqlDbType.VarChar, -1).Value = objQuoteBO.Ref_Number;
                sqlcmd.Parameters.Add(ResourceDAL.Quotation_no, SqlDbType.VarChar, -1).Value = objQuoteBO.Quotation_no;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetAcceptedQuotesDAL(QuoteBO objQuoteBO)
        {
            DataTable dtoutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceDAL.sp_getAcceptedQuotes, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceDAL.StartDate, SqlDbType.VarChar, 100).Value = objQuoteBO.start_date;
                sqlcmd.Parameters.Add(ResourceDAL.EndDate, SqlDbType.VarChar, 100).Value = objQuoteBO.end_date;
                sqlcmd.Parameters.Add(ResourceDAL.cter, SqlDbType.VarChar, 10).Value = objQuoteBO.cter;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtoutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return dtoutput;
        }

        public DataTable GetQuoteStatusLogDAL(QuoteBO objQuoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceDAL.sp_getStatusLog, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceDAL.RefNumber, SqlDbType.VarChar, -1).Value = objQuoteBO.Ref_Number;
                sqlcmd.Parameters.Add(ResourceDAL.item, SqlDbType.VarChar, 100).Value = objQuoteBO.Item_Number;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetSplQuoteDropdownsDAL(SplQuoteBO objSplQuoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceDAL.sp_GetSplCategories, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceDAL.Value, SqlDbType.VarChar,-1).Value = objSplQuoteBO.DropdwonValue;
                sqlcmd.Parameters.Add(ResourceDAL.DDLNo, SqlDbType.Int).Value = objSplQuoteBO.DropdwonNumber;
                sqlcmd.Parameters.Add(ResourceDAL.Family, SqlDbType.VarChar, -1).Value = objSplQuoteBO.Family;

                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }
        
        public DataTable GetCustomerDetailsDAL(SplQuoteBO objSplQuoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceDAL.getCustomerDetails, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceDAL.roleId, SqlDbType.VarChar, 50).Value = "HO";
                sqlcmd.Parameters.Add(ResourceDAL.custtype, SqlDbType.VarChar,10).Value = objSplQuoteBO.CustomerType;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }
        public DataTable GetCPCustomerDetailsDAL(SplQuoteBO objSplQuoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceDAL.sp_GetCustomersBasedOnDistributor, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceDAL.DISTRIBUTOR, SqlDbType.VarChar, 10).Value = objSplQuoteBO.Distributor;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public DataTable GetItemDetailsDAL(SplQuoteBO objSplQuoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceDAL.sp_GetItemDetailsForQuote, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceDAL.item, SqlDbType.VarChar, 100).Value = objSplQuoteBO.item;
                sqlcmd.Parameters.Add(ResourceDAL.CustNumber, SqlDbType.VarChar, 100).Value = objSplQuoteBO.CustomerNumber;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public SplQuoteBO SaveSplQuotesDAL(DataTable dt)
        {
            SplQuoteBO objQuoteBO = new SplQuoteBO();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlCommand sqlcmd = null;
            SqlDataAdapter sqlda = null;
            DataTable dtoutput = new DataTable();
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceDAL.sp_saveSplQuotes, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue(ResourceDAL.tblQuote, dt);
                sqlcmd.Parameters.Add(ResourceDAL.error_code, SqlDbType.Int).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceDAL.error_msg, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceDAL.RefNumber, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceDAL.TO, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceDAL.CC, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceDAL.Subject, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceDAL.MailBody, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                sqlcmd.ExecuteNonQuery();
                objQuoteBO.Err_code = Convert.ToInt32(sqlcmd.Parameters[ResourceDAL.error_code].Value);
                objQuoteBO.Err_msg = Convert.ToString(sqlcmd.Parameters[ResourceDAL.error_msg].Value);
                objQuoteBO.Ref_Number = Convert.ToString(sqlcmd.Parameters[ResourceDAL.RefNumber].Value);
                objQuoteBO.TO = Convert.ToString(sqlcmd.Parameters[ResourceDAL.TO].Value);
                objQuoteBO.CC = Convert.ToString(sqlcmd.Parameters[ResourceDAL.CC].Value);
                objQuoteBO.Subject = Convert.ToString(sqlcmd.Parameters[ResourceDAL.Subject].Value);
                objQuoteBO.Body = Convert.ToString(sqlcmd.Parameters[ResourceDAL.MailBody].Value);

                //sqlcmd = new SqlCommand(ResourceDAL.sp_getMailDetailsForQuote, sqlconn);
                //sqlcmd.CommandType = CommandType.StoredProcedure;
                //sqlcmd.Parameters.Add(ResourceDAL.Module, SqlDbType.VarChar, -1).Value = "Quote";
                //sqlcmd.Parameters.Add(ResourceDAL.Component, SqlDbType.VarChar, -1).Value = "SubmitQuote";
                //sqlcmd.Parameters.Add(ResourceDAL.RefNumber, SqlDbType.VarChar, -1).Value = objQuoteBO.Ref_Number;
                //sqlcmd.Parameters.Add(ResourceDAL.item, SqlDbType.VarChar, 100).Value = objQuoteBO.Item_Number;
                //sqlda = new SqlDataAdapter(sqlcmd);
                //sqlda.Fill(dtoutput);
                //if (dtoutput != null)
                //{
                //    if (dtoutput.Rows.Count > 0)
                //    {
                //        objQuoteBO.to = Convert.ToString(dtoutput.Rows[0]["To"]);
                //        objQuoteBO.cc = Convert.ToString(dtoutput.Rows[0]["CC"]);
                //        objQuoteBO.subject = Convert.ToString(dtoutput.Rows[0]["Subject"]);
                //        objQuoteBO.message = Convert.ToString(dtoutput.Rows[0]["Message"]);
                //    }
                //}

            }
            catch (Exception ex)
            {
                objQuoteBO.Err_code = 202;
                objQuoteBO.Err_msg = ex.Message;
                objCom.ErrorLog(ex);
            }
            return objQuoteBO;
        }

        public DataTable GetOfferPriceSplItemDAL(SplQuoteBO objSplQuoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceDAL.getOfferPriceForSplItem, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceDAL.Value1, SqlDbType.VarChar, -1).Value = objSplQuoteBO.Value1;
                sqlcmd.Parameters.Add(ResourceDAL.Value2, SqlDbType.VarChar, -1).Value = objSplQuoteBO.Value2;
                sqlcmd.Parameters.Add(ResourceDAL.Value3, SqlDbType.VarChar, -1).Value = objSplQuoteBO.Value3;
                sqlcmd.Parameters.Add(ResourceDAL.Value4, SqlDbType.VarChar, -1).Value = objSplQuoteBO.Value4;
                sqlcmd.Parameters.Add(ResourceDAL.Value5, SqlDbType.VarChar, -1).Value = objSplQuoteBO.Value5;
                sqlcmd.Parameters.Add(ResourceDAL.Value6, SqlDbType.VarChar, -1).Value = objSplQuoteBO.Value6;
                sqlcmd.Parameters.Add(ResourceDAL.Value7, SqlDbType.VarChar, -1).Value = objSplQuoteBO.Value7;
                sqlcmd.Parameters.Add(ResourceDAL.Value8, SqlDbType.VarChar, -1).Value = objSplQuoteBO.Value8;
                sqlcmd.Parameters.Add(ResourceDAL.Value9, SqlDbType.VarChar, -1).Value = objSplQuoteBO.Value9;
                sqlcmd.Parameters.Add(ResourceDAL.Value10, SqlDbType.VarChar, -1).Value = objSplQuoteBO.Value10;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public int GetMaxQuoteIDDAL()
        {
            int Output = 0;
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceDAL.GetMaxQuoteId, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceDAL.quote_id, SqlDbType.Int).Direction = ParameterDirection.Output;
                sqlcmd.ExecuteNonQuery();
                Output = Convert.ToInt32(sqlcmd.Parameters[ResourceDAL.quote_id].Value);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return Output;
        }

        public DataTable GetSPLQuotesDAL(SplQuoteBO objSplQuoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceDAL.sp_getSPLQuotes, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceDAL.StartDate, SqlDbType.VarChar, 100).Value = objSplQuoteBO.start_date;
                sqlcmd.Parameters.Add(ResourceDAL.EndDate, SqlDbType.VarChar, 100).Value = objSplQuoteBO.end_date;
                sqlcmd.Parameters.Add(ResourceDAL.custtype, SqlDbType.VarChar, 10).Value = objSplQuoteBO.CustomerType;
                sqlcmd.Parameters.Add(ResourceDAL.CustNumber, SqlDbType.VarChar, 100).Value = objSplQuoteBO.CustomerNumber;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }
        public static DataTable GetSPLItemDetailsDAL(SplQuoteBO objSplQuoteBO)
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceDAL.sp_getSPLItemsDetails, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceDAL.RefNumber, SqlDbType.VarChar, 100).Value = objSplQuoteBO.Ref_Number;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                CommonFunctions.StaticErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }


        public DataTable GetIndustryListDAL()
        {
            DataTable dtOutput = new DataTable();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlDataAdapter sqlda = null;
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceDAL.sp_getIndustryManager, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlda = new SqlDataAdapter(sqlcmd);
                sqlda.Fill(dtOutput);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return dtOutput;
        }

        public SplQuoteBO SaveProjectQuotesDAL(DataTable dt, string industry)
        {
            SplQuoteBO objQuoteBO = new SplQuoteBO();
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlCommand sqlcmd = null;
            SqlDataAdapter sqlda = null;
            DataTable dtoutput = new DataTable();
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceDAL.sp_saveProjectQuotes, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue(ResourceDAL.tblQuote, dt);
                sqlcmd.Parameters.Add(ResourceDAL.Industry, SqlDbType.VarChar, 100).Value = industry;
                sqlcmd.Parameters.Add(ResourceDAL.error_code, SqlDbType.Int).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceDAL.error_msg, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceDAL.RefNumber, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceDAL.TO, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceDAL.CC, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceDAL.Subject, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.Add(ResourceDAL.MailBody, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                sqlcmd.ExecuteNonQuery();
                objQuoteBO.Err_code = Convert.ToInt32(sqlcmd.Parameters[ResourceDAL.error_code].Value);
                objQuoteBO.Err_msg = Convert.ToString(sqlcmd.Parameters[ResourceDAL.error_msg].Value);
                objQuoteBO.Ref_Number = Convert.ToString(sqlcmd.Parameters[ResourceDAL.RefNumber].Value);
                objQuoteBO.TO = Convert.ToString(sqlcmd.Parameters[ResourceDAL.TO].Value);
                objQuoteBO.CC = Convert.ToString(sqlcmd.Parameters[ResourceDAL.CC].Value);
                objQuoteBO.Subject = Convert.ToString(sqlcmd.Parameters[ResourceDAL.Subject].Value);
                objQuoteBO.Body = Convert.ToString(sqlcmd.Parameters[ResourceDAL.MailBody].Value);

            }
            catch (Exception ex)
            {
                objQuoteBO.Err_code = 202;
                objQuoteBO.Err_msg = ex.Message;
                objCom.ErrorLog(ex);
            }
            return objQuoteBO;
        }

        public int GetProjMaxQuoteIDDAL()
        {
            int Output = 0;
            SqlConnection sqlconn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlCommand sqlcmd = null;
            try
            {
                sqlconn.Open();
                sqlcmd = new SqlCommand(ResourceDAL.GetProjMaxQuoteId, sqlconn);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.Add(ResourceDAL.quote_id, SqlDbType.Int).Direction = ParameterDirection.Output;
                sqlcmd.ExecuteNonQuery();
                Output = Convert.ToInt32(sqlcmd.Parameters[ResourceDAL.quote_id].Value);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                sqlconn.Close();
            }
            return Output;
        }

    }
}