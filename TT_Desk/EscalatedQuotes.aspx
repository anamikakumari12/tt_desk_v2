﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EscalatedQuotes.aspx.cs" Inherits="TT_Desk.EscalatedQuotes" MasterPageFile="~/SiteMaster.Master" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <link href="vendor/daterangepicker2/daterangepicker.css" rel="stylesheet" />
    <script src="vendor/daterangepicker2/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" />
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedcolumns/3.2.6/css/fixedColumns.dataTables.min.css" />
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script>

    <style>
        .table.dataTable.no-footer
        {
            width:100%!important;
        }
        .btn {
            font-size: 13px;
            padding: 5px;
        }

        table td {
            border: none;
        }

        th {
            border: none;
        }

         thead
        {
            color: black!important;
        }

        table {
            border: none;
        }

        .required:after {
            content: " *";
            color: red;
        }

        .cssComment {
            min-width: 150px;
        }
    </style>
    <script type="text/javascript">
        var win;
        $(document).ready(function () {
            debugger;
            var SelectedStart = sessionStorage.getItem("selectedEscalatedStart");
            var SelectedEnd = sessionStorage.getItem("selectedEscalatedEnd");
            var start = (SelectedStart == null ? "07/20/2019" : SelectedStart);
            var end = (SelectedEnd == null ? moment() : SelectedEnd);
            $('#body_txtDateRange').daterangepicker({
                //opens: 'left',
                autoUpdateInput: true,
                locale: {
                    format: 'MM/DD/YYYY'
                },
                startDate: start,
                endDate: end,
                ranges: {
                    'All Date': ['07/20/2019', moment()],
                    'Last Year': [moment().subtract(1, 'year'), moment()],
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
            }, function (start, end, label) {
                console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
                var start = start.format('MM/DD/YYYY');
                var end = end.format('MM/DD/YYYY');
                sessionStorage.setItem('selectedEscalatedStart', start);
                sessionStorage.setItem('selectedEscalatedEnd', end);
            });
            LoadTable();

        });

        function LoadTable() {
            var head_content = $('#body_grdEscalatedQuote tr:first').html();
            $('#body_grdEscalatedQuote').prepend('<thead></thead>')
            $('#body_grdEscalatedQuote thead').html('<tr>' + head_content + '</tr>');
            $('#body_grdEscalatedQuote tbody tr:first').hide();
            var table = $('#body_grdEscalatedQuote').dataTable({
                //scrollY: "350px",
                scrollX: true,
                scrollCollapse: true,
                fixedColumns: true,
                fixedColumns: {
                    leftColumns: 1,
                    rightColumns: 1
                },
                paging: true
                //lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                //dom: 'lBfrtip',

            });

            var totaltrCount = $("[id*=grdEscalatedQuote] tr").length;
            var trCount = $("[id*=sgrdEscalatedQuote] td").closest("tr").length;
            sessionStorage.setItem("tot_Count", trCount);
        }

        function Clear() {
            sessionStorage.removeItem('selectedEscalatedStart');
            sessionStorage.removeItem('selectedEscalatedEnd');
            var SelectedStart = sessionStorage.getItem("selectedEscalatedStart");
            var SelectedEnd = sessionStorage.getItem("selectedEscalatedEnd");
            var start = (SelectedStart == null ? "07/20/2019" : SelectedStart);
            var end = (SelectedEnd == null ? moment() : SelectedEnd);
            $('#body_txtDateRange').daterangepicker({
                //opens: 'left',
                autoUpdateInput: true,
                locale: {
                    format: 'MM/DD/YYYY'
                },
                startDate: start,
                endDate: end,
                ranges: {
                    'All Date': ['07/20/2019', moment()],
                    'Last Year': [moment().subtract(1, 'year'), moment()],
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
            }, function (start, end, label) {
                console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
                var start = start.format('MM/DD/YYYY');
                var end = end.format('MM/DD/YYYY');
                sessionStorage.setItem('selectedEscalatedStart', start);
                sessionStorage.setItem('selectedEscalatedEnd', end);
            });
        }

        function OpenDetailView(evt, obj) {
            var flag = 'Escalated';
            var id = obj.id;
            var ref_id = id.replace("btnView", "hdnID");
            var ref = $("#" + ref_id).val();
			if(ref.includes(','))
			{
			ref=ref.split(',');
			ref=ref[0];
			}
            win = window.open("QuoteSummaryDetails.aspx?id=" + ref + "&flag=" + flag, "_blank", "WIDTH=900,HEIGHT=650,scrollbars=no, menubar=no,resizable=yes,directories=no,location=no");

            return false;
        }

        

        function OpenStatusLog(evt, obj) {
            debugger;
            var id = obj.id;
            var ref_id = id.replace("ImageButton1", "lblref");
            var itemdesc_id = id.replace("ImageButton1", "lblitemdesc");
            var item_id = id.replace("ImageButton1", "lblitem");
            var ref_number = $("#" + ref_id).text();
            var item_desc = $("#" + itemdesc_id).text();
            var item = $("#" + item_id).text();
            var uri = "QuoteStatusDetails.aspx?item=" + item + "&desc=" + item_desc + "&ref=" + ref_number;
            win = window.open(encodeURI(uri), "_blank", "WIDTH=1000,HEIGHT=350,scrollbars=no, menubar=no,resizable=yes,directories=no,location=no");
			return false;
        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div class="container1">
        <div class="col-md-12">
            <div class="mn_view_stock">
                <h4>Escalated Quotes </h4>
            </div>

             <div class="mn_section mn_filter">
            <div>
                <div>
                   <%-- <div class="col-md-2 mn_view_stock pull-right">--%>
                     <div class="col-md-2 mn_view_stock">
                        <label class="control-label">Requested Date</label>
                        <asp:TextBox ID="txtDateRange" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-md-1 mn_view_stock" style="margin-top: 30px; padding: 1px!important">

                        <asp:Button ID="btnFilter" runat="server" CssClass="btn btn-success button pull-right" Text="Filter" CausesValidation="false" OnClick="btnFilter_Click" />
                    </div>
                    <div class="col-md-1 mn_view_stock" style="margin-top: 30px; padding: 1px!important">
                        <asp:Button ID="btnClear" runat="server" CssClass="btn btn-success button" Text="Clear" OnClientClick="Clear();" OnClick="btnFilter_Click" />
                    </div>
                    <div class="col-md-8 mn_view_stock pull-right"></div>
                </div>
            </div>
             </div>
            <asp:Label ID="lblresult" Text="" runat="server"></asp:Label>
            <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>
            <asp:UpdatePanel ID="panel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divQuotes" runat="server">
                        <div class="col-md-12 nopad" runat="server" style="display: block;">
                            <asp:Label ID="lblErrMsg" runat="server" Style="color: red;"></asp:Label>
                            <asp:GridView ID="grdEscalatedQuote" CssClass="display compact" runat="server" AutoGenerateColumns="false" Style="width: 100%">
                                <Columns>

                                    <asp:TemplateField HeaderText="CP Name">
                                        <ItemTemplate>
                                            <asp:HiddenField runat="server" ID="hdnID" Value='<%# Eval("ID")%>' />
                                            <asp:Label ID="txtCustName" Text='<%#Bind("CP_Name") %>' runat="server"></asp:Label>
                                            <asp:HiddenField runat="server" ID="hdnStockCode" Value='<%# Eval("StockCode")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Reference No">
                                        <ItemTemplate>
                                            <asp:Label ID="lblref" runat="server" Text='<%#Bind("Ref_number") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Item Code">
                                        <ItemTemplate>
                                            <asp:Label ID="lblitem" runat="server" Text='<%#Bind("Item_code") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Item Description" HeaderStyle-Width="200px">
                                        <ItemTemplate>
                                            <asp:Label ID="lblitemdesc" runat="server" Text='<%#Bind("Item_Desc") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderTargetPrice" Text="Expected Price" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblTargetPrice" Text='<%#Bind("Expected_price") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:HiddenField ID="lblQTYPO" Value='<%#Bind("QTY_perOrder") %>' runat="server"></asp:HiddenField>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label Text="Order Valildity" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblOrdValidity" Text='<%#Bind("OrderValidity") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label Text="Break Quantity" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblOrderQuant" Text='<%#Bind("Approved_OrderQty") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label Text="MOQ" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblMOQ" Text='<%#Bind("MOQ") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label Text="System Price" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblOfferPrice" runat="server" Text='<%#Bind("Offer_price") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label Text="Special Price" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblNewOfferPrice" runat="server" Text='<%#Bind("New_OfferPrice") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--    <asp:TemplateField HeaderStyle-CssClass="cssComment">
                                <HeaderTemplate>
                                    <asp:Label ID="lblComment" Text="Comment" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox Rows="2" Columns="20" ID="txtComment" TextMode="MultiLine" runat="server" ></asp:TextBox>
                                    <asp:Label ID="lblmsg" runat="server" Style="color: red;"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label Text="Actions" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                                <div style="width: 90px;">
        
                                               <asp:ImageButton ToolTip="info Details" ImageUrl="images/info.png" runat="server" ID="ImageButton1" OnClientClick="return OpenStatusLog(event,this);" Style="height: 25px;" />
                                                <asp:ImageButton ToolTip="View Details" ImageUrl="images/icons/view_details.png" runat="server" ID="btnView" OnClientClick="return OpenDetailView(event,this);" Style="height: 25px;" />
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>

                             <div id="divdetail" style="display: none;">
                        <table id="grdDetailedQuoteSummary" class="display responsive nowrap" cellpadding="0" cellspacing="0" style="width:100%;">
                            <thead style="background-color: #DC5807; color: White; font-weight: bold">
                                <tr style="border: solid 1px #000000">
                                     <td>Action</td>
                                    <td>CP Name</td>
                                    <td>Reference No</td>
                                     <td>Item Code</td>
                                    <td>Item Desc</td>
                                    <td>Expected Price</td>
                                     <td>Order Validity</td>
                                    <td>Break Quantity</td>
                                    <td>MOQ</td>
                                    <td>System price</td>
                                    <td>Special Price</td>
                                    <td>Status</td>
                                    <td>Action</td>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>

                        </div>

                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                        <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color: #fff">Please wait</span>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>

    </div>
</asp:Content>
