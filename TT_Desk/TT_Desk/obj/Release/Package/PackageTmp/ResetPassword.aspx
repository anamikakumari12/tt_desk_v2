﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ResetPassword.aspx.cs" Inherits="TT_Desk.ResetPassword" MasterPageFile="~/SiteMaster.Master" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .toppadding {
            margin-top: 10px;
        }
    </style>
    <script type="text/javascript">
        function validateControls() {
            //debugger
            var err_flag = 0;
            var err_old = 0;
            if ($('#body_txtOld').val() == "") {
                $('#body_txtOld').css('border-color', 'red');
                err_flag = 1;
            }
            else {
                $('#body_txtOld').css('border-color', '');
                //err_old = checkOldPassword();
            }
            if ($('#body_txtNew').val() == "") {
                $('#body_txtNew').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#body_txtNew').css('border-color', '');
            }

            if ($('#body_txtConfirm').val() == "") {
                $('#body_txtConfirm').css('border-color', 'red');

                err_flag = 1;
            }
            else {
                $('#body_txtConfirm').css('border-color', '');
            }


            if (err_flag == 0) {
                if (err_old == 0) {
                    $('#body_lblError').text('');
                    return checkPasswordMatch();
                 }
                else {
                    $('#body_txtOld').css('border-color', 'red');
                    //$('#body_lblError').text('Please enter correct old password.');
                    return false;
                }
            }
            else {
                $('#body_lblError').text('Please enter all the mandatory fields.');
                return false;
            }
        }

        function checkPasswordMatch() {
            var pwd = $('#body_txtNew').val();
            var repwd = $('#body_txtConfirm').val();
            if (pwd == repwd) {
                $('#body_lblError').text('');
                return true;

            }
            else {
                $('#body_lblError').text('New Password and Confirm Password does not match.');
                return false;
            }
        }
        <%--function checkOldPassword() {
            var val = "<%= Session["LoggedInPassword"]%>";
            if ($('#body_txtOld').val() == val) {
                $('#body_txtOld').css('border-color', '');
                $('#body_lblError').text('');
                return 0;
            }
            else {
                $('#body_txtOld').css('border-color', 'red');
                $('#body_lblError').text('Please enter correct old password.');
                return 1;
            }
        }--%>
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <%--<form id="form1" runat="server">--%>
    <div class="filter_panel form-boxRest" style="width: 60%; margin-left: 20%;">
        <%--    <form id="Form1" action="#" class="login-form" runat="server">--%>

        <div class="col-md-12 toppadding">
            <div class="col-md-5 nopad">
                <p><b>Old Password* :</b></p>
            </div>
            <div class="col-md-7 nopad">
                <%-- <asp:TextBox ID="txtOld" runat="server" placeholder="txtOld" name="password" type="password" CssClass="form-control"></asp:TextBox>--%>
                <asp:TextBox ID="txtOld" runat="server" class="form-control1 mn_inp control3" TextMode="Password"></asp:TextBox>
            </div>
        </div>


        <asp:Label ID="lblresult" Text="" runat="server"></asp:Label>
        <div class="col-md-12 toppadding">
            <div class="col-md-5 nopad">
                <p><b>New Password* :</b></p>
            </div>
            <div class="col-md-7 nopad">
                <asp:TextBox runat="server" ID="txtNew" class="form-control1 mn_inp control3" maxlength="8" TextMode="Password"></asp:TextBox>
            </div>
        </div>


        <div class="col-md-12 toppadding">
            <div class="col-md-5 nopad">
                <p><b>Confirm New Password* :</b></p>
            </div>
            <div class="col-md-7 nopad">
                <asp:TextBox runat="server" ID="txtConfirm" class="form-control1 mn_inp control3" maxlength="8" TextMode="Password"></asp:TextBox>
            </div>
        </div>

        <div class="col-md-12 toppadding">
            <div class="col-md-5 nopad">
                <asp:Label runat="server" ID="lblError" ForeColor="Red"></asp:Label>
            </div>
            <div class="col-md-7 nopad">
                <asp:Button runat="server" ID="btnReset" Text="Change Password" CssClass="login100-form-btn login" OnClick="btnReset_Click" OnClientClick="return validateControls();" style="background: #3fab76;color: white;border-radius: 17px;width: 134px;" />
                
            </div>
            
        </div>
    </div>
    <%--  </form>--%>
    <%-- </form>--%>
</asp:Content>
