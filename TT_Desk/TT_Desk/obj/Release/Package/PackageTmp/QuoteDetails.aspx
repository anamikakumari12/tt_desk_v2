﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QuoteDetails.aspx.cs" Inherits="TT_Desk.QuoteDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

	<link href="css/style.css" rel="stylesheet" />
	<link href="vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
	<link href="css/jquery-ui.css" rel="stylesheet" />
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="js/jquery-ui.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<title>Quote Details</title>
	<style type="text/css">
		.title
		{
			margin: 5px 0;
			width: 100%;
			font-size: large;
			font-weight: bold;
			padding: 10px;
			display: block;
			text-align: center;
		}

		.subtitle
		{
			margin: 5px 0;
			width: 100%;
			font-size: large;
			font-weight: bold;
			display: block;
			text-align: center;
		}

		table
		{
			width: 100%;
		}

		.divhead
		{
			font-weight: bold;
			/*float: right;*/
		}

		.quete_details table td
		{
			border: solid 1px #ddd;
			padding: 2px;
		}

		.quote_d_middle
		{
			margin: 10px 0;
			background: #8cffc6;
			padding: 0px 0;
		}

		.quete_comment
		{
			width: 100%;
		}

			.quete_comment textarea
			{
				width: 100%;
				margin: 5px 0;
				height: 80px;
			}

		.quete_details input
		{
			width: 100%;
		}

		.quete_details select
		{
			width: 100%;
		}

		.cp_name
		{
			line-height: 24px;
		}
	</style>
	<script type="text/javascript">
	    $(document).ready(function () {
	        debugger;
	        var ordervalidity = sessionStorage.getItem('ordervalidity');
	        $('#txtOrdValidity').val(ordervalidity);

	        var orderqty = sessionStorage.getItem('orderqty');
	        $('#txtOrderQuant').val(orderqty);
	        //var MOQ = sessionStorage.getItem('MOQ');
	        //$('#txtMOQ').val(MOQ);
	        var SP = sessionStorage.getItem('sysPrice');
	        $('#txtNewOfferPrice').val(SP);
	        var comment = sessionStorage.getItem('comment');
	        $('#txtComment').val(comment);
	    });

		function isNumberKey(evt, obj) {

			var charCode = (evt.which) ? evt.which : event.keyCode
			var value = obj.value;
			var dotcontains = value.indexOf(".") != -1;
			if (dotcontains) {
				var match = ('' + value).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
				if (!match) { return 0; }
				var decCount = Math.max(0,
					 // Number of digits right of decimal point.
					 (match[1] ? match[1].length : 0)
					 // Adjust for scientific notation.
					 - (match[2] ? +match[2] : 0));
				if (decCount > 1) return false;
				if (charCode == 46) return false;
			}
			else {
				if (value.length > 10) {
					if (charCode == 46) return true;
					else return false;
				}
			}
			if (charCode == 46) return true;
			if (charCode > 31 && (charCode < 48 || charCode > 57))
				return false;
			return true;
		}
		function IsnullOrEmpty(val) {
			if (val != '' && val != undefined && val != '--Select--')
				return false;
			else
				return true;
		}
		function validateFields(evt, obj) {
			debugger;
			var errFlag = 0;
			var id = obj.id;
			var comment_id;
			var lbl;
			var offerprice_id;
			var ref_id;
			var refnum_id;
			var item_id;
			var MOQ_id;
			var multiflag_id;
			var status;
			var RMOQ_id;
			var MOQ
			var RMOQ;
			var lblMOQmsg_id;
			var stock_id;
			var systemprice_id;
			var txtOrderQuant = $("#txtOrderQuant").val();
			//var expiry_id;
			if (id.includes("btnReject")) {
				comment_id = id.replace("btnReject", "txtComment");
				lbl = id.replace("btnReject", "lblmsg");
				offerprice_id = id.replace("btnReject", "txtNewOfferPrice");
				ref_id = id.replace("btnReject", "hdnID");
				refnum_id = id.replace("btnReject", "lblref");
				item_id = id.replace("btnReject", "hdnitem");
				//MOQ_id = id.replace("btnReject", "txtMOQ");
				//RMOQ_id = id.replace("btnReject", "lblReqMOQ");
			   // multiflag_id = id.replace("btnReject", "chkMulti");
				//MOQ = $("#" + MOQ_id).val();
				////expiry_id = id.replace("btnReject", "txtExpiryDate");
				//if (IsnullOrEmpty(MOQ)) {
				//	MOQ = "";
				//}
				if (IsnullOrEmpty($("#" + comment_id).val())) {
					$("#" + comment_id).css("border", "1px solid red");
					$("span#" + lbl).text("*");
					$("#body_lblErrMsg").text("*Comment is mandatory.");
					errFlag++;
				}
				else {
					$("span#" + lbl).text("");
					$("#" + comment_id).css("border", "");
					status = "Rejected";
				}
			}
			else if (id.includes("btnApprove")) {
				comment_id = id.replace("btnApprove", "txtComment");
				offerprice_id = id.replace("btnApprove", "txtNewOfferPrice");
				systemprice_id = id.replace("btnApprove", "lblOfferPrice");
				targetprice_id = id.replace("btnApprove", "lblEP");
				lbl = id.replace("btnApprove", "lblpricemsg");
				ref_id = id.replace("btnApprove", "hdnID");
				refnum_id = id.replace("btnApprove", "lblref");
				item_id = id.replace("btnApprove", "hdnitem");
				//MOQ_id = id.replace("btnApprove", "txtMOQ");
				//RMOQ_id = id.replace("btnApprove", "lblReqMOQ");
				////multiflag_id = id.replace("btnApprove", "chkMulti");
				//lblMOQmsg_id = id.replace("btnApprove", "lblMOQmsg");
				////expiry_id = id.replace("btnApprove", "txtExpiryDate");
				//MOQ = $("#" + MOQ_id).val();
				//RMOQ = $("#" + RMOQ_id).text();
				stock_id = id.replace("btnApprove", "lblStockCode");
				var stock = $("#" + stock_id).text();
				//if (IsnullOrEmpty(MOQ)) {
				//	if (stock == "1" || stock == "6") {
				//		MOQ = RMOQ;
				//		$("span#" + lblMOQmsg_id).text("");
				//		$("#" + MOQ_id).css("border", "");
				//	}
				//	else {
				//		$("#" + MOQ_id).css("border", "1px solid red");
				//		$("span#" + lblMOQmsg_id).text("*");
				//		err_comment ="*MOQ is mandatory.";
				//		errFlag++;

				//	}
				//}

				//else if (parseInt(MOQ) > parseInt(txtOrderQuant)) {
				//    $("#txtOrderQuant").css("border", "1px solid red");
				//	$("span#" + lblMOQmsg_id).text("*");
				//	err_comment = "*Break quantity should be equal to or greater than MOQ.";
				//	errFlag++;
				//}
				//else {
				//	$("span#" + lblMOQmsg_id).text("");
				//}
				if (IsnullOrEmpty($("#" + offerprice_id).val())) {
					$("#" + offerprice_id).css("border", "1px solid red");
					$("span#" + lbl).text("*");
					err_comment ="*Special Price is mandatory.";
					errFlag++;
				}
				else {
					var offerprice = $("#" + offerprice_id).val();
					/*var targetprice = $("#" + targetprice_id).text();*/
					var systemprice = $("#" + systemprice_id).val();
					//if (parseFloat(parseFloat(targetprice).toFixed(2)) > parseFloat(parseFloat(offerprice).toFixed(2))) {
					//	//if (targetprice > offerprice) {
					//	$("#" + offerprice_id).css("border", "1px solid red");
					//	$("span#" + lbl).text("*");
					//	err_comment = "*Price should be equal to or greater than Expected Price.";
					//	errFlag++;
					//}
					if (parseFloat(parseFloat(systemprice).toFixed(2)) > parseFloat(parseFloat(offerprice).toFixed(2))) {
						//if (targetprice > offerprice) {
						$("#" + offerprice_id).css("border", "1px solid red");
						$("span#" + lbl).text("*");
						err_comment = "*Price should be equal to or greater than System Price.";
						errFlag++;
					}
					else {
						$("#" + offerprice_id).css("border", "");
						$("span#" + lbl).text("");
						status = "Approved";
					}
				}
			}
			if (errFlag > 0) {
			    
			    $("#lblmsg").text(err_comment);
			    return false;
			}
			else {
			    debugger;
			    $("#lblmsg").text("");
				var ref = $("#" + ref_id).val();
				var refnum = $("#" + refnum_id).text();
				var item = $("#" + item_id).val();
				var txtOrdValidity = $("#txtOrdValidity").val();
				
				var ddlOrderFreq = '';
                    //$("#ddlOrderFreq").val();
				//var expiry_date = $("#" + expiry_id).val();
				var multiflag=1;
				//if ($("#" + multiflag_id).is(':checked'))
				//	multiflag = 1;
				//else
				//	multiflag = 0;
				var offerprice = $("#" + offerprice_id).val();
				var comment = $("#" + comment_id).val();
				var objList = [];
				var obj = {
					"id": ref
					, "item": item
					, "ref_no": refnum
					, "status": status
					, "comment": comment
                    , "MOQ": txtOrderQuant
					, "offerPrice": offerprice
					, "multiorder_flag": multiflag
					, "Order_Validity": txtOrdValidity
					, "Approved_OrderQty": txtOrderQuant
					, "Approved_OrderFreq": ddlOrderFreq
					//, "expiry_date": expiry_date
				};
				objList.push(obj);
				var param = {"objList": objList };
				$("#hdnparam").val(JSON.stringify(param));
				//$.ajax({
				//	url: 'PendingTasks.aspx/UpdateQuoteStatus',
				//	method: 'post',
				//	datatype: 'json',
				//	data: param,
				//	//data: '{id:"' + ref + '", item:"' + item + '", ref_no:"' + refnum + '", status:"' + status + '", comment:"' + comment + '", MOQ:"' + MOQ + '", offerPrice:"' + offerprice + '", multiorder_flag:' + multiflag + '}',
				//	contentType: "application/json; charset=utf-8",
				//	success: function (msg) {
				//		msg = JSON.parse(msg.d);
				//		alert(msg.msg);
				//		window.location.reload();
				//	},
				//	error: function (xhr, ajaxOptions, thrownError) {
				//		alert("Error");
				//	}
			    //});
				return true;
			}
		}
    </script>
</head>

<body>
	<form id="form1" runat="server">
		<div style="background: #3fab76;">
			<asp:Label ID="lblTitle" CssClass="title" Text="QUOTE DETAILS" runat="server"></asp:Label>
		</div>
		<div>
			<asp:HiddenField runat="server" ID="hdnparam" />
			<asp:HiddenField runat="server" ID="hdnID" />
			<asp:HiddenField runat="server" ID="hdnitem" />
		</div>
		<div class="cp_name">
			<div class="col-sm-12">
				<div class="row">
					<div class="col-sm-3 divhead">CP Name : </div>
					<div class="col-sm-9">
						<asp:Label ID="lblCP" runat="server"></asp:Label>
					</div>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="row">
					<div class="col-sm-3 divhead">CP Customer : </div>
					<div class="col-sm-9">
						<asp:Label ID="lblCustomer" runat="server"></asp:Label>
					</div>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="row">
					<div class="col-sm-3 divhead">RFQ Number :</div>
					<div class="col-sm-9">
						<asp:Label ID="lblref" runat="server"></asp:Label>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-12 quete_details">
			<table>
				<tr>
					<td width="25%" class="divhead">TTIL ITEM NUMBER : </td>
					<td width="25%" class="">
						<asp:Label ID="lblItem" runat="server"></asp:Label>
					</td>
                    <td class=" divhead">PREV.RFQ QTY : </td>
					<td class="">
						<asp:Label ID="lblTotalQty" runat="server"></asp:Label>
					</td>
					<%--<td width="25%" class="divhead">COMPETITOR PRODUCT : </td>
					<td width="25%" class="">
						<asp:Label ID="lblComP" runat="server"></asp:Label>
					</td>--%>
				</tr>

				<tr>
					<td class=" divhead">STOCK CODE : </td>
					<td class="">
						<asp:Label ID="lblStockCode" runat="server"></asp:Label>
					</td>
					<td class=" divhead">LAST. SUPP.PRICE : </td>
					<td class="">
						<asp:Label ID="lblLSP" runat="server"></asp:Label>
					</td>
                   <%-- <td class=" divhead">ORDER PER QUANTITY : </td>
					<td class="">
						<asp:Label ID="lblReqMOQ" runat="server"></asp:Label>
					</td>--%>
					<%--<td class=" divhead">COMPETITOR NAME : </td>
					<td class="">
						<asp:Label ID="lblComC" runat="server"></asp:Label>
					</td>--%>
				</tr>

				<tr>
					<td class=" divhead">WHS : </td>
					<td class="">
						<asp:Label ID="lblWHS" runat="server"></asp:Label>
					</td>
					 <td class=" divhead">DLP : </td>
					<td class="">
						<asp:Label ID="lblLP" runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					<td class=" divhead">STOCK : </td>
					<td class="">
						<asp:Label ID="lblStock" runat="server"></asp:Label>
					</td>
					<td class=" divhead">END CUSTOMER PRICE : </td>
					<td class="">
						<asp:Label ID="lblSP" runat="server"></asp:Label>
					</td>
					<%-- <td class=" divhead">EXP.PRICE  : </td>
					<td class="">
						<asp:Label ID="lblEP" runat="server"></asp:Label>
					</td>--%>
				</tr>
				<%--<tr>
					<td class=" divhead">COMPETITOR SUPPLY PRICE : </td>
					<td class="">
						<asp:Label ID="lblComSP" runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					
				</tr>--%>
				<tr>
					<td class=" divhead">AVG.SELL PRICE : </td>
					<td class="">
						<asp:Label ID="lblaveSell" runat="server"></asp:Label>
					</td>
					<td class="divhead">UNIT COST : </td>
					<td class="">
						<asp:Label ID="lblUnitPrice" runat="server"></asp:Label>
					</td>
                  <%-- <td class=" divhead">EXP. DISC % : </td>
					<td class="">
						<asp:Label ID="lblDR" runat="server"></asp:Label>
					</td>--%>
					<%--<td class=" divhead">REQUIRED ORDER FREQUENCY : </td>
					<td class="">
						<asp:Label ID="lblOrderFrequency" runat="server"></asp:Label>
					</td>--%>
				</tr>
			<%--	<tr>
					<td class=" divhead">LAST. SUPP.PRICE : </td>
					<td class="">
						<asp:Label ID="lblLSP" runat="server"></asp:Label>
					</td>
					<td class="divhead">UNIT COST : </td>
					<td class="">
						<asp:Label ID="lblUnitPrice" runat="server"></asp:Label>
					</td>
				</tr>--%>
                <tr>
                   <%-- <td class=" divhead">DLP : </td>
					<td class="">
						<asp:Label ID="lblLP" runat="server"></asp:Label>
					</td>--%>
                    <td class=" divhead"><asp:Label ID="lblCom_head" runat="server"></asp:Label></td>
					<td class="">
                        <asp:Label ID="lblComment" runat="server"></asp:Label>
					</td>
                </tr>
			</table>
		</div>
		<div class="col-sm-12 text-center quote_d_middle">
			<asp:Label ID="lblSubTitle" CssClass="subtitle" Text="TTIL OFFER" runat="server"></asp:Label>
		</div>
		<div class="col-sm-12 quete_details">
			<table>
				<tr>
					<%--<td width="25%" class=" divhead">MOQ : </td>
					<td width="25%" class="">
						<asp:TextBox Enabled="false" ID="txtMOQ" runat="server" onkeypress="return isNumberKey(event,this);"></asp:TextBox>
					</td>--%>
					<td class=" divhead">Break Quantity : </td>
					<td class="">
						<asp:TextBox Enabled="false" ID="txtOrderQuant" runat="server" onkeypress="return isNumberKey(event,this);"></asp:TextBox>
						<%--<asp:RangeValidator Id="valOrderQuant" ErrorMessage="Quantity should be greater than 0" ForeColor="Red" ControlToValidate="txtOrderQuant" MaximumValue="100" MinimumValue="1" runat="server"></asp:RangeValidator>--%>
					</td>
					 <td class=" divhead">Order Validity : </td>
					<td class="">
						<asp:TextBox Enabled="false" ID="txtOrdValidity" runat="server" onkeypress="return isNumberKey(event,this);"></asp:TextBox>
						<%--<asp:RangeValidator Id="valOrdValidity" ErrorMessage="Validity should be greater than 0" ForeColor="Red" ControlToValidate="txtOrdValidity" MaximumValue="100" MinimumValue="1" runat="server"></asp:RangeValidator>--%>
					</td>
					
				</tr>
				<tr>
					<%--<td class=" divhead">ORDER FREQUENCY : </td>
					<td class="">
						<asp:DropDownList ID="ddlOrderFreq" runat="server">
							<asp:ListItem Text="Weekly" Value="weekly">
							</asp:ListItem>
							<asp:ListItem Text="Fortnightly" Value="fortnightly">
							</asp:ListItem>
							<asp:ListItem Text="Monthly" Value="monthly">
							</asp:ListItem>
						</asp:DropDownList>
					</td>--%>
                   
					<td width="25%" class="divhead">SYSTEM PRICE : </td>
					<td width="25%" class="">
						<asp:Label ID="lblOfferPrice" runat="server"></asp:Label>
					</td>
                  
					<td class=" divhead">SPECIAL PRICE : </td>
					<td class="">
						<asp:TextBox Enabled="false" ID="txtNewOfferPrice" runat="server" onkeypress="return isNumberKey(event,this);"></asp:TextBox>
					</td>
				</tr>
				<tr>
					
                      <td class=" divhead"></td>
					<td class="">
					<%--	<asp:Label ID="lblGP" runat="server"></asp:Label>--%>
					</td>					
				</tr>
				<%--<tr>					
					<td class=" divhead">Line Value : </td>
					<td class="">
						<asp:Label ID="lbltotalOrderValue" runat="server"></asp:Label>
					</td>
				</tr>--%>
			</table>
		</div>
		<div class="col-sm-12 quete_comment">

			<asp:TextBox Rows="2" Columns="20" ID="txtComment" TextMode="MultiLine" runat="server" onkeyup="setTextareaVal(event,this);"></asp:TextBox>
			<asp:Label ID="lblCommentmsg" runat="server" Style="color: red;"></asp:Label>
		</div>

        <div>
            <asp:Label ID="lblmsg" ForeColor="Red" runat="server"></asp:Label>
        </div>
		<div class="col-sm-12 quete_comment">
			<div class="row">
				<div class="col-sm-6">
					<asp:Button  style="float:right" OnClientClick="return validateFields(event,this);" CommandName="Approve" Text="Add" ID="btnApprove" CssClass="btn btn-success button pull-right" runat="server" OnClick="btnApprove_Click" />
				</div>
				<div class="col-sm-6">
					<%--<asp:Button OnClientClick="return validateFields(event,this);" CommandName="Reject" CommandArgument='<%#Bind("ID") %>' Text="Reject" ID="btnReject" CssClass="btn btn-danger button pull-right" runat="server" />--%>
				</div>
			</div>
		</div>
		<%-- <div>
				<div class="divhead">Order type : </div>
				<div>
					<asp:Label ID="lblOrderType" runat="server"></asp:Label>
				</div>
			</div>
			<div>
				<div class="divhead">List Price : </div>
				<div>
					<asp:Label ID="lblLP" runat="server"></asp:Label>
				</div>
			</div>
		--%>
	</form>
</body>
</html>
