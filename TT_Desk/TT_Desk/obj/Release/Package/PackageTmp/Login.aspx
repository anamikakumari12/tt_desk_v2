﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="TT_Desk.Login" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login Desk</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.ico" />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <!--===============================================================================================-->
    <style>
        .login {
            background: #3e7739;
            background: -webkit-linear-gradient(right, #3e7739, #2c632c, #76b068, #2c632c);
            transition: all 0.4s;
        }
    </style>
    <script type="text/javascript">
        function Login() {
            window.open("PendingTasks.aspx", "", "", false);
        }
    </script>
</head>
<body>

    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <form class="login100-form validate-form" runat="server">
                    <span class="login100-form-title p-b-26">Welcome
                    </span>
                    <span class="login100-form-title p-b-48">
                        <img src="images/icons/admin.png" style="width: 100px;" />
                        <%--<i class="zmdi zmdi-font"></i>--%>
                    </span>

                    <div class="wrap-input100 validate-input" data-validate="Valid email is: a@b.c">
                        <asp:TextBox ID="txtemail" runat="server" CssClass="input100"></asp:TextBox>
                        <%--<input class="input100" type="text" name="email">--%>
                        <span class="focus-input100" data-placeholder="Email"></span>
                    </div>

                    <div class="wrap-input100 validate-input" data-validate="Enter password">
                        <span class="btn-show-pass">
                            <i class="zmdi zmdi-eye"></i>
                        </span>
                        <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" CssClass="input100"></asp:TextBox>
                        <%--<input class="input100" type="password" name="pass">--%>
                        <span class="focus-input100" data-placeholder="Password"></span>
                    </div>

                    <div class="container-login100-form-btn">
                        <div class="wrap-login100-form-btn">
                            <div class="login100-form-bgbtn"></div>
                            <%-- <button class="login100-form-btn" onclick="Login();">
                                Login
                            </button>--%>
                            <asp:Button ID="btnLogin" runat="server" CssClass="login100-form-btn login" Text="Login" OnClick="btnLogin_Click" />

                        </div>
                        <div class="form-group-1">
                            <asp:ValidationSummary ID="valSum" runat="server" ValidationGroup="VGLogin" ForeColor="Red" />
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="VGRequest" />
                            <asp:LinkButton ValidationGroup="VGRequest" Text="Request Password" ID="lnkRequest" runat="server" OnClick="lnkRequest_Click"></asp:LinkButton>
                        </div>
                        <%--<a class="txt2" href="#">
							Request Password
						</a>--%>
                    </div>

                    <%--<div class="text-center p-t-115">
						<span class="txt1">
							Don’t have an account?
						</span>

						<a class="txt2" href="#">
							Sign Up
						</a>
					</div>--%>
                </form>
            </div>
        </div>
    </div>


    <div id="dropDownSelect1"></div>

    <!--===============================================================================================-->
    <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/animsition/js/animsition.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/bootstrap/js/popper.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/select2/select2.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/daterangepicker/moment.min.js"></script>
    <script src="vendor/daterangepicker/daterangepicker.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/countdowntime/countdowntime.js"></script>
    <!--===============================================================================================-->
    <script src="js/main.js"></script>

</body>
</html>
