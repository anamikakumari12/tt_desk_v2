﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using TTDeskBL;
using TTDeskBO;

namespace TT_Desk
{
    public partial class SiteMaster : System.Web.UI.MasterPage
    {
        CommonFunctions objCom = new CommonFunctions();
        LoginBO objLoginBO;
        LoginBL objLoginBL;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblUserName.Text = Convert.ToString(Session["UserFullName"]);
            }
            objLoginBO = new LoginBO();
            objLoginBL = new LoginBL();
            DataSet ds = new DataSet();
            objLoginBO.emailid = Convert.ToString(Session["LoginMailId"]);
            ds = objLoginBL.GetAllMenusBL(objLoginBO);
            if (ds.Tables.Count > 1)
            {
                var MenuList = ds.Tables[1].AsEnumerable()

                        .Select(row => new
                        {
                            Menu_ID = row.Field<int>("Menu_ID"),
                            Menu = row.Field<string>("Menu"),
                        })
                        .Distinct().AsEnumerable();
                foreach (var drMenu in MenuList)
                //for(int i=0;i<MenuList.Count;i++)
                {
                    var SubMenuList = ds.Tables[1].AsEnumerable()
                        .Where(dataRow => dataRow.Field<int>("Menu_ID") == drMenu.Menu_ID && dataRow.Field<int>("SubMenu_ID") > 0)
                      .Select(row => new
                      {
                          SubMenu_ID = row.Field<int>("SubMenu_ID"),
                          SubMenu = row.Field<string>("SubMenu_Name"),
                          URL = row.Field<string>("URL")
                      })
                       .Distinct().AsEnumerable();
                    HtmlGenericControl li = new HtmlGenericControl("li");
                    tabs.Controls.Add(li);
                    HtmlGenericControl anchor = new HtmlGenericControl("a");

                    anchor.InnerText = Convert.ToString(drMenu.Menu);
                    if (SubMenuList.ToList().Count > 0)
                    {

                        anchor.Attributes.Add("href", "#");
                    }
                    else
                    {
                        var MenuURL = ds.Tables[1].AsEnumerable()
                        .Where(dataRow => dataRow.Field<int>("Menu_ID") == drMenu.Menu_ID && dataRow.Field<int>("SubMenu_ID") == 0)
                      .Select(row => new
                      {
                          URL = row.Field<string>("URL")
                      })
                       .Distinct().AsEnumerable();
                        foreach (var url in MenuURL)
                        {
                            anchor.Attributes.Add("href", Convert.ToString(url.URL));
                        }
                    }
                    li.Controls.Add(anchor);
                    if (SubMenuList.ToList().Count > 0)
                    {
                        HtmlGenericControl ul = new HtmlGenericControl("ul");
                        foreach (var subMenu in SubMenuList)
                        {
                            HtmlGenericControl ili = new HtmlGenericControl("li");
                            ul.Controls.Add(ili);
                            HtmlGenericControl ianchor = new HtmlGenericControl("a");
                            ianchor.Attributes.Add("href", Convert.ToString(subMenu.URL));
                            ianchor.InnerText = Convert.ToString(subMenu.SubMenu);
                            ili.Controls.Add(ianchor);
                        }

                        li.Controls.Add(ul);
                    }
                }
            }
        }

        protected void lnkbtnLogout_Click(object sender, EventArgs e)
        {
            if (Session["UserFullName"] != null)
            {

                Session["LoginMailId"] = null;
                Session["EscalatedQuotes"] = null;
                Session["UserFullName"] = null;
                Session["UserId"] = null;
                Session["PendingQuotes"] = null;
                Session["AcceptedQuotes"] = null;
                Session["dtQuoteDetails"] = null;

                Session.Clear();
                Session.Abandon();
                Session.RemoveAll();
                Response.Redirect("Login.aspx");

                //ClientScript.RegisterStartupScript(typeof(Page), "closePage", "window.close();", true);
            }
        }

        

       
    }
}