﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TTDeskBO;

namespace TT_Desk
{
    public partial class QuoteDetails : System.Web.UI.Page
    {
        #region GlobalDeclaration
        CommonFunctions objCom = new CommonFunctions();
        #endregion

        #region Events

        /// <summary>
        /// Author : Anamika Kumari
        /// Date : Jan 15, 2020
        /// Desc : Load all the details for selected quote item from Session value. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["LoginMailId"]))) { Response.Redirect("Login.aspx"); return; }

            if (!IsPostBack)
            {
                try
                {
                    string id = Convert.ToString(Request.QueryString["id"]);
                    string Escid = Convert.ToString(Request.QueryString["Escid"]);
                    if (string.IsNullOrEmpty(id) && string.IsNullOrEmpty(Escid))
                    {
                        lblmsg.Text = "There is a problem fetching details for selected quote. Please try again.";
                    }
                    if (!string.IsNullOrEmpty(id))
                    {
                        DataTable dt = new DataTable();
                        dt = (DataTable)Session["PendingQuotes"];
                        if (dt != null)
                        {
                            if (dt.Rows.Count > 0)
                            {
                                var rows = from row in dt.AsEnumerable()
                                           where row.Field<int>("ID") == Convert.ToInt32(id)
                                           select row;
                                dt = rows.CopyToDataTable();
                                if (dt.Rows.Count > 0)
                                {
                                    hdnitem.Value = Convert.ToString(dt.Rows[0]["Item_code"]);
                                    hdnID.Value = Convert.ToString(dt.Rows[0]["ID"]);
                                    lblref.Text = Convert.ToString(dt.Rows[0]["Ref_number"]);
                                    lblItem.Text = String.Concat(Convert.ToString(dt.Rows[0]["Item_Desc"]));
                                    lblWHS.Text = Convert.ToString(dt.Rows[0]["WHS"]);
                                    //lblOrderType.Text = Convert.ToString(dt.Rows[0]["Order_type"]);
                                    //lblOrderFrequency.Text = Convert.ToString(dt.Rows[0]["Order_frequency"]);
                                    lblTotalQty.Text = Convert.ToString(dt.Rows[0]["Total_QTY"]);
                                    lblLP.Text = Convert.ToString(dt.Rows[0]["List_Price"]);
                                    //lblEP.Text = Convert.ToString(dt.Rows[0]["Expected_price"]);
                                    //lblDR.Text = Convert.ToString(dt.Rows[0]["DC_rate"]);
                                    lblCP.Text = Convert.ToString(dt.Rows[0]["CP_Name"]);
                                    lblCustomer.Text = Convert.ToString(dt.Rows[0]["Cust_Name"]);
                                    lblSP.Text = Convert.ToString(dt.Rows[0]["Cust_SP"]);
                                    //lblComC.Text = Convert.ToString(dt.Rows[0]["Comp_Name"]);
                                    //lblComP.Text = Convert.ToString(dt.Rows[0]["Comp_Desc"]);
                                    //lblComSP.Text = Convert.ToString(dt.Rows[0]["Comp_SP"]);
                                    //lblReqMOQ.Text = Convert.ToString(dt.Rows[0]["QTY_perOrder"]);
                                    lblaveSell.Text = Convert.ToString(dt.Rows[0]["AverageSellingPrice"]);
                                    lblUnitPrice.Text = Convert.ToString(dt.Rows[0]["UnitPrice"]);
                                    lblStock.Text = Convert.ToString(dt.Rows[0]["Stock"]);
                                    lblStockCode.Text = Convert.ToString(dt.Rows[0]["StockCode"]);
                                    txtOrdValidity.Text = Convert.ToString(dt.Rows[0]["OrderValidity"]);
                                    //lblGP.Text = Convert.ToString(dt.Rows[0]["GP"]);
                                    //lbltotalOrderValue.Text = Convert.ToString(dt.Rows[0]["TotOrderValue"]);
                                    lblOfferPrice.Text = Convert.ToString(dt.Rows[0]["Offer_price"]);

                                    //if (Convert.ToString(dt.Rows[0]["StockCode"]) == "1" || Convert.ToString(dt.Rows[0]["StockCode"]) == "6")
                                    //{
                                    //    txtMOQ.Enabled = false;
                                    //}
                                    //else
                                    //{
                                    //    txtMOQ.Enabled = true;
                                    //}
                                    //if (Convert.ToString(dt.Rows[0]["SE_Status"]) == "Approved By SE")
                                    //{
                                    //    lblCom_head.Text = "COMMENT BY SE: ";
                                    //    lblComment.Text = Convert.ToString(dt.Rows[0]["CommentBySE"]);
                                    //}
                                    //else
                                    //{
                                    //    lblCom_head.Text = "STATUS BY SE: ";
                                    //    lblComment.Text = Convert.ToString(dt.Rows[0]["SE_Status"]);
                                    //}
                                }
                            }

                        }

                    }
                    if (!string.IsNullOrEmpty(Escid))
                    {
                        DataTable dt = new DataTable();
                        dt = (DataTable)Session["EscalatedQuotes"];
                        if (dt != null)
                        {
                            if (dt.Rows.Count > 0)
                            {
                                var rows = from row in dt.AsEnumerable()
                                           where row.Field<int>("ID") == Convert.ToInt32(Escid)
                                           select row;
                                dt = rows.CopyToDataTable();
                                if (dt.Rows.Count > 0)
                                {
                                    hdnitem.Value = Convert.ToString(dt.Rows[0]["Item_code"]);
                                    hdnID.Value = Convert.ToString(dt.Rows[0]["ID"]);
                                    lblref.Text = Convert.ToString(dt.Rows[0]["Ref_number"]);
                                    lblItem.Text = String.Concat(Convert.ToString(dt.Rows[0]["Item_Desc"]));
                                    lblWHS.Text = Convert.ToString(dt.Rows[0]["WHS"]);
                                    //lblOrderType.Text = Convert.ToString(dt.Rows[0]["Order_type"]);
                                    //lblOrderFrequency.Text = Convert.ToString(dt.Rows[0]["Order_frequency"]);
                                    lblTotalQty.Text = Convert.ToString(dt.Rows[0]["Total_QTY"]);
                                    lblLP.Text = Convert.ToString(dt.Rows[0]["List_Price"]);
                                    //lblEP.Text = Convert.ToString(dt.Rows[0]["Expected_price"]);
                                    //lblDR.Text = Convert.ToString(dt.Rows[0]["DC_rate"]);
                                    lblCP.Text = Convert.ToString(dt.Rows[0]["CP_Name"]);
                                    lblCustomer.Text = Convert.ToString(dt.Rows[0]["Cust_Name"]);
                                    lblSP.Text = Convert.ToString(dt.Rows[0]["Cust_SP"]);
                                    //lblComC.Text = Convert.ToString(dt.Rows[0]["Comp_Name"]);
                                    //lblComP.Text = Convert.ToString(dt.Rows[0]["Comp_Desc"]);
                                    //lblComSP.Text = Convert.ToString(dt.Rows[0]["Comp_SP"]);
                                    //lblReqMOQ.Text = Convert.ToString(dt.Rows[0]["QTY_perOrder"]);
                                    lblaveSell.Text = Convert.ToString(dt.Rows[0]["AverageSellingPrice"]);
                                    lblUnitPrice.Text = Convert.ToString(dt.Rows[0]["UnitPrice"]);
                                    lblStock.Text = Convert.ToString(dt.Rows[0]["Stock"]);
                                    lblStockCode.Text = Convert.ToString(dt.Rows[0]["StockCode"]);
                                    txtOrdValidity.Text = Convert.ToString(dt.Rows[0]["OrderValidity"]);
                                    //lblGP.Text = Convert.ToString(dt.Rows[0]["GP"]);
                                    //lbltotalOrderValue.Text = Convert.ToString(dt.Rows[0]["TotOrderValue"]);
                                    lblOfferPrice.Text = Convert.ToString(dt.Rows[0]["Offer_price"]);

                                    //if (Convert.ToString(dt.Rows[0]["StockCode"]) == "1" || Convert.ToString(dt.Rows[0]["StockCode"]) == "6")
                                    //{
                                    //    txtMOQ.Enabled = false;
                                    //}
                                    //else
                                    //{
                                    //    txtMOQ.Enabled = true;
                                    //}
                                    //if (Convert.ToString(dt.Rows[0]["SE_Status"]) == "Approved By SE")
                                    //{
                                    //    lblCom_head.Text = "COMMENT BY SE: ";
                                    //    lblComment.Text = Convert.ToString(dt.Rows[0]["CommentBySE"]);
                                    //}
                                    //else
                                    //{
                                    //    lblCom_head.Text = "STATUS BY SE: ";
                                    //    lblComment.Text = Convert.ToString(dt.Rows[0]["SE_Status"]);
                                    //}
                                }
                            }

                        }

                    }
                }
                catch (Exception ex)
                {
                    lblmsg.Text = "Error in loading. Please try again.";
                    objCom.ErrorLog(ex);
                }
            }
           
        }

        /// <summary>
        /// Author : Anamika Kumari
        /// Date : Mar 11, 2020
        /// Desc : Load all the details for selected quote item from Session value. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnApprove_Click(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(Page.GetType(), "scr", "window.opener.parentFunction('" + hdnparam.Value + "'); window.close();", true);
        }
        #endregion
    }
}