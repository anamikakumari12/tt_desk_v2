﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using TTDeskBL;
using TTDeskBO;

namespace TT_Desk
{
    public partial class SpecialQuoteSummary : System.Web.UI.Page
    {
        #region GlobalDeclaration
        CommonFunctions objCom = new CommonFunctions();

        #endregion

        #region Evenets
        /// <summary>
        /// Author : Anamika Kumari
        /// Date : Dec 2, 2019
        /// Desc : 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["LoginMailId"]))) { Response.Redirect("Login.aspx"); return; }

            if (!IsPostBack)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "LoadDateRange();", true);
                BindGrid();
            }
        }

        /// <summary>
        /// Author : Anamika Kumari
        /// Date : Dec 2, 2019
        /// Desc : It will call the Bind grid function to load based on date range
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnFilter_Click(object sender, EventArgs e)
        {
            BindGrid();
        }

        #endregion

        #region Methods
        /// <summary>
        /// Author : Anamika Kumari
        /// Date : Dec 2, 2019
        /// Desc : It will fetch the quote details from backend and bind the gridview 
        /// </summary>
        private void BindGrid()
        {
            try
            {
                string start_date = string.Empty;
                string end_date = string.Empty;
                string selectedDate = txtDateRange.Text;
                if (!string.IsNullOrEmpty(selectedDate))
                {
                    if (selectedDate.Contains("/"))
                    {
                        string[] splittedDates = selectedDate.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                        start_date = Convert.ToString(splittedDates[0].TrimEnd().Trim());
                        end_date = Convert.ToString(splittedDates[1].TrimStart().Trim());
                    }
                }
                else
                {
                    end_date = DateTime.Now.ToString("MM/dd/yyyy").Replace("-", "/");
                    start_date = DateTime.Now.AddDays(-7).ToString("MM/dd/yyyy").Replace("-", "/");
                }
                SplQuoteBO objQuoteBO = new SplQuoteBO();
                objQuoteBO.start_date = start_date;
                objQuoteBO.end_date = end_date;
                objQuoteBO.CustomerType = Convert.ToString(ddlcustType.SelectedValue) == "ALL" ? null : Convert.ToString(ddlcustType.SelectedValue);
                objQuoteBO.CustomerNumber = null;
                QuoteBL objQuoteBL = new QuoteBL();
                DataTable dt = objQuoteBL.GetSPLQuotesBL(objQuoteBO);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        grdQuoteSummary.DataSource = dt;
                        grdQuoteSummary.DataBind();
                        if(objQuoteBO.CustomerType=="C")
                        grdQuoteSummary.Columns[3].Visible = false; 
                        else
                            grdQuoteSummary.Columns[3].Visible = true;
                    }
                    else
                    {
                        lblresult.Text = "No quote is available.";
                        grdQuoteSummary.DataSource = null;
                        grdQuoteSummary.DataBind();
                    }
                }
                else
                {
                    lblresult.Text = "No quote is available.";
                    grdQuoteSummary.DataSource = null;
                    grdQuoteSummary.DataBind();
                }
                
            }
            catch (Exception ex)
            {
                // lblresult.Text = "Error in loading. Please try again.";
                lblresult.Text = ex.Message;
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika Kumari
        /// Date : Dec 2, 2019
        /// Desc : Webmethod is called in ajax call to fetch detailed item for RFQ expand
        /// </summary>
        /// <param name="ref_no"></param>
        /// <returns></returns>
        [WebMethod]
        public static string LoadDetailedGrid(string ref_no)
        {
            string output = string.Empty;
            try
            {
                SplQuoteBO obj = new SplQuoteBO();
                obj.Ref_Number = ref_no;
                DataTable dt = QuoteBL.GetSPLItemDetailsBL(obj);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        //var rows = from row in dt.AsEnumerable()
                        //           where row.Field<string>("Ref_number").Trim() == ref_no
                        //           select row;
                        //dt = rows.CopyToDataTable();
                        if (dt.Rows.Count > 0)
                        {
                            output = DataTableToJSONWithStringBuilder(dt);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                CommonFunctions.StaticErrorLog(ex);
            }
            return output;
        }

        [WebMethod]
        public static string DownloadFile(string file)
        {
            string output = string.Empty;
            try
            {
                string filePath = ConfigurationManager.AppSettings["Special_Folder"].ToString() +"Quote_"+ Convert.ToString(file);

                byte[] bytes = File.ReadAllBytes(filePath);

                return Convert.ToBase64String(bytes, 0, bytes.Length);
            }
            catch (Exception ex)
            {
                CommonFunctions.StaticErrorLog(ex);
                return ex.Message;
            }
        }

        [WebMethod]
        public static string DownloadItemFile(string file)
        {
            string output = string.Empty;
            try
            {
                string filePath = file;
                //ConfigurationManager.AppSettings["Special_Folder"].ToString() + "Quote_" + Convert.ToString(file);

                byte[] bytes = File.ReadAllBytes(filePath);

                return Convert.ToBase64String(bytes, 0, bytes.Length);
            }
            catch (Exception ex)
            {
                CommonFunctions.StaticErrorLog(ex);
                return ex.Message;
            }
        }

        /// <summary>
        /// Author : Anamika Kumari
        /// Date : Dec 2, 2019
        /// Desc : Converts datatable to json string
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public static string DataTableToJSONWithStringBuilder(DataTable table)
        {
            var JSONString = new StringBuilder();
            if (table.Rows.Count > 0)
            {
                JSONString.Append("[");
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    JSONString.Append("{");
                    for (int j = 0; j < table.Columns.Count; j++)
                    {
                        if (j < table.Columns.Count - 1)
                        {
                            JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString().Replace('"', ' ') + "\",");
                        }
                        else if (j == table.Columns.Count - 1)
                        {
                            JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString().Replace('"', ' ') + "\"");
                        }
                    }
                    if (i == table.Rows.Count - 1)
                    {
                        JSONString.Append("}");
                    }
                    else
                    {
                        JSONString.Append("},");
                    }
                }
                JSONString.Append("]");
            }
            return JSONString.ToString();
        }

        #endregion

        protected void imgFile_Command(object sender, CommandEventArgs e)
        {
            string output = string.Empty;
            try
            {
                HttpResponse Response = HttpContext.Current.Response;
                if (Convert.ToString(e.CommandArgument) != string.Empty)
                {
                    string filePath = Convert.ToString(e.CommandArgument);
                    if (File.Exists(filePath))
                    {
                        System.IO.FileInfo file = new System.IO.FileInfo(filePath);
                        if (file.Exists)
                        {
                            Response.ContentType = "application/octet-stream";
                            byte[] bts = System.IO.File.ReadAllBytes(filePath);
                            MemoryStream ms = new MemoryStream(bts);
                            Response.Clear();
                            Response.AddHeader("Content-Disposition", "attachment;filename=\"" + Path.GetFileName(filePath) + "\"");
                            Response.TransmitFile(filePath);
                            //Response.End();
                            //HttpContext.Current.ApplicationInstance.CompleteRequest();
                            HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                            HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                            HttpContext.Current.ApplicationInstance.CompleteRequest();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.StaticErrorLog(ex);
            }
        }

        protected void grdQuoteSummary_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton lb = e.Row.FindControl("imgFile") as ImageButton;
                ScriptManager.GetCurrent(this).RegisterPostBackControl(lb);
            }
        }
    }
}