﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;

namespace TT_Desk
{
    /// <summary>
    /// Summary description for FileUpload
    /// </summary>
    public class FileUpload : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            if (context.Request.Files.Count > 0)
            {
                string ID = context.Request.QueryString[0];
                //Fetch the Uploaded File.
                HttpPostedFile postedFile = context.Request.Files[0];
                string path = string.Empty;
                path = (Convert.ToString(ID)) + "/";
                string folder = ConfigurationManager.AppSettings["Special_Folder"].ToString() + (Convert.ToString(ID))+ "/";
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }
                string filename = postedFile.FileName;
                path = Path.Combine(path, filename);
                string filepath = Path.Combine(folder, filename);

                //Set the File Name.
                // string fileName = Path.GetFileName(postedFile.FileName);

                //Save the File in Folder.
                postedFile.SaveAs(filepath);

                //Send File details in a JSON Response.
                string json = new JavaScriptSerializer().Serialize(
                    new
                    {
                        name = filepath
                    });
                context.Response.StatusCode = (int)HttpStatusCode.OK;
                context.Response.ContentType = "text/json";
                context.Response.Write(json);
                context.Response.End();
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}