﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PendingTasks.aspx.cs" Inherits="TT_Desk.PendingTasks" MasterPageFile="~/SiteMaster.Master" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <script src="js/jquery-ui.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <%--    <script src="js/daterangepicker.js"></script>
    <link href="vendor/daterangepicker/daterangepicker.css" rel="stylesheet" />--%>
    <link href="vendor/daterangepicker2/daterangepicker.css" rel="stylesheet" />
    <script src="vendor/daterangepicker2/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" />
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedcolumns/3.2.6/css/fixedColumns.dataTables.min.css" />
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script>
    <link href="css/Tabs.css" rel="stylesheet" />
    <style>
        .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio]
        {
            margin-left:0px;
        }
        .btn
        {
            font-size: 13px;
            padding: 5px;
        }

        table td
        {
            border: none;
        }

        th
        {
            border: none;
        }

        table
        {
            border: none;
        }

        .required:after
        {
            content: " *";
            color: red;
        }

        .cssComment
        {
            min-width: 150px;
        }
    </style>
    <script type="text/javascript">
        var win;
        $(document).ready(function () {
            debugger;
            $('#body_btndownload').hide();
            $('#body_btnMultiApprove').hide();
            $('#body_btnMultiApprove1').hide();
            $('#body_btnMultiAdded').hide();
            
            //var totaltrCount = $("[id*=grdPendingQuote] tr").length;
            //var trCount = $("[id*=grdPendingQuote] td").closest("tr").length;
            //var date_id, hdn_date_id;
            //for (i = 0; i < trCount; i++) {
            //    date_id = "body_grdPendingQuote_txtExpiryDate_" + i;
            //    hdn_date_id = "body_grdPendingQuote_hdnExpiryDate_" + i;
            //    var defDate = $('#' + hdn_date_id).val();
            //    debugger;
            //    $('#' + date_id).daterangepicker({
            //        singleDatePicker: true,
            //        startDate: defDate,
            //        endDate: null,
            //        locale: {
            //            format: 'MM/DD/YYYY'
            //        },
            //        showDropdowns: true,
            //        //minYear: 12,
            //        //maxYear: parseInt(moment().format('YYYY'),10)
            //    }, function(start, end, label) {
            //        debugger;
            //        var id=this.element[0].id;
            //        var hdnid = this.element[0].id.replace("txtExpiryDate", "hdnExpiryDate");
            //        $('#' + hdnid).val(start.format('MM/DD/YYYY'));
            //       // LoadExpiryDate(hdnid, id, start.format('MM/DD/YYYY'));
            //        var name = this.element[0].name;
            //        $('body').find("input[name^='" + name + "']").val(start.format('MM/DD/YYYY'));
            //    });
            //}

            var SelectedStart = sessionStorage.getItem("selectedPendingStart");
            var SelectedEnd = sessionStorage.getItem("selectedPendingEnd");
            var start = (SelectedStart == null ? "'07/20/2019" : SelectedStart);
            var end = (SelectedEnd == null ? moment() : SelectedEnd);
            $('#body_txtDateRange').daterangepicker({
                //opens: 'left',
                autoUpdateInput: true,
                locale: {
                    format: 'MM/DD/YYYY'
                },
                startDate: start,
                endDate: end,
                ranges: {
                    'All Date': ['07/20/2019', moment()],
                    'Last Year': [moment().subtract(1, 'year'), moment()],
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
            }, function (start, end, label) {
                console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
                var start = start.format('MM/DD/YYYY');
                var end = end.format('MM/DD/YYYY');
                sessionStorage.setItem('selectedPendingStart', start);
                sessionStorage.setItem('selectedPendingEnd', end);
            });

            LoadTable();
            //$('#body_grdPendingQuote input[type="checkbox"]').click(function (evt, obj) {
            //    debugger;
            //    console.log('suggested-in-comment', 'click');
           
           $('.checkbox').on('change', function (evt, obj) {
                debugger;
                var id = this.children[0].id.replace('chk_multi', 'hdn_chk_multi');
                if ($('#' + id).val() == "0")
                    $('#' + id).val("1");
                else
                    $('#' + id).val("0");
                var totaltrCount = $("[id*=grdPendingQuote] tr").length;
                var trCount = sessionStorage.getItem("tot_Count");
                    //$("[id*=grdPendingQuote] td").closest("tr").length;
                var multi_btn_flag = 0;
                var chk_id;
                for (i = 0; i < trCount; i++) {
                    chk_id = "body_grdPendingQuote_hdn_chk_multi_" + i;
                    if ($('#' + chk_id).val() == "1") {
                        multi_btn_flag++;
                        break;
                    }
                }
                if (multi_btn_flag > 0) {
                    $('#body_btnMultiApprove').show();
                }
                else {
                    $('#body_btnMultiApprove').hide();
                }

           })
            $('.checkbox1').on('change', function (evt, obj) {
                debugger;
                var id = this.children[0].id.replace('chk_multi1', 'hdn_chk_multi1');
                if ($('#' + id).val() == "0")
                    $('#' + id).val("1");
                else
                    $('#' + id).val("0");
                var totaltrCount = $("[id*=grdEscalatedQuote] tr").length;
                var trCount = sessionStorage.getItem("tot_Count");
                //$("[id*=grdPendingQuote] td").closest("tr").length;
                var multi_btn_flag = 0;
                var chk_id;
                for (i = 0; i < trCount; i++) {
                    chk_id = "body_grdEscalatedQuote_hdn_chk_multi_" + i;
                    if ($('#' + chk_id).val() == "1") {
                        multi_btn_flag++;
                        break;
                    }
                }
                if (multi_btn_flag > 0) {
                    $('#body_btnMultiApprove1').show();
                }
                else {
                    $('#body_btnMultiApprove1').hide();
                }

            })
           $('.chk_multi_add').on('change', function (evt, obj) {
               debugger;
               var id = this.children[0].id.replace('chk_multi', 'hdn_chk_multi');
               if ($('#' + id).val() == "0")
                   $('#' + id).val("1");
               else
                   $('#' + id).val("0");
               var totaltrCount = $("[id*=grdAcceptedQuote] tr").length;
               var trCount = totaltrCount-3;
                   //sessionStorage.getItem("tot_Count");
               //$("[id*=grdPendingQuote] td").closest("tr").length;
               var multi_btn_flag = 0;
               var all_flag = 0;
               var non_flag = 0;
               var chk_id;
               var chk_All;
               chk_All = "body_grdAcceptedQuote_chk_All";
               for (i = 0; i < trCount; i++) {
                   chk_id = "body_grdAcceptedQuote_hdn_chk_multi_" + i;
                   if ($('#' + chk_id).val() == "1") {
                       multi_btn_flag++;
                       all_flag++;
                       //break;
                   }
                   else if ($('#' + chk_id).val() == "0")
                   {
                       non_flag++;
                   }
               }
               if (non_flag > 0)
               {
                   $("input[id=" + chk_All + "]").prop('checked', false);
                   //$('#body_grdAcceptedQuote_chk_All').prop("checked", false);
                   $('#body_grdAcceptedQuote_hdn_chk_All').val("0");
               }
               else
               {
                   $("input[id=" + chk_All + "]").prop('checked', true);
                   //$('#body_grdAcceptedQuote_chk_All').prop("checked", true);
                   $('#body_grdAcceptedQuote_hdn_chk_All').val("1");
               }
               if (multi_btn_flag > 0) {
                   $('#body_btnMultiAdded').show();
                   $('#body_btndownload').show();
               }
               else {
                   $('#body_btnMultiAdded').hide();
                   $('#body_btndownload').hide();
               }

           })

          
        });
        $('.checkbox1').on('change', function (evt, obj) {
            debugger;
            var id = this.children[0].id.replace('chk_multi1', 'hdn_chk_multi1');
            if ($('#' + id).val() == "0")
                $('#' + id).val("1");
            else
                $('#' + id).val("0");
            var totaltrCount = $("[id*=grdEscalatedQuote] tr").length;
            var trCount = $("[id*=grdEscalatedQuote] td").closest("tr").length;
            var multi_btn_flag = 0;
            var chk_id;
            for (i = 0; i < trCount; i++) {
                chk_id = "body_grdEscalatedQuote_hdn_chk_multi_" + i;
                if ($('#' + chk_id).val() == "1") {
                    multi_btn_flag++;
                    break;
                }
            }
            if (multi_btn_flag > 0) {
                $('#body_btnMultiApprove1').show();
            }
            else {
                $('#body_btnMultiApprove1').hide();
            }

        })
        $('.checkbox').on('change', function (evt, obj) {
            debugger;
            var id = this.children[0].id.replace('chk_multi', 'hdn_chk_multi');
            if ($('#' + id).val() == "0")
                $('#' + id).val("1");
            else
                $('#' + id).val("0");
            var totaltrCount = $("[id*=grdPendingQuote] tr").length;
            var trCount = $("[id*=grdPendingQuote] td").closest("tr").length;
            var multi_btn_flag = 0;
            var chk_id;
            for (i = 0; i < trCount; i++) {
                chk_id = "body_grdPendingQuote_hdn_chk_multi_" + i;
                if ($('#' + chk_id).val() == "1") {
                    multi_btn_flag++;
                    break;
                }
            }
            if (multi_btn_flag > 0) {
                $('#body_btnMultiApprove').show();
            }
            else {
                $('#body_btnMultiApprove').hide();
            }

        })
        $('.chk_multi_add').on('change', function (evt, obj) {
            debugger;
            var id = this.children[0].id.replace('chk_multi', 'hdn_chk_multi');
            if ($('#' + id).val() == "0")
                $('#' + id).val("1");
            else
                $('#' + id).val("0");
            var totaltrCount = $("[id*=grdAcceptedQuote] tr").length;
            var trCount = sessionStorage.getItem("tot_Count");
            //$("[id*=grdPendingQuote] td").closest("tr").length;
            var multi_btn_flag = 0;
            var chk_id;
            for (i = 0; i < trCount; i++) {
                chk_id = "body_grdAcceptedQuote_hdn_chk_multi_" + i;
                if ($('#' + chk_id).val() == "1") {
                    multi_btn_flag++;
                    break;
                }
            }
            if (multi_btn_flag > 0) {
                $('#body_btnMultiAdded').show();
                $('#body_btndownload').show();
            }
            else {
                $('#body_btnMultiAdded').hide();
                $('#body_btndownload').hide();
            }

        })

        

        function LoadExpiryDate(hdnid, id, defDate) {
            debugger;
            $('#' + hdnid).val(defDate);
            $('#' + id).daterangepicker({
                singleDatePicker: true,
                startDate: defDate,
                endDate: null,
                locale: {
                    format: 'MM/DD/YYYY'
                },
                showDropdowns: true,
                //minYear: 12,
                //maxYear: parseInt(moment().format('YYYY'),10)
            }, function (start, end, label) {
                debugger;
                //var id = this.element[0].id;
                //var hdnid = this.element[0].id.replace("txtExpiryDate", "hdnExpiryDate");
                LoadExpiryDate(hdnid, id, start.format('MM/DD/YYYY'));
            });

        }
        function LoadTable() {
           
                var head_content1 = $('#body_grdEscalatedQuote tr:first').html();
                $('#body_grdEscalatedQuote').prepend('<thead></thead>')
                $('#body_grdEscalatedQuote thead').html('<tr>' + head_content1 + '</tr>');
                $('#body_grdEscalatedQuote tbody tr:first').hide();
                var table2 = $('#body_grdEscalatedQuote').dataTable({

                    scrollX: true,
                    scrollCollapse: true,
                    fixedColumns: true,
                    fixedColumns: {
                        leftColumns: 1,
                        rightColumns: 1
                    },
                    paging: false
                });
            
        }

        function validateFields(evt, obj) {
            debugger;
            var errFlag = 0;
            var id = obj.id;
            var comment_id;
            var lbl;
            var offerprice_id;
            var ref_id;
            var refnum_id;
            var item_id;
            var MOQ_id;
            var multiflag_id;
            var status;
            var RMOQ_id;
            var MOQ
            var RMOQ;
            var lblMOQmsg_id;
            var stock_id;
            var systemprice_id;
            var txtOrdValidity_id;
            var txtOrderQuant_id;
            var ddlOrderFreq_id;
            var txtOrderQuant;
            //var expiry_id;
            if (id.includes("btnReject")) {
                comment_id = id.replace("btnReject", "txtComment");
                lbl = id.replace("btnReject", "lblmsg");
                offerprice_id = id.replace("btnReject", "txtNewOfferPrice");
                ref_id = id.replace("btnReject", "hdnID");
                refnum_id = id.replace("btnReject", "lblref");
                item_id = id.replace("btnReject", "lblitem");
                //MOQ_id = id.replace("btnReject", "txtMOQ");
                //RMOQ_id = id.replace("btnReject", "lblQTYPO");
                //multiflag_id = id.replace("btnReject", "chkMulti");
                //MOQ = $("#" + MOQ_id).val();
                //expiry_id = id.replace("btnReject", "txtExpiryDate");
                //if (IsnullOrEmpty(MOQ)) {
                //    MOQ = "";
                //}
                if (IsnullOrEmpty($("#" + comment_id).val())) {
                    $("#" + comment_id).css("border", "1px solid red");
                    $("span#" + lbl).text("*");
                    $("#body_lblErrMsg").text("*Comment is mandatory.");
                    errFlag++;
                }
                else {
                    $("span#" + lbl).text("");
                    $("#" + comment_id).css("border", "");
                    status = "Rejected";
                }
            }
            else if (id.includes("btnApprove")) {
                comment_id = id.replace("btnApprove", "txtComment");
                offerprice_id = id.replace("btnApprove", "txtNewOfferPrice");
                systemprice_id = id.replace("btnApprove", "txtOfferPrice");
                targetprice_id = id.replace("btnApprove", "lblTargetPrice");
                lbl = id.replace("btnApprove", "lblpricemsg");
                ref_id = id.replace("btnApprove", "hdnID");
                refnum_id = id.replace("btnApprove", "lblref");
                item_id = id.replace("btnApprove", "lblitem");
                //MOQ_id = id.replace("btnApprove", "txtMOQ");
                //RMOQ_id = id.replace("btnApprove", "lblQTYPO");
                //multiflag_id = id.replace("btnApprove", "chkMulti");
                //lblMOQmsg_id = id.replace("btnApprove", "lblMOQmsg");

                txtOrdValidity_id = id.replace("btnApprove", "txtOrdValidity");
                txtOrderQuant_id = id.replace("btnApprove", "txtOrderQuant");
                ddlOrderFreq_id = id.replace("btnApprove", "ddlOrderFreq");

                txtOrderQuant = $("#" + txtOrderQuant_id).val();
                //expiry_id = id.replace("btnApprove", "txtExpiryDate");
                //MOQ = $("#" + MOQ_id).val();
                //RMOQ = $("#" + RMOQ_id).val();
                stock_id = id.replace("btnApprove", "hdnStockCode");
                var stock = $("#" + stock_id).val();
                if (IsnullOrEmpty(txtOrderQuant)) {
                    $("#" + txtOrderQuant_id).css("border", "1px solid red");
                    $("#body_lblErrMsg").text("*Break quantity is required.");
                    errFlag++;
                }
                else {
                    $("#body_lblErrMsg").text("");
                    $("#" + txtOrderQuant_id).css("border", "");
                }
                //if (IsnullOrEmpty(MOQ)) {
                //    if (stock == "1" || stock == "6") {
                //        MOQ = RMOQ;
                //        $("span#" + lblMOQmsg_id).text("");
                //    }
                //    else {
                //        $("span#" + lblMOQmsg_id).text("*");
                //        $("#body_lblErrMsg").text("*MOQ is mandatory.");
                //        errFlag++;

                //    }
                //}
                //else if (parseInt(MOQ) > parseInt(txtOrderQuant)) {
                //    $("#" + MOQ_id).css("border", "1px solid red");
                //    $("span#" + lblMOQmsg_id).text("*");
                //    $("#body_lblErrMsg").text("*Break quantity should be equal to or greater than MOQ.");
                //    errFlag++;
                //}
                //else {
                //    $("span#" + lblMOQmsg_id).text("");
                //}
                if (IsnullOrEmpty($("#" + offerprice_id).val())) {
                    $("#" + offerprice_id).css("border", "1px solid red");
                    $("span#" + lbl).text("*");
                    $("#body_lblErrMsg").text("*Special Price is mandatory.");
                    errFlag++;
                }
                else {
                    var offerprice = $("#" + offerprice_id).val();
                    /*var targetprice = $("#" + targetprice_id).text();*/
                    var systemprice = $("#" + systemprice_id).val();
                    //if (parseFloat(parseFloat(targetprice).toFixed(2)) > parseFloat(parseFloat(offerprice).toFixed(2))) {
                    //    //if (targetprice > offerprice) {
                    //    $("#" + offerprice_id).css("border", "1px solid red");
                    //    $("span#" + lbl).text("*");
                    //    err_comment = "*Price should be equal to or greater than Expected Price.";
                    //    errFlag++;
                    //}
                    //if (parseFloat(parseFloat(systemprice).toFixed(2)) > parseFloat(parseFloat(offerprice).toFixed(2))) {
                    //    //if (targetprice > offerprice) {
                    //    $("#" + offerprice_id).css("border", "1px solid red");
                    //    $("span#" + lbl).text("*");
                    //    err_comment = "*Price should be equal to or greater than System Price.";
                    //    errFlag++;
                    //}
                    
                   /* else {*/
                        $("#" + offerprice_id).css("border", "");
                        $("span#" + lbl).text("");
                        status = "Approved";
                   /* }*/
                }
            }
            if (errFlag > 0) {
                return false;
            }
            else {

                var txtOrdValidity = $("#" + txtOrdValidity_id).val();
                var txtOrderQuant = $("#" + txtOrderQuant_id).val();
                var ddlOrderFreq = '';
                    //$("#" + ddlOrderFreq_id).val();

                var ref = $("#" + ref_id).val();
                var refnum = $("#" + refnum_id).text();
                var item = $("#" + item_id).text();
                //var expiry_date = $("#" + expiry_id).val();
                var multiflag=1;
                //if ($("#" + multiflag_id).is(':checked'))
                //    multiflag = 1;
                //else
                //    multiflag = 0;
                var offerprice = $("#" + offerprice_id).val();
                var comment = $("#" + comment_id).val();
                var objList = [];
                var obj = {
                    "id": ref
                    , "item": item
                    , "ref_no": refnum
                    , "status": status
                    , "comment": comment
                    , "MOQ": txtOrderQuant
                    , "offerPrice": offerprice
                    , "multiorder_flag": multiflag
					, "Order_Validity": txtOrdValidity
					, "Approved_OrderQty": txtOrderQuant
					, "Approved_OrderFreq": ddlOrderFreq
                    //, "expiry_date": expiry_date
                };
                objList.push(obj);
                var param = "{objList:" + JSON.stringify(objList) + "}";
                $.ajax({
                    url: 'PendingTasks.aspx/UpdateQuoteStatus',
                    method: 'post',
                    datatype: 'json',
                    data: param,
                    //data: '{id:"' + ref + '", item:"' + item + '", ref_no:"' + refnum + '", status:"' + status + '", comment:"' + comment + '", MOQ:"' + MOQ + '", offerPrice:"' + offerprice + '", multiorder_flag:' + multiflag + '}',
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        msg = JSON.parse(msg.d);
                        alert(msg.msg);
                        window.location.reload();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("Error");
                    }
                });
                return true;
            }
        }

        function validateFieldsbyId(id) {
            var errFlag = 0, comment_id, lbl, offerprice_id, ref_id, refnum_id, item_id, MOQ_id, multiflag_id, status, RMOQ_id, MOQ, RMOQ, lblMOQmsg_id, stock_id, systemprice_id;

            var txtOrdValidity_id;
            var txtOrderQuant_id;
            var ddlOrderFreq_id;
            var err_comment;
            if (id.includes("btnReject")) {
                comment_id = id.replace("btnReject", "txtComment");
                lbl = id.replace("btnReject", "lblmsg");
                offerprice_id = id.replace("btnReject", "txtNewOfferPrice");
                ref_id = id.replace("btnReject", "hdnID");
                refnum_id = id.replace("btnReject", "lblref");
                item_id = id.replace("btnReject", "lblitem");
                //MOQ_id = id.replace("btnReject", "txtMOQ");
                //RMOQ_id = id.replace("btnReject", "lblQTYPO");
               /* multiflag_id = id.replace("btnReject", "chkMulti");*/
             /*   MOQ = $("#" + MOQ_id).val();*/
                lbl = id.replace("btnReject", "lblmsg");
                if (IsnullOrEmpty($("#" + comment_id).val())) {
                    $("#" + comment_id).css("border", "1px solid red");
                    $("span#" + lbl).text("*");
                    err_comment = "*Comment is mandatory.";
                    errFlag++;
                }
                else {
                    $("span#" + lbl).text("");
                    $("#" + comment_id).css("border", "");
                    status = "Rejected";
                }
            }
            else if (id.includes("btnApprove")) {
                comment_id = id.replace("btnApprove", "txtComment");
                offerprice_id = id.replace("btnApprove", "txtNewOfferPrice");
                systemprice_id = id.replace("btnApprove", "txtOfferPrice");
                /*targetprice_id = id.replace("btnApprove", "lblTargetPrice");*/
                lbl = id.replace("btnApprove", "lblpricemsg");
                ref_id = id.replace("btnApprove", "hdnID");
                refnum_id = id.replace("btnApprove", "lblref");
                item_id = id.replace("btnApprove", "lblitem");
                //MOQ_id = id.replace("btnApprove", "txtMOQ");
                //RMOQ_id = id.replace("btnApprove", "lblQTYPO");
               /* multiflag_id = id.replace("btnApprove", "chkMulti");*/
               /* lblMOQmsg_id = id.replace("btnApprove", "lblMOQmsg");*/
                txtOrdValidity_id = id.replace("btnApprove", "txtOrdValidity");
                txtOrderQuant_id = id.replace("btnApprove", "txtOrderQuant");
                ddlOrderFreq_id = id.replace("btnApprove", "ddlOrderFreq");
                //MOQ = $("#" + MOQ_id).val();
                //RMOQ = $("#" + RMOQ_id).val();
                stock_id = id.replace("btnApprove", "hdnStockCode");
                var stock = $("#" + stock_id).val();
                //if (IsnullOrEmpty(MOQ)) {
                //    if (stock == "1" || stock == "6") {
                //        $("span#" + lblMOQmsg_id).text("*");
                //        err_comment = "*MOQ is mandatory.";
                //        errFlag++;
                //    }
                //    else {
                //        MOQ = RMOQ;
                //        $("span#" + lblMOQmsg_id).text("");
                //    }
                //}
                //else if (MOQ < RMOQ) {
                //    $("#" + MOQ_id).css("border", "1px solid red");
                //    $("span#" + lblMOQmsg_id).text("*");
                //    err_comment = "*MOQ should be equal to or greater than Qty per order.";
                //    errFlag++;
                //}
                //else {
                //    $("span#" + lblMOQmsg_id).text("");
                //}
                if (IsnullOrEmpty($("#" + offerprice_id).val())) {
                    $("#" + offerprice_id).css("border", "1px solid red");
                    $("span#" + lbl).text("*");
                    err_comment = "*Special Price is mandatory.";
                    errFlag++;
                }
                else {
                    var offerprice = $("#" + offerprice_id).val();
                   /* var targetprice = $("#" + targetprice_id).text();*/
                    var systemprice = $("#" + systemprice_id).val();
                    //if (parseFloat(parseFloat(targetprice).toFixed(2)) > parseFloat(parseFloat(offerprice).toFixed(2))) {
                    //    //if (targetprice > offerprice) {
                    //    $("#" + offerprice_id).css("border", "1px solid red");
                    //    $("span#" + lbl).text("*");
                    //    err_comment = "*Price should be equal to or greater than Expected Price.";
                    //    errFlag++;
                    //}
                     if (parseFloat(parseFloat(systemprice).toFixed(2)) > parseFloat(parseFloat(offerprice).toFixed(2))) {
                        //if (targetprice > offerprice) {
                        $("#" + offerprice_id).css("border", "1px solid red");
                        $("span#" + lbl).text("*");
                        err_comment = "*Price should be equal to or greater than System Price.";
                        errFlag++;
                    }
                    else {
                        $("#" + offerprice_id).css("border", "");
                        $("span#" + lbl).text("");
                        status = "Approved";
                    }
                }
            }
            var json;
            if (errFlag > 0) {
                $("#body_lblErrMsg").text(err_comment);
            }
            else {
                var txtOrdValidity = $("#" + txtOrdValidity_id).val();
                var txtOrderQuant = $("#" + txtOrderQuant_id).val();
                var ddlOrderFreq = $("#" + ddlOrderFreq_id).val();
                var ref = $("#" + ref_id).val();
                var refnum = $("#" + refnum_id).text();
                var item = $("#" + item_id).text();

                var multiflag;
                if ($("#" + multiflag_id).is(':checked'))
                    multiflag = 1;
                else
                    multiflag = 0;
                var offerprice = $("#" + offerprice_id).val();
                var comment = $("#" + comment_id).val();

                json = {
                    "id": ref,
                    "item": item,
                    "ref_no": refnum,
                    "status": status,
                    "comment": comment,
                    "MOQ": txtOrderQuant,
                    "offerPrice": offerprice,
                    "multiorder_flag": multiflag
					, "Order_Validity": txtOrdValidity
					, "Approved_OrderQty": txtOrderQuant
					, "Approved_OrderFreq": ddlOrderFreq
                };
                $("#body_lblErrMsg").text("");
            }

            var output = {
                "errFlag": errFlag,
                "err_comment": err_comment,
                "json": json
            };

            return output;
        }
        function validateMultiple(evt, obj) {
            debugger;
            var totaltrCount = $("[id*=grdPendingQuote] tr").length;
            var trCount = $("[id*=grdPendingQuote] td").closest("tr").length;
            var errorFlag = 0;
            var output;
            var id;
            var chk_id;
            var err_msg = [];
            var obj = [];
            for (i = 0; i < trCount; i++) {
                id = "body_grdPendingQuote_btnApprove_" + i;
                chk_id = "body_grdPendingQuote_hdn_chk_multi_" + i;
                if ($('#' + chk_id).val() == "1") {
                    output = validateFieldsbyId(id);
                    errorFlag += output.errFlag;
                    if (output.errFlag == 0) {
                        obj.push(output.json);
                    }
                    else {
                        err_msg.push(output.err_comment);
                    }
                }
            }
            debugger;
            if (errorFlag > 0)
                return false;
            else {
                var param = "{objList:" + JSON.stringify(obj) + "}";
                $.ajax({
                    url: 'PendingTasks.aspx/UpdateQuoteStatus',
                    method: 'post',
                    datatype: 'json',
                    data: param,
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        msg = JSON.parse(msg.d);
                        alert(msg.msg);
                        window.location.reload();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("Error");
                    }
                });
            }
        }
        function IsnullOrEmpty(val) {
            if (val != '' && val != undefined && val != '--Select--')
                return false;
            else
                return true;
        }

        function isNumberKey(evt, obj) {

            var charCode = (evt.which) ? evt.which : event.keyCode
            var value = obj.value;
            var dotcontains = value.indexOf(".") != -1;
            if (dotcontains) {
                var match = ('' + value).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
                if (!match) { return 0; }
                var decCount = Math.max(0,
                     // Number of digits right of decimal point.
                     (match[1] ? match[1].length : 0)
                     // Adjust for scientific notation.
                     - (match[2] ? +match[2] : 0));
                if (decCount > 1) return false;
                if (charCode == 46) return false;
            }
            else {
                if (value.length > 10) {
                    if (charCode == 46) return true;
                    else return false;
                }
            }
            if (charCode == 46) return true;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function setTextareaVal(evt, obj) {
            debugger;
            var value = obj.value;
            $("#" + obj.id).val(value);
        }

        function OpenDetailView(evt, obj) {
            debugger;
            evt.preventDefault();
            var id = obj.id;
            var ref_id = id.replace("btnView", "hdnID");
            sessionStorage.setItem('ref_id', ref_id);
            var refnum_id = id.replace("btnView", "lblref");
            var item_id = id.replace("btnView", "lblitem");
            var ref = $("#" + ref_id).val();
            var refnum = $("#" + refnum_id).text();
            var item = $("#" + item_id).text();
            var ordervalidity_id = id.replace("btnView", "txtOrdValidity");
            var ordervalidity = $("#" + ordervalidity_id).val();
            sessionStorage.setItem('ordervalidity', ordervalidity);
            var orderqty_id = id.replace("btnView", "txtOrderQuant");
            var orderqty = $("#" + orderqty_id).val();
            sessionStorage.setItem('orderqty', orderqty);
            var MOQ_id = id.replace("btnView", "txtMOQ");
            var MOQ = $("#" + MOQ_id).val();
            sessionStorage.setItem('MOQ', MOQ);
            var SP_id = id.replace("btnView", "txtNewOfferPrice");
            var SP = $("#" + SP_id).val();
            sessionStorage.setItem('sysPrice', SP);
            var comment_id = id.replace("btnView", "txtComment");
            var comment = $("#" + comment_id).val();
            sessionStorage.setItem('comment', comment);
            win=window.open("QuoteDetails.aspx?id=" + ref, "_blank", "WIDTH=900,HEIGHT=650,scrollbars=no, menubar=no,resizable=yes,directories=no,location=no");
        }
        function parentFunction(val) {
            debugger;
            console.log(val);
            win.close();
            val = val.replace("\\", "\\\\");
            var obj = JSON.parse(val);
            console.log(obj);
            var id = sessionStorage.getItem('ref_id');
            var orderValidity_id = id.replace("hdnID", "txtOrdValidity");
            var OrderQuant_id = id.replace("hdnID", "txtOrderQuant");
            var OrderFreq_id = id.replace("hdnID", "ddlOrderFreq");
            var MOQ_id = id.replace("hdnID", "txtMOQ");
            var NewOfferPrice_id = id.replace("hdnID", "txtNewOfferPrice");
            var Comment_id = id.replace("hdnID", "txtComment");
            $("#" + orderValidity_id).val(obj.objList[0].Order_Validity);
            $("#" + OrderQuant_id).val(obj.objList[0].Approved_OrderQty);
            $("#" + OrderFreq_id).val(obj.objList[0].Approved_OrderFreq);
            $("#" + MOQ_id).val(obj.objList[0].MOQ);
            $("#" + NewOfferPrice_id).val(obj.objList[0].offerPrice);
            $("#" + Comment_id).val(obj.objList[0].comment);
            //document.getElementById(Comment_id).value = obj.objList[0].comment;
            //document.getElementById(Comment_id).innerHTML = obj.objList[0].comment;
            //document.getElementById(Comment_id).textContent = obj.objList[0].comment;
            //document.getElementById(Comment_id).outerHTML = "<textarea rows=\"2\" cols=\"20\" id=\"" + Comment_id + "\" onkeyup=\"setTextareaVal(event,this);\">" + obj.objList[0].comment + "</textarea>";
        }
        document.addEventListener("DOMContentLoaded", function () {
            debugger;
            var tabs = document.querySelectorAll('.tabbed li');
            var switchers = document.querySelectorAll('.switcher-box a');
            var skinable = document.getElementById('skinable');

            for (var i = 0, len = tabs.length; i < len; i++) {
                tabs[i].addEventListener("click", function () {
                    if (this.classList.contains('active')) {
                        debugger;
                        return;
                    }
                    var parent = this.parentNode,
                        innerTabs = parent.querySelectorAll('li');

                    for (var index = 0, iLen = innerTabs.length; index < iLen; index++) {
                        innerTabs[index].classList.remove('active');
                    }

                    this.classList.add('active');
                });
            }

            for (var i = 0, len = switchers.length; i < len; i++) {
                switchers[i].addEventListener("click", function () {
                    if (this.classList.contains('active'))
                        return;

                    var parent = this.parentNode,
                        innerSwitchers = parent.querySelectorAll('a'),
                        skinName = this.getAttribute('skin');

                    for (var index = 0, iLen = innerSwitchers.length; index < iLen; index++) {
                        innerSwitchers[index].classList.remove('active');
                    }

                    this.classList.add('active');
                    skinable.className = 'tabbed round ' + skinName;
                });
            }
        });
        function tabchange(e) {
            debugger;
            if (e.id == "body_pendingList") {
                $('#body_divPendingQuote').css("display", "block");
              //  $('#body_btndownload').css("display", "none");
                $('#body_divAddInPA').css("display", "none");
                $('#body_divAddInPA').removeClass("active");
                $('#body_divEscalatedQuote').css("display", "none");
                $('#body_divEscalatedQuote').removeClass("active");
                $('#body_divPendingQuote').addClass("active");
                if (!$.fn.DataTable.isDataTable('#body_grdPendingQuote')) {
                    var head_content = $('#body_grdPendingQuote tr:first').html();
                    $('#body_grdPendingQuote').prepend('<thead></thead>')
                    $('#body_grdPendingQuote thead').html('<tr>' + head_content + '</tr>');
                    $('#body_grdPendingQuote tbody tr:first').hide();
                    var table = $('#body_grdPendingQuote').dataTable({
                        //scrollY: "350px",
                        scrollX: true,
                        scrollCollapse: true,
                        fixedColumns: true,
                        fixedColumns: {
                            leftColumns: 1,
                            rightColumns: 1
                        },
                        paging: false
                        //lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                        //dom: 'lBfrtip',

                    });
                    var totaltrCount = $("[id*=grdPendingQuote] tr").length;
                    var trCount = $("[id*=grdPendingQuote] td").closest("tr").length;
                    sessionStorage.setItem("tot_Count", trCount);
                }

            }
            else if (e.id == "body_escalatedList") {
                $('#body_divPendingQuote').css("display", "none");
                //  $('#body_btndownload').css("display", "block");
                $('#body_divAddInPA').css("display", "none");
                $('#body_divAddInPA').removeClass("active");
                $('#body_divEscalatedQuote').css("display", "block");
                $('#body_divEscalatedQuote').addClass("active");
                $('#body_divPendingQuote').removeClass("active");
                if (!$.fn.DataTable.isDataTable('#body_grdEscalatedQuote')) {

                    var head_content1 = $('#body_grdEscalatedQuote tr:first').html();
                    $('#body_grdEscalatedQuote').prepend('<thead></thead>')
                    $('#body_grdEscalatedQuote thead').html('<tr>' + head_content1 + '</tr>');
                    $('#body_grdEscalatedQuote tbody tr:first').hide();
                    var table2 = $('#body_grdEscalatedQuote').dataTable({
                        
                        scrollX: true,
                        scrollCollapse: true,
                        fixedColumns: true,
                        fixedColumns: {
                            leftColumns: 1,
                            rightColumns: 1
                        },
                        paging: false
                    });
                }
            }
            else {
                $('#body_divPendingQuote').css("display", "none");
              //  $('#body_btndownload').css("display", "block");
                $('#body_divAddInPA').css("display", "block");
                $('#body_divAddInPA').addClass("active");
                $('#body_divPendingQuote').removeClass("active");
                $('#body_divEscalatedQuote').css("display", "none");
                $('#body_divEscalatedQuote').removeClass("active");
                if (!$.fn.DataTable.isDataTable('#body_grdAcceptedQuote')) {

                    var head_content1 = $('#body_grdAcceptedQuote tr:first').html();
                    $('#body_grdAcceptedQuote').prepend('<thead></thead>')
                    $('#body_grdAcceptedQuote thead').html('<tr>' + head_content1 + '</tr>');
                    $('#body_grdAcceptedQuote tbody tr:first').hide();
                    var table1 = $('#body_grdAcceptedQuote').dataTable({
                        ////"order": [[2, 'desc']]
                        //scrollY: "350px",
                        //scrollX: true,
                        //scrollCollapse: true,
                        ////fixedColumns: true,
                        ////fixedColumns: {
                        ////    leftColumns: 1,
                        ////    rightColumns: 4
                        ////},
                        //paging:false,
                        //lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
                        //dom: 'lBfrtip',

                        scrollX: true,
                        scrollCollapse: true,
                        fixedColumns: true,
                        fixedColumns: {
                            leftColumns: 1,
                            rightColumns: 1
                        },
                        paging: false
                    });
                }
            }

        }

        function AddedInPA(evt, obj) {
            debugger;
            var objList = [];
            var id = obj.id;
            var ref_id = id.replace("btnAddInPA", "hdnID");
            var refnum_id = id.replace("btnAddInPA", "lblref");
            var item_id = id.replace("btnAddInPA", "lblitem");
            var obj = {
                "id": $('#' + ref_id).val()
                    , "item": $('#' + item_id).text()
                    , "ref_no":$('#' + refnum_id).text()
                    , "status": "Added In PA"
                    
            };
            objList.push(obj);
            var param = "{objList:" + JSON.stringify(objList) + "}";
            $.ajax({
                url: 'PendingTasks.aspx/UpdateQuoteStatus',
                method: 'post',
                datatype: 'json',
                data: param,
                //data: '{id:"' + ref + '", item:"' + item + '", ref_no:"' + refnum + '", status:"' + status + '", comment:"' + comment + '", MOQ:"' + MOQ + '", offerPrice:"' + offerprice + '", multiorder_flag:' + multiflag + '}',
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    msg = JSON.parse(msg.d);
                    alert(msg.msg);
                    window.location.reload();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("Error");
                }
            });

        }

        function MultiItemsinPA(evt, obj) {
            debugger;
            var totaltrCount = $("[id*=grdAcceptedQuote] tr").length;
            var trCount = $("[id*=grdAcceptedQuote] td").closest("tr").length;
            var id;
            var chk_id;
            var objList = [];
            var obj;
            var q_id;
            var quote_ids = "";
            for (i = 0; i < trCount; i++) {
                q_id = "body_grdAcceptedQuote_hdnID_" + i;
                id = "body_grdAcceptedQuote_btnAddInPA_" + i;
                chk_id = "body_grdAcceptedQuote_hdn_chk_multi_" + i;
                if ($('#' + chk_id).val() == "1") {
                    //var id = obj.id;
                    quote_ids = quote_ids + "," + $('#' + q_id).val();
                    var ref_id = id.replace("btnAddInPA", "hdnID");
                    var refnum_id = id.replace("btnAddInPA", "lblref");
                    var item_id = id.replace("btnAddInPA", "lblitem");
                    var obj = {
                        "id": $('#' + ref_id).val()
                            , "item": $('#' + item_id).text()
                            , "ref_no": $('#' + refnum_id).text()
                            , "status": "Added In PA"

                    };
                    objList.push(obj);
                }
            }
            quote_ids = quote_ids.substr(1, quote_ids.length - 1);
            $("#body_hdnQuoteIds").val(quote_ids);
            var param = "{objList:" + JSON.stringify(objList) + "}";
            $.ajax({
                url: 'PendingTasks.aspx/UpdateQuoteStatus',
                method: 'post',
                datatype: 'json',
                data: param,
                //data: '{id:"' + ref + '", item:"' + item + '", ref_no:"' + refnum + '", status:"' + status + '", comment:"' + comment + '", MOQ:"' + MOQ + '", offerPrice:"' + offerprice + '", multiorder_flag:' + multiflag + '}',
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    msg = JSON.parse(msg.d);
                    alert(msg.msg);
                    window.location.reload();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("Error");
                }
            });
        }
        function DownlaodAccepted(evt, obj) {
            debugger;
            var totaltrCount = $("[id*=grdAcceptedQuote] tr").length;
            var trCount = $("[id*=grdAcceptedQuote] td").closest("tr").length;
            var id;
            var quote_ids="";
            for (i = 0; i < trCount; i++) {
                id = "body_grdAcceptedQuote_hdnID_" + i;
                chk_id = "body_grdAcceptedQuote_hdn_chk_multi_" + i;
                if ($('#' + chk_id).val() == "1") {
                    quote_ids = quote_ids + "," + $('#' + id).val();
                }
            }
            quote_ids = quote_ids.substr(1, quote_ids.length - 1);
         
            if (IsnullOrEmpty(quote_ids)) {
                return false;
            }
            else {
                $("#body_hdnQuoteIds").val(quote_ids);
                return true;
            }
        }
        function Clear() {
            sessionStorage.removeItem('selectedPendingStart');
            sessionStorage.removeItem('selectedPendingEnd');
            var SelectedStart = sessionStorage.getItem("selectedPendingStart");
            var SelectedEnd = sessionStorage.getItem("selectedPendingEnd");
            var start = (SelectedStart == null ? "'07/20/2019" : SelectedStart);
            var end = (SelectedEnd == null ? moment() : SelectedEnd);
            $('#body_txtDateRange').daterangepicker({
                //opens: 'left',
                autoUpdateInput: true,
                locale: {
                    format: 'MM/DD/YYYY'
                },
                startDate: start,
                endDate: end,
                ranges: {
                    'All Date': ['07/20/2019', moment()],
                    'Last Year': [moment().subtract(1, 'year'), moment()],
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
            }, function (start, end, label) {
                console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
                var start = start.format('MM/DD/YYYY');
                var end = end.format('MM/DD/YYYY');
                sessionStorage.setItem('selectedPendingStart', start);
                sessionStorage.setItem('selectedPendingEnd', end);
            });
        }
        function selectAll(obj)
        {
            debugger;
            var id = obj.children[0].id.replace('chk_All', 'hdn_chk_All');
            if ($('#' + id).val() == "0")
                $('#' + id).val("1");
            else
                $('#' + id).val("0");
            var totaltrCount = $("[id*=grdAcceptedQuote] tr").length;
            var trCount = totaltrCount;
                //sessionStorage.getItem("tot_Count");
            //$("[id*=grdPendingQuote] td").closest("tr").length;
            var multi_btn_flag = 0;
            var hdn_chk_id;
            var chk_id;
            if ($('#' + id).val() == "0") {
                for (i = 0; i < trCount; i++) {
                    hdn_chk_id = "body_grdAcceptedQuote_hdn_chk_multi_" + i;
                    chk_id = "body_grdAcceptedQuote_chk_multi_" + i;

                    //$('#' + chk_id).attr('aria-checked', 'false').prop('checked', false);
                    //$('input[type=checkbox]').prop('checked', false);
                    $("input[id=" + chk_id + "]").prop('checked', false);
                    $('#' + hdn_chk_id).val("0");

                }
                $('#body_btnMultiAdded').hide();
                $('#body_btndownload').hide();
            }
            else {
                for (i = 0; i < trCount; i++) {
                    hdn_chk_id = "body_grdAcceptedQuote_hdn_chk_multi_" + i;
                    chk_id = "body_grdAcceptedQuote_chk_multi_" + i;

                    //$('#' + chk_id).attr('aria-checked', 'true').prop('checked', true);
                    //$('input[type=checkbox]').prop('checked', true);
                    $("input[id=" + chk_id + "]").prop('checked', true);
                    $('#' + hdn_chk_id).val("1");

                }
                $('#body_btnMultiAdded').show();
                $('#body_btndownload').show();
            }
        }

        //Esclated tab
        function validateMultiple1(evt, obj) {
            debugger;
            var totaltrCount = $("[id*=grdEscalatedQuote] tr").length;
            var trCount = $("[id*=grdEscalatedQuote] td").closest("tr").length;
            var errorFlag = 0;
            var output;
            var id;
            var chk_id;
            var err_msg = [];
            var obj = [];
            for (i = 0; i < trCount; i++) {
                id = "body_grdEscalatedQuote_btnApprove_" + i;
                chk_id = "body_grdEscalatedQuote_hdn_chk_multi_" + i;
                if ($('#' + chk_id).val() == "1") {
                    output = validateFieldsbyId(id);
                    errorFlag += output.errFlag;
                    if (output.errFlag == 0) {
                        obj.push(output.json);
                    }
                    else {
                        err_msg.push(output.err_comment);
                    }
                }
            }
            debugger;
            if (errorFlag > 0)
                return false;
            else {
                var param = "{objList:" + JSON.stringify(obj) + "}";
                $.ajax({
                    url: 'PendingTasks.aspx/UpdateQuoteStatus',
                    method: 'post',
                    datatype: 'json',
                    data: param,
                    contentType: "application/json; charset=utf-8",
                    success: function (msg) {
                        msg = JSON.parse(msg.d);
                        alert(msg.msg);
                        window.location.reload();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("Error");
                    }
                });
            }
        }
        function OpenDetailEscalatedView(evt, obj) {
            debugger;
            evt.preventDefault();
            var id = obj.id;
            var ref_id = id.replace("btnView", "hdnID");
            sessionStorage.setItem('ref_id', ref_id);
            var refnum_id = id.replace("btnView", "lblref");
            var item_id = id.replace("btnView", "lblitem");
            var ref = $("#" + ref_id).val();
            var refnum = $("#" + refnum_id).text();
            var item = $("#" + item_id).text();
            var ordervalidity_id = id.replace("btnView", "txtOrdValidity");
            var ordervalidity = $("#" + ordervalidity_id).val();
            sessionStorage.setItem('ordervalidity', ordervalidity);
            var orderqty_id = id.replace("btnView", "txtOrderQuant");
            var orderqty = $("#" + orderqty_id).val();
            sessionStorage.setItem('orderqty', orderqty);
            var MOQ_id = id.replace("btnView", "txtMOQ");
            var MOQ = $("#" + MOQ_id).val();
            sessionStorage.setItem('MOQ', MOQ);
            var SP_id = id.replace("btnView", "txtNewOfferPrice");
            var SP = $("#" + SP_id).val();
            sessionStorage.setItem('sysPrice', SP);
            var comment_id = id.replace("btnView", "txtComment");
            var comment = $("#" + comment_id).val();
            sessionStorage.setItem('comment', comment);
            win = window.open("QuoteDetails.aspx?Escid=" + ref, "_blank", "WIDTH=900,HEIGHT=650,scrollbars=no, menubar=no,resizable=yes,directories=no,location=no");
        }

        function DownloadItemFile(obj) {
            var filename = $("#" + obj.id).text();
            var hdnpath = obj.id.replace("lnkFile", "hdnFilePath");
            var filepath = $("#" + hdnpath).val();
            DownloadFile(filepath, filename);
        }
        function DownloadFile(Escalation_file, filename) {
            $.ajax({
                type: "POST",
                url: "QuoteSummary.aspx/DownloadFile",
                data: '{file: "' + Escalation_file + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {
                    debugger;
                    //Convert Base64 string to Byte Array.
                    var bytes = Base64ToBytes(r.d);

                    //Convert Byte Array to BLOB.
                    var blob = new Blob([bytes], { type: "application/octetstream" });

                    //Check the Browser type and download the File.
                    var isIE = false || !!document.documentMode;
                    if (isIE) {
                        window.navigator.msSaveBlob(blob, fileName);
                    } else {
                        var url = window.URL || window.webkitURL;
                        link = url.createObjectURL(blob);
                        var a = $("<a />");
                        a.attr("download", filename);
                        a.attr("href", link);
                        $("body").append(a);
                        a[0].click();
                        //$("body").remove(a);
                    }
                    location.reload();
                }
            });

        }
        function Base64ToBytes(base64) {
            var s = window.atob(base64);
            var bytes = new Uint8Array(s.length);
            for (var i = 0; i < s.length; i++) {
                bytes[i] = s.charCodeAt(i);
            }
            return bytes;
        };
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div class="container1">
        <div class="col-md-12">
            <div class="mn_view_stock">
                <h4>Pending Quotes </h4>
            </div>
            <div class="mn_section mn_filter">
                <div>
                    <div>
                        <div class="col-md-2 mn_view_stock">
                            <label class="control-label">Requested Date</label>
                            <asp:TextBox ID="txtDateRange" runat="server"></asp:TextBox>
                        </div>
                        <%--  <button class="btn btn-success button pull-right">Filter</button>
                            <button class="btn btn-danger button pull-right">Clear</button>--%>

                        <div class="col-md-1 mn_view_stock" style="margin-top: 30px; padding:1px!important">

                            <asp:Button ID="btnFilter" runat="server" CssClass="btn btn-success button pull-right" Text="Filter" OnClick="btnFilter_Click" />
                        </div>
                        <div class="col-md-1 mn_view_stock" style="margin-top: 30px; padding:1px!important">
                            <asp:Button ID="btnClear" runat="server" CssClass="btn btn-success button" Text="Clear" OnClientClick="Clear();" OnClick="btnFilter_Click" />
                            <%--<asp:Panel runat="server" ID="pnlData">--%>
                        </div>
                     <%-- <div class="col-md-1 mn_view_stock button pull-right" style="float:right" padding:"1px!important">
                            <asp:Button ID="btndownload"  runat="server" CssClass="btn btn-success button pull-right" OnClick="btndownload_Click" Text="Download" />
                        </div>--%>
                        <div class="col-md-1 mn_view_stock button pull-right" style="float:right;margin-top: 28px;padding-left: 1px;">
                            </div>
                    </div>
                </div>
            </div>
            <asp:Label ID="lblresult" Text="" runat="server"></asp:Label>
            <asp:ScriptManager ID="SM1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>
            <asp:UpdatePanel ID="panel1" runat="server" UpdateMode="Conditional">
                <%--<asp:Panel runat="server" ID="pnlData">--%>
                  
                <ContentTemplate>
                      <%--<div class="col-md-1 mn_view_stock button pull-right" style="float:right" padding:"1px!important">
                        <asp:Button ID="btndownload" runat="server" CssClass="btn btn-success button pull-right" OnClick="btndownload_Click" Text="Download"/>
                            </div>--%>
                    <div id="divQuotes" runat="server">
                    <div class="tabbed skin-turquoise round" id="skinable" style="margin-bottom: 10px;">
                        <ul>
                            <li id="pendingList" runat="server" onclick="tabchange(this);">Pending Quotes</li>
                            <li id="escalatedList" runat="server" class="active"  onclick="tabchange(this);">Escalated Quotes</li>
                            <li id="tobeAddedList" runat="server" onclick="tabchange(this);">Accepted Quotes</li>
                        </ul>
                    </div>
                      
                    <div class="col-md-12 nopad" id="divPendingQuote" runat="server" style="display: none;">
                    <asp:Button ID="btnMultiApprove" runat="server" CssClass="btn btn-success button" Text="Approve" OnClientClick="return validateMultiple(event,this);" />
                    <asp:Label ID="lblErrMsg" runat="server" Style="color: red;"></asp:Label>
                    <asp:GridView ID="grdPendingQuote" CssClass="display compact" runat="server" AutoGenerateColumns="false">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chk_multi" runat="server" CssClass="checkbox" />
                                    <asp:HiddenField ID="hdn_chk_multi" runat="server" Value="0" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CP Name">
                                <ItemTemplate>
                                    <asp:HiddenField runat="server" ID="hdnID" Value='<%# Eval("ID")%>' />
                                    <asp:Label ID="txtCustName" Text='<%#Bind("CP_Name") %>' runat="server"></asp:Label>
                                    <asp:HiddenField runat="server" ID="hdnStockCode" Value='<%# Eval("StockCode")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Branch">
                                <ItemTemplate>
                                    <asp:Label ID="lblbranch" runat="server" Text='<%#Bind("Branch") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Reference No">
                                <ItemTemplate>
                                    <asp:Label ID="lblref" runat="server" Text='<%#Bind("Ref_number") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Code">
                                <ItemTemplate>
                                    <asp:Label ID="lblitem" runat="server" Text='<%#Bind("Item_code") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Description" HeaderStyle-Width="200px">
                                <ItemTemplate>
                                    <asp:Label ID="lblitemdesc" runat="server" Text='<%#Bind("Item_Desc") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHeaderWHS" Text="WHS" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblWHS" Text='<%#Bind("WHS") %>' CssClass="ddl" runat="server" Style="width: 70px;"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHeaderOrderType" Text="Order Type" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblOrderType" Text='<%#Bind("Order_type") %>' CssClass="ddl" runat="server" Style="width: 80px;"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHeaderOrderFreq" Text="Order Frequency" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblOrderFreq" Text='<%#Bind("Order_frequency") %>' CssClass="ddl" runat="server" Style="width: 80px;"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHeaderQTY" Text="Total QTY" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblQTY" Text='<%#Bind("Total_QTY") %>' CssClass="ddl" runat="server" Style="width: 80px;"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHeaderDLP" Text="List Price" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblDLP" Text='<%#Bind("List_Price") %>' CssClass="ddl" runat="server" Style="width: 80px;"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHeaderTargetPrice" Text="Expected Price" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblTargetPrice" Text='<%#Bind("Expected_price") %>' CssClass="ddl" runat="server" Style="width: 80px;"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                           <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHeaderDCRate" Text="Discount Rate(%)" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblDCRate" Text='<%#Bind("DC_rate") %>' CssClass="ddl" runat="server" Style="width: 80px;"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHeaderCustSP" Text="Sales Price" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblCustSP" Text='<%#Bind("Cust_SP") %>' CssClass="ddl" Style="width: 80px;" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHeaderCompanyName" Text="Competitor Company" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblCompanyName" Text='<%#Bind("Comp_Name") %>' Style="width: 200px;" CssClass="ddl" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHeaderDescription" Text="Competitor Product" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblDescription" Text='<%#Bind("Comp_Desc") %>' Style="width: 200px;" CssClass="ddl" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHeaderCompanySP" Text="Competitor Sales Price" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblCompanySP" Text='<%#Bind("Comp_SP") %>' CssClass="ddl" Style="width: 80px;" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                           <%-- <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblMulti" Text="Multiple Order Flag" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkMulti" runat="server" Checked="true" />
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                          <%--  <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHeaderQTYPO" Text="Requested MOQ" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:HiddenField ID="lblQTYPO" Value='<%#Bind("QTY_perOrder") %>' runat="server"></asp:HiddenField>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label Text="Order Valildity" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox Enabled="false" ID="txtOrdValidity" Text='<%#Bind("OrderValidity") %>' onkeypress="return isNumberKey(event,this);" Style="width: 80px;" runat="server"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label Text="Break Quantity" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox Enabled="false" ID="txtOrderQuant" Text='<%#Bind("Approved_OrderQty") %>' onkeypress="return isNumberKey(event,this);" Style="width: 80px;" runat="server"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                           <%-- <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label Text="Order Frequency" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:DropDownList ID="ddlOrderFreq" runat="server">
                                        <asp:ListItem Text="Weekly" Value="weekly">
                                        </asp:ListItem>
                                        <asp:ListItem Text="Fortnightly" Value="fortnightly">
                                        </asp:ListItem>
                                        <asp:ListItem Text="Monthly" Value="monthly">
                                        </asp:ListItem>
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <%--<asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblMOQ" Text="MOQ" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox Enabled="false" ID="txtMOQ" Text='<%#Bind("Approved_OrderQty") %>' onkeypress="return isNumberKey(event,this);" Style="width: 80px;" runat="server"></asp:TextBox>
                                    <asp:Label ID="lblMOQmsg" runat="server" Style="color: red;"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblOfferPrice" Text="System Price" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox  ID="txtOfferPrice" Enabled="false" Style="width: 80px;" runat="server" Text='<%#Bind("Offer_price") %>'></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblNewOfferPrice" Text="Special Price" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox Enabled="false" ID="txtNewOfferPrice" onkeypress="return isNumberKey(event,this);" CssClass="number1" Style="width: 80px;" runat="server" Text='<%#Bind("New_OfferPrice") %>'></asp:TextBox>
                                    <br />
                                    <asp:Label ID="lblpricemsg" runat="server" Style="color: red;"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblExpiryDate" Text="Expiry Date" runat="server"></asp:Label> onkeyup="setTextareaVal(event,this);"
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtExpiryDate" runat="server"></asp:TextBox>
                                    <asp:HiddenField ID="hdnExpiryDate" runat="server" Value='<%#Bind("ExpiryDate") %>' />
                                 </ItemTemplate>
                                </asp:TemplateField>--%>
                            <asp:TemplateField HeaderStyle-CssClass="cssComment">
                                <HeaderTemplate>
                                    <asp:Label ID="lblComment" Text="Comment" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox Rows="2" Columns="20" ID="txtComment" TextMode="MultiLine" runat="server" ></asp:TextBox>
                                    <asp:Label ID="lblmsg" runat="server" Style="color: red;"></asp:Label>
                                    <%--<textarea id="txtComment" cols="20" rows="2" runat="server"></textarea>onkeyup="setTextareaVal(event,this);"--%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label Text="Actions" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div style="width: 90px;">
                                        <asp:ImageButton ToolTip="Approve"  ImageUrl="images/icons/approve.png" OnClientClick="return validateFields(event,this);" CommandName="Approve" ID="btnApprove"  runat="server" style="height:25px;" />
                                       <%-- <asp:Button OnClientClick="return validateFields(event,this);" CommandName="Approve" Text="Approve" ID="btnApprove" CssClass="btn btn-success button pull-right" runat="server" />--%>
                                        <%--Enabled='<%# Eval("Accept_Visible_Flag").ToString() == "1" ? true : false %>' CommandArgument='<%#Bind("ID") %>' --%>
                                        <asp:ImageButton ToolTip="Reject" ImageUrl="images/icons/reject.png" OnClientClick="return validateFields(event,this);" CommandName="Reject" CommandArgument='<%#Bind("ID") %>' ID="btnReject" runat="server"  style="height:25px;" />
                                        <%--<asp:Button OnClientClick="return validateFields(event,this);" CommandName="Reject" CommandArgument='<%#Bind("ID") %>' Text="Reject" ID="btnReject" CssClass="btn btn-danger button pull-right" runat="server" />--%>
                                        <asp:ImageButton ToolTip="View Details" ImageUrl="images/icons/view_details.png" runat="server" ID="btnView"  OnClientClick="OpenDetailView(event,this);"  style="height:25px;" />
                                        <%--<asp:Button runat="server" ID="btnView" Text="View Details" CssClass="btn btn-success button pull-right" OnClientClick="OpenDetailView(event,this);" />--%>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                        </div>
                    <div class="col-md-12 nopad" id="divEscalatedQuote" runat="server" style="display: block;">
                    <asp:Button ID="btnMultiApprove1" runat="server" CssClass="btn btn-success button" Text="Approve" OnClientClick="return validateMultiple1(event,this);" />
                    <asp:Label ID="lblErrMsg1" runat="server" Style="color: red;"></asp:Label>
                    <asp:GridView ID="grdEscalatedQuote" CssClass="display compact" runat="server" AutoGenerateColumns="false">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chk_multi1" runat="server" CssClass="checkbox1" />
                                    <asp:HiddenField ID="hdn_chk_multi1" runat="server" Value="0" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CP Name">
                                <ItemTemplate>
                                    <asp:HiddenField runat="server" ID="hdnID" Value='<%# Eval("ID")%>' />
                                    <asp:Label ID="txtCustName" Text='<%#Bind("CP_Name") %>' runat="server"></asp:Label>
                                    <asp:HiddenField runat="server" ID="hdnStockCode" Value='<%# Eval("StockCode")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                           <%--  <asp:TemplateField HeaderText="Branch">
                                <ItemTemplate>
                                    <asp:Label ID="lblbranch1" runat="server" Text='<%#Bind("Branch") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:TemplateField HeaderText="Reference No">
                                <ItemTemplate>
                                    <asp:Label ID="lblref" runat="server" Text='<%#Bind("Ref_number") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Code">
                                <ItemTemplate>
                                    <asp:Label ID="lblitem" runat="server" Text='<%#Bind("Item_code") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Description" HeaderStyle-Width="200px">
                                <ItemTemplate>
                                    <asp:Label ID="lblitemdesc" runat="server" Text='<%#Bind("Item_Desc") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                           <%-- <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHeaderTargetPrice1" Text="Expected Price" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblTargetPrice1" Text='<%#Bind("Expected_price") %>' CssClass="ddl" runat="server" Style="width: 80px;"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblMulti" Text="Multiple Order Flag" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkMulti" runat="server" Checked="true" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:HiddenField ID="lblQTYPO" Value='<%#Bind("QTY_perOrder") %>' runat="server"></asp:HiddenField>
                                </ItemTemplate>
                            </asp:TemplateField--%>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label Text="Order Valildity" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox Enabled="false" ID="txtOrdValidity" Text='<%#Bind("OrderValidity") %>' onkeypress="return isNumberKey(event,this);" Style="width: 80px;" runat="server"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label Text="Break Quantity" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox Enabled="false" ID="txtOrderQuant" Text='<%#Bind("Approved_OrderQty") %>' onkeypress="return isNumberKey(event,this);" Style="width: 80px;" runat="server"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                         
                        <%--    <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblMOQ" Text="MOQ" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox Enabled="false" ID="txtMOQ" Text='<%#Bind("Approved_OrderQty") %>' onkeypress="return isNumberKey(event,this);" Style="width: 80px;" runat="server"></asp:TextBox>
                                    <asp:Label ID="lblMOQmsg" runat="server" Style="color: red;"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblOfferPrice" Text="System Price" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtOfferPrice" Enabled="false" Style="width: 80px;" runat="server" Text='<%#Bind("Offer_price") %>'></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblNewOfferPrice" Text="Special Price" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox Enabled="false" ID="txtNewOfferPrice" onkeypress="return isNumberKey(event,this);" CssClass="number1" Style="width: 80px;" runat="server" Text='<%#Bind("New_OfferPrice") %>'></asp:TextBox>
                                    <br />
                                    <asp:Label ID="lblpricemsg" runat="server" Style="color: red;"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="End Customer">
                                <ItemTemplate>
                                    <asp:Label ID="lblendcust" runat="server" Text='<%#Bind("End_Customer") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                               <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblAttachments" Text="File Upload By CP" runat="server"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                         <asp:HiddenField ID="hdnFilePath" runat="server" Value='<%# Bind("Escalation_file") %>' />
                                        <asp:LinkButton runat="server" ID="lnkFile" Text='<%# Bind("Escalation_file_name") %>' OnClientClick="DownloadItemFile(this);"></asp:LinkButton>
               
                                    </ItemTemplate>
                                </asp:TemplateField>
                            <asp:TemplateField HeaderText="Last Updated Comment">
                                <ItemTemplate>
                                    <asp:Label ID="lblstatuscomment" runat="server" Text='<%#Bind("StatusChange_Comment") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-CssClass="cssComment">
                                <HeaderTemplate>
                                    <asp:Label ID="lblComment" Text="Comment" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox Rows="2" Columns="20" ID="txtComment" TextMode="MultiLine" runat="server" ></asp:TextBox>
                                    <asp:Label ID="lblmsg" runat="server" Style="color: red;"></asp:Label>
                                    <%--<textarea id="txtComment" cols="20" rows="2" runat="server"></textarea>onkeyup="setTextareaVal(event,this);"--%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label Text="Actions" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div style="width: 90px;">
                                        <asp:ImageButton ToolTip="Approve"  ImageUrl="images/icons/approve.png" OnClientClick="return validateFields(event,this);" CommandName="Approve" ID="btnApprove"  runat="server" style="height:25px;" />
                                        <asp:ImageButton ToolTip="Reject" ImageUrl="images/icons/reject.png" OnClientClick="return validateFields(event,this);" CommandName="Reject" CommandArgument='<%#Bind("ID") %>' ID="btnReject" runat="server"  style="height:25px;" />
                                        <asp:ImageButton ToolTip="View Details" ImageUrl="images/icons/view_details.png" runat="server" ID="btnView"  OnClientClick="OpenDetailEscalatedView(event,this);"  style="height:25px;" />
                                     </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                        </div>
                        <div class="col-md-12 nopad" id="divAddInPA" runat="server" style="display: none;">
                              <asp:Button ID="btnMultiAdded" runat="server" CssClass="btn btn-success button" Text="Add In PA" OnClientClick="MultiItemsinPA(event,this);" OnClick="btndownload_Click" />
                              <asp:Button ID="btndownload" runat="server" CssClass="btn btn-success button pull-left" style="margin-right:10px;" OnClick="btndownload_Click" Text="Download Excel" OnClientClick="return DownlaodAccepted(event,this);"/>
                      <asp:HiddenField ID="hdnQuoteIds" runat="server" />
                    <asp:Label ID="lbladdErrMsg" runat="server" Style="color: red;"></asp:Label>
                            <asp:GridView ID="grdAcceptedQuote" style="width:100%;" CssClass="display compact" runat="server" AutoGenerateColumns="false">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    
                                    <asp:CheckBox ID="chk_All" runat="server" CssClass="chk_multi_All" onchange="selectAll(this);" />
                                    <asp:HiddenField ID="hdn_chk_All" runat="server" Value="0" />

                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chk_multi" runat="server" CssClass="chk_multi_add" />
                                    <asp:HiddenField ID="hdn_chk_multi" runat="server" Value="0" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CP Name">
                                <ItemTemplate>
                                    <asp:HiddenField runat="server" ID="hdnID" Value='<%# Eval("ID")%>' />
                                    <asp:Label ID="txtCustName" Text='<%#Bind("CP_Name") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Reference No">
                                <ItemTemplate>
                                    <asp:Label ID="lblref" runat="server" Text='<%#Bind("Ref_number") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Code">
                                <ItemTemplate>
                                    <asp:Label ID="lblitem" runat="server" Text='<%#Bind("Item_code") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Description" HeaderStyle-Width="200px">
                                <ItemTemplate>
                                    <asp:Label ID="lblitemdesc" runat="server" Text='<%#Bind("Item_Desc") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHeaderTargetPrice" Text="Expected Price" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblTargetPrice" Text='<%#Bind("Expected_price") %>' CssClass="ddl" runat="server" Style="width: 80px;"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                           
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblNewOfferPrice" Text="Special Price" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="txtNewOfferPrice"  CssClass="ddl" Style="width: 80px;" runat="server" Text='<%#Bind("New_OfferPrice") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                           
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="Label3" Text="Actions" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div style="width: 90px;">
                                        <asp:ImageButton ToolTip="Added In PA"  ImageUrl="images/icons/approve.png" OnClientClick="AddedInPA(event,this);" ID="btnAddInPA"  runat="server" style="height:25px;" />
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                            </div>
                        </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnMultiAdded" />
                    <asp:PostBackTrigger ControlID="btndownload" />
                </Triggers>
            </asp:UpdatePanel>
            <asp:UpdateProgress ID="updateProgress" runat="server">
                <ProgressTemplate>
                    <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                        <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color: #fff">Please wait</span>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>

    </div>
</asp:Content>
