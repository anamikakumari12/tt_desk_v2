﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TTDeskBL;
using TTDeskBO;

namespace DistributorSystem
{
    public partial class QuoteStatusDetails : System.Web.UI.Page
    {
        #region GlobalDeclaration
        CommonFunctions objCom = new CommonFunctions();
        #endregion

        #region Events

        /// <summary>
        /// Author : Anamika Kumari
        /// Date : Aug 26, 2020
        /// Desc : To Load the status log for selected item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["LoginMailId"]))) { Response.Redirect("Login.aspx"); return; }

            if (!IsPostBack)
            {
                try
                {
                    string item = Request.QueryString["item"];
                    string desc = Request.QueryString["desc"];
                    string ref_number = Request.QueryString["ref"];
                    if (string.IsNullOrEmpty(item) || string.IsNullOrEmpty(ref_number))
                    {
                        lblmsg.Text = "There is a problem fetching details for selected quote. Please try again.";
                        grdStatusLog.DataSource = null;
                        grdStatusLog.DataBind();
                    }
                    else
                    {
                        DataTable dt = new DataTable();
                        QuoteBO objQuoteBO = new QuoteBO();
                        QuoteBL objquoteBL = new QuoteBL();
                        objQuoteBO.Item_Number = item;
                        objQuoteBO.Ref_Number = ref_number;
                        lblTitle.Text = "Status Log For " + ref_number;
                        lblSubTitle.Text = item + "(" + desc + ")";
                        dt = objquoteBL.GetQuoteStatusLogBL(objQuoteBO);

                        if (dt != null)
                        {
                            if (dt.Rows.Count > 0)
                            {
                               
                                grdStatusLog.DataSource = dt;
                                grdStatusLog.DataBind();
                            }
                            else
                            {
                                grdStatusLog.DataSource = null;
                                grdStatusLog.DataBind();
                            }

                        }
                    }

                }
                catch (Exception ex)
                {
                    lblmsg.Text = "Error in loading. Please try again.";
                    objCom.ErrorLog(ex);
                }

            }

        }
        #endregion
    }
}