﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TTDeskBO;
using TTDeskBL;
using System.Data;
using System.Web.Services;
using System.Reflection;
using System.Configuration;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;

namespace TT_Desk
{
    public partial class SpecialQuote : System.Web.UI.Page
    {
        #region GlobalDeclaration
        CommonFunctions objCom = new CommonFunctions();
        QuoteBL objQuoteBL = new QuoteBL();
        SplQuoteBO objSplQuoteBO = new SplQuoteBO();
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["LoginMailId"]))) { Response.Redirect("Login.aspx"); return; }

            DataTable dt = new DataTable();
            if (!IsPostBack)
            {
                objQuoteBL = new QuoteBL();
                try
                {
                    
                    //objQuoteBL = new QuoteBL();
                    //objSplQuoteBO = new SplQuoteBO();
                    //objSplQuoteBO.CustomerNumber = "1197";
                    //objSplQuoteBO.Payment_SourceType = "GAL";
                    //dt = new DataTable();
                    //dt = objQuoteBL.GetPaymentTermsBL(objSplQuoteBO);
                    //if (dt.Rows.Count > 0)
                    //{
                    //    ddlPT.DataSource = dt;
                    //    ddlPT.DataTextField = "Payment_Terms";
                    //    ddlPT.DataValueField = "Payment_Terms";
                    //    ddlPT.DataBind();
                    //    ddlPT.Items.Insert(0, "-- Select --");
                    //}
                    //else
                    //{
                    //    ddlPT.DataSource = null;
                    //    ddlPT.DataBind();
                    //}
                    dt = objQuoteBL.GetFamilyBL();
                    if (dt.Rows.Count > 0)
                    {
                        ddlFamily.DataSource = dt;
                        ddlFamily.DataTextField = "Family_Name";
                        ddlFamily.DataValueField = "Family_Name";
                        ddlFamily.DataBind();
                        ddlFamily.Items.Insert(0, "-- Select --");
                    }
                    else
                    {
                        ddlFamily.DataSource = null;
                        ddlFamily.DataBind();
                    }
                    
                }
                catch (Exception ex)
                {
                    objCom.ErrorLog(ex);
                }
            }
           
        }

        protected void ddlCat1_SelectedIndexChanged(object sender, EventArgs e)
        {
            objQuoteBL = new QuoteBL();
            objSplQuoteBO = new SplQuoteBO();
            DataTable dt = new DataTable();
            try
            {
                objSplQuoteBO.DropdwonValue = Convert.ToString(ddlCat1.SelectedValue);
                objSplQuoteBO.DropdwonNumber = 2;
                objSplQuoteBO.Family = Convert.ToString(ddlFamily.SelectedValue);

                dt = objQuoteBL.GetSplQuoteDropdownsBL(objSplQuoteBO);
                if (dt.Rows.Count > 0)
                {
                    ddlCat2.DataSource = dt;
                    ddlCat2.DataTextField = "Value";
                    ddlCat2.DataValueField = "Value";
                    ddlCat2.DataBind();
                    ddlCat2.Items.Insert(0, "-- Select --");
                    lblCat2.Text = Convert.ToString(dt.Rows[0]["Text"]);
                    hdnCat2.Value = Convert.ToString(dt.Rows[0]["Text"]);
                    ddlCat2.Visible = true;
                    lblCat2.Visible = true;
                }
                else
                {
                    ddlCat2.DataSource = null;
                    ddlCat2.DataBind();
                    ddlCat2.Visible = false;
                    lblCat2.Visible = false;
                }
                divItem.Visible = false;
                ddlCat3.Visible = false;
                lblCat3.Visible = false;
                ddlCat4.Visible = false;
                lblCat4.Visible = false;
                ddlCat5.Visible = false;
                lblCat5.Visible = false;
                ddlCat6.Visible = false;
                lblCat6.Visible = false;
                ddlCat7.Visible = false;
                lblCat7.Visible = false;
                ddlCat8.Visible = false;
                lblCat8.Visible = false;

                txtItem.Text = "";
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlCat2_SelectedIndexChanged(object sender, EventArgs e)
        {
            objQuoteBL = new QuoteBL();
            objSplQuoteBO = new SplQuoteBO();
            DataTable dt = new DataTable();
            try
            {
                objSplQuoteBO.DropdwonValue = Convert.ToString(ddlCat2.SelectedValue);
                objSplQuoteBO.DropdwonNumber = 3;
                objSplQuoteBO.Family = Convert.ToString(ddlFamily.SelectedValue);
                dt = objQuoteBL.GetSplQuoteDropdownsBL(objSplQuoteBO);
                if (dt.Rows.Count > 0)
                {
                    ddlCat3.DataSource = dt;
                    ddlCat3.DataTextField = "Value";
                    ddlCat3.DataValueField = "Value";
                    ddlCat3.DataBind();
                    ddlCat3.Items.Insert(0, "-- Select --");
                    lblCat3.Text = Convert.ToString(dt.Rows[0]["Text"]);
                    hdnCat3.Value = Convert.ToString(dt.Rows[0]["Text"]);
                    ddlCat3.Visible = true;
                    lblCat3.Visible = true;
                }
                else
                {
                    ddlCat3.DataSource = null;
                    ddlCat3.DataBind();
                    ddlCat3.Visible = false;
                    lblCat3.Visible = false;
                }
                ddlCat4.Visible = false;
                lblCat4.Visible = false;
                ddlCat5.Visible = false;
                lblCat5.Visible = false;
                ddlCat6.Visible = false;
                lblCat6.Visible = false;
                ddlCat7.Visible = false;
                lblCat7.Visible = false;
                ddlCat8.Visible = false;
                lblCat8.Visible = false;
                divItem.Visible = false;
                txtItem.Text = "";
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlCat3_SelectedIndexChanged(object sender, EventArgs e)
        {
            objQuoteBL = new QuoteBL();
            objSplQuoteBO = new SplQuoteBO();
            DataTable dt = new DataTable();
            try
            {
                objSplQuoteBO.DropdwonValue = Convert.ToString(ddlCat3.SelectedValue);
                objSplQuoteBO.DropdwonNumber = 4;
                objSplQuoteBO.Family = Convert.ToString(ddlFamily.SelectedValue);
                dt = objQuoteBL.GetSplQuoteDropdownsBL(objSplQuoteBO);
                if (dt.Rows.Count > 0)
                {
                    ddlCat4.DataSource = dt;
                    ddlCat4.DataTextField = "Value";
                    ddlCat4.DataValueField = "Value";
                    ddlCat4.DataBind();
                    ddlCat4.Items.Insert(0, "-- Select --");
                    lblCat4.Text = Convert.ToString(dt.Rows[0]["Text"]);
                    hdnCat4.Value = Convert.ToString(dt.Rows[0]["Text"]);
                    ddlCat4.Visible = true;
                    lblCat4.Visible = true;
                }
                else
                {
                    ddlCat4.DataSource = null;
                    ddlCat4.DataBind();
                    ddlCat4.Visible = false;
                    lblCat4.Visible = false;
                }
                divItem.Visible = false;
                txtItem.Text = "";
                ddlCat5.Visible = false;
                lblCat5.Visible = false;
                ddlCat6.Visible = false;
                lblCat6.Visible = false;
                ddlCat7.Visible = false;
                lblCat7.Visible = false;
                ddlCat8.Visible = false;
                lblCat8.Visible = false;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlCat4_SelectedIndexChanged(object sender, EventArgs e)
        {
            objQuoteBL = new QuoteBL();
            objSplQuoteBO = new SplQuoteBO();
            DataTable dt = new DataTable();
            try
            {
                objSplQuoteBO.DropdwonValue = Convert.ToString(ddlCat4.SelectedValue);
                objSplQuoteBO.DropdwonNumber = 5;
                objSplQuoteBO.Family = Convert.ToString(ddlFamily.SelectedValue);
                dt = objQuoteBL.GetSplQuoteDropdownsBL(objSplQuoteBO);
                if (dt.Rows.Count > 0)
                {
                    ddlCat5.DataSource = dt;
                    ddlCat5.DataTextField = "Value";
                    ddlCat5.DataValueField = "Value";
                    ddlCat5.DataBind();
                    ddlCat5.Items.Insert(0, "-- Select --");
                    lblCat5.Text = Convert.ToString(dt.Rows[0]["Text"]);
                    hdnCat5.Value = Convert.ToString(dt.Rows[0]["Text"]);
                    ddlCat5.Visible = true;
                    lblCat5.Visible = true;
                }
                else
                {
                    ddlCat5.DataSource = null;
                    ddlCat5.DataBind();
                    ddlCat5.Visible = false;
                    lblCat5.Visible = false;
                }
                divItem.Visible = false;
                txtItem.Text = "";
                ddlCat6.Visible = false;
                lblCat6.Visible = false;
                ddlCat7.Visible = false;
                lblCat7.Visible = false;
                ddlCat8.Visible = false;
                lblCat8.Visible = false;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlCat5_SelectedIndexChanged(object sender, EventArgs e)
        {
            objQuoteBL = new QuoteBL();
            objSplQuoteBO = new SplQuoteBO();
            DataTable dt = new DataTable();
            try
            {
                objSplQuoteBO.DropdwonValue = Convert.ToString(ddlCat5.SelectedValue);
                objSplQuoteBO.DropdwonNumber = 6;
                objSplQuoteBO.Family = Convert.ToString(ddlFamily.SelectedValue);
                dt = objQuoteBL.GetSplQuoteDropdownsBL(objSplQuoteBO);
                if (dt.Rows.Count > 0)
                {
                    ddlCat6.DataSource = dt;
                    ddlCat6.DataTextField = "Value";
                    ddlCat6.DataValueField = "Value";
                    ddlCat6.DataBind();
                    ddlCat6.Items.Insert(0, "-- Select --");
                    lblCat6.Text = Convert.ToString(dt.Rows[0]["Text"]);
                    hdnCat6.Value = Convert.ToString(dt.Rows[0]["Text"]);
                    ddlCat6.Visible = true;
                    lblCat6.Visible = true;
                }
                else
                {
                    ddlCat6.DataSource = null;
                    ddlCat6.DataBind();
                    ddlCat6.Visible = false;
                    lblCat6.Visible = false;
                }
                divItem.Visible = false;
                txtItem.Text = "";
                ddlCat7.Visible = false;
                lblCat7.Visible = false;
                ddlCat8.Visible = false;
                lblCat8.Visible = false;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlCat6_SelectedIndexChanged(object sender, EventArgs e)
        {
            objQuoteBL = new QuoteBL();
            objSplQuoteBO = new SplQuoteBO();
            DataTable dt = new DataTable();
            try
            {
                objSplQuoteBO.DropdwonValue = Convert.ToString(ddlCat6.SelectedValue);
                objSplQuoteBO.DropdwonNumber = 7;
                objSplQuoteBO.Family = Convert.ToString(ddlFamily.SelectedValue);
                dt = objQuoteBL.GetSplQuoteDropdownsBL(objSplQuoteBO);
                if (dt.Rows.Count > 0)
                {
                    ddlCat7.DataSource = dt;
                    ddlCat7.DataTextField = "Value";
                    ddlCat7.DataValueField = "Value";
                    ddlCat7.DataBind();
                    ddlCat7.Items.Insert(0, "-- Select --");
                    lblCat7.Text = Convert.ToString(dt.Rows[0]["Text"]);
                    hdnCat7.Value = Convert.ToString(dt.Rows[0]["Text"]);
                    ddlCat7.Visible = true;
                    lblCat7.Visible = true;
                }
                else
                {
                    ddlCat7.DataSource = null;
                    ddlCat7.DataBind();
                    ddlCat7.Visible = false;
                    lblCat7.Visible = false;
                }
                divItem.Visible = false;
                txtItem.Text = "";
                ddlCat8.Visible = false;
                lblCat8.Visible = false;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlCat7_SelectedIndexChanged(object sender, EventArgs e)
        {
            objQuoteBL = new QuoteBL();
            objSplQuoteBO = new SplQuoteBO();
            DataTable dt = new DataTable();
            try
            {
                objSplQuoteBO.DropdwonValue = Convert.ToString(ddlCat7.SelectedValue);
                objSplQuoteBO.DropdwonNumber = 8;
                objSplQuoteBO.Family = Convert.ToString(ddlFamily.SelectedValue);
                dt = objQuoteBL.GetSplQuoteDropdownsBL(objSplQuoteBO);
                if (dt.Rows.Count > 0)
                {
                    ddlCat8.DataSource = dt;
                    ddlCat8.DataTextField = "Value";
                    ddlCat8.DataValueField = "ID";
                    ddlCat8.DataBind();
                    ddlCat8.Items.Insert(0, "-- Select --");
                    lblCat8.Text = Convert.ToString(dt.Rows[0]["Text"]);
                    hdnCat8.Value = Convert.ToString(dt.Rows[0]["Text"]);
                    ddlCat8.Visible = true;
                    lblCat8.Visible = true;
                }
                else
                {
                    ddlCat8.DataSource = null;
                    ddlCat8.DataBind();
                    ddlCat8.Visible = false;
                    lblCat8.Visible = false;
                }
                divItem.Visible = false;
                txtItem.Text = "";
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlCustomerType_SelectedIndexChanged(object sender, EventArgs e)
        {
            objQuoteBL = new QuoteBL();
            objSplQuoteBO = new SplQuoteBO();
            DataTable dt = new DataTable();
            try
            {
                objSplQuoteBO.CustomerType = Convert.ToString(ddlCustomerType.SelectedValue);
                dt = objQuoteBL.GetCustomerDetailsBL(objSplQuoteBO);
                if (dt.Rows.Count > 0)
                {
                    ddlCustomers.DataSource = dt;
                    ddlCustomers.DataTextField = "customer_short_name";
                    ddlCustomers.DataValueField = "customer_number";
                    ddlCustomers.DataBind();
                    ddlCustomers.Items.Insert(0, "-- Select --");
                    ddlCustomers.Visible = true;
                    lblCustomers.Visible = true;
                    lblCustomers.Text = Convert.ToString(ddlCustomerType.SelectedItem.Text);
                    divCustomers.Visible = true;
                }
                else
                {
                    ddlCustomers.DataSource = null;
                    ddlCustomers.DataBind();
                    ddlCustomers.Visible = false;
                    lblCustomers.Visible = false;
                    divCustomers.Visible = false;
                }
                divCPCustomers.Visible = false;
                ddlCat1.Visible = false;
                lblCat1.Visible = false;
                ddlCat2.Visible = false;
                lblCat2.Visible = false;
                ddlCat3.Visible = false;
                lblCat3.Visible = false;
                ddlCat4.Visible = false;
                lblCat4.Visible = false;
                ddlCat5.Visible = false;
                lblCat5.Visible = false;
                ddlCat6.Visible = false;
                lblCat6.Visible = false;
                ddlCat7.Visible = false;
                lblCat7.Visible = false;
                ddlCat8.Visible = false;
                lblCat8.Visible = false;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void ddlCustomers_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt;
            if (ddlCustomerType.SelectedValue=="D")
            {
                objQuoteBL = new QuoteBL();
                objSplQuoteBO = new SplQuoteBO();
                dt = new DataTable();
                try
                {
                    objSplQuoteBO.Distributor = Convert.ToString(ddlCustomers.SelectedValue);
                    dt = objQuoteBL.GetCPCustomerDetailsBL(objSplQuoteBO);
                    if (dt.Rows.Count > 0)
                    {
                        ddlCPCustomers.DataSource = dt;
                        ddlCPCustomers.DataTextField = "customername";
                        ddlCPCustomers.DataValueField = "customernumber";
                        ddlCPCustomers.DataBind();
                        ddlCPCustomers.Items.Insert(0, "-- Select --");
                        divCPCustomers.Visible = true;

                    }
                    else
                    {
                        ddlCPCustomers.DataSource = null;
                        ddlCPCustomers.DataBind();
                        divCPCustomers.Visible = true;
                    }
                }
                catch (Exception ex)
                {
                    objCom.ErrorLog(ex);
                }
            }

            
            

            objQuoteBL = new QuoteBL();
            objSplQuoteBO = new SplQuoteBO();
            try
            {
                int SplQuoteId = 0;
                SplQuoteId = objQuoteBL.GetMaxQuoteIDBL();
                Session["SplQuoteId"] = SplQuoteId;
                hdnID.Value = Convert.ToString(SplQuoteId);

                objQuoteBL = new QuoteBL();
                objSplQuoteBO = new SplQuoteBO();
                objSplQuoteBO.CustomerNumber = Convert.ToString(ddlCustomers.SelectedValue);
                dt = new DataTable();
                dt = objQuoteBL.GetItemDescBL(objSplQuoteBO);
                Session["ItemTable"] = dt;

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            //objQuoteBL = new QuoteBL();
            //objSplQuoteBO = new SplQuoteBO();
            //dt = new DataTable();
            //try
            //{
            //    objSplQuoteBO.CustomerNumber = Convert.ToString(ddlCustomers.SelectedValue);
            //    dt = objQuoteBL.GetItemDetailsBL(objSplQuoteBO);
            //    if (dt.Rows.Count > 0)
            //    {
            //        ddlItem.DataSource = dt;
            //        ddlItem.DataTextField = "item_desc";
            //        ddlItem.DataValueField = "item";
            //        ddlItem.DataBind();
            //        ddlItem.Items.Insert(0, "-- Select --");
            //    }
            //    else
            //    {
            //        ddlItem.DataSource = null;
            //        ddlItem.DataBind();
            //    }
            //}
            //catch (Exception ex)
            //{
            //    objCom.ErrorLog(ex);
            //}
        }


        [WebMethod]
        public static Output CreateRFQ(List<SplQuoteStatusDB> obj)
        {
            CommonFunctions objCom = new CommonFunctions();
            Output objOutput = new Output();
            List<PDFOutput> myojb = new List<PDFOutput>();
            string emailAttachments = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(Convert.ToString(HttpContext.Current.Session["UserId"])))
                {
                    foreach (SplQuoteStatusDB item in obj)
                    {
                        if(!string.IsNullOrEmpty(item.Item_File) && !(item.family.Contains("Manual")))
                            emailAttachments = emailAttachments + item.Item_File+";";
                        item.GST = item.GST.Contains("%") ? item.GST : string.Concat(item.GST, "%");
                        //item.ID = Convert.ToInt32(HttpContext.Current.Session["SplQuoteId"]);
                        //item.RequestedBy = Convert.ToString(HttpContext.Current.Session["UserId"]);
                        //item.RequestedBy_Flag = "DESK";
                    }

                    if (!string.IsNullOrEmpty(obj[0].Common_File))
                        emailAttachments = emailAttachments + obj[0].Common_File + ";";
                    DataTable dt = ToDataTable<SplQuoteStatusDB>(obj);
                    dt.Columns.Remove("family");
                    QuoteBL objQuoteBL = new QuoteBL();
                    SplQuoteBO objQuoteBO = new SplQuoteBO();
                    objQuoteBO = objQuoteBL.SaveSplQuotesBL(dt);

                    if (objQuoteBO.Err_code == 0)
                    {
                        
                        string ref_number = Convert.ToString(objQuoteBO.Ref_Number);
                        myojb= callAPI(ref_number);
                        if (Convert.ToInt32(myojb[0].err_code)==200)
                        {
                            //string attachment = GenerateQuoteFormat(objQuoteBO.Ref_Number);
                            emailAttachments = emailAttachments + Convert.ToString( myojb[0].filename);
                            EmailDetails objEmail = new EmailDetails();
                            objEmail.toMailId = objQuoteBO.TO;
                            objEmail.ccMailId = objQuoteBO.CC;
                            objEmail.subject = objQuoteBO.Subject;
                            objEmail.body = objQuoteBO.Body;
                            objEmail.attachment = emailAttachments;
                            //objCom.SendMail(objEmail);
                            CommonFunctions.SendGridMail(objEmail).Wait();
                        }
                        objOutput.ErrorCode = 200;
                        objOutput.ErrorMsg = "Requests for quotes are submitted successfully.";
                    }
                    else
                    {
                        objOutput.ErrorCode = 201;
                        objOutput.ErrorMsg = "There is error in saving data. Please try again." + objQuoteBO.Err_msg;
                    }
                }
                else
                {
                    objOutput.ErrorCode = 201;
                    objOutput.ErrorMsg = "Session Time out.";
                }


            }
            catch (Exception ex)
            {
                objOutput.ErrorCode = 201;
                objOutput.ErrorMsg = "There is error in saving data. Please try again.";
                objCom.ErrorLog(ex);
            }
            return objOutput;
        }
        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name, type);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = objQuoteBL.GetDeliveryWeekBL();
                if (dt.Rows.Count > 0)
                {
                    ddlDW.DataSource = dt;
                    ddlDW.DataTextField = "Delivery_Week";
                    ddlDW.DataValueField = "Delivery_Week";
                    ddlDW.DataBind();
                    ddlDW.Items.Insert(0, "-- Select --");
                }
                else
                {
                    ddlDW.DataSource = null;
                    ddlDW.DataBind();
                }
                divItem.Visible = true;
                objQuoteBL = new QuoteBL();
                objSplQuoteBO = new SplQuoteBO();
                objSplQuoteBO.Value1 = (ddlCat1.Visible) ? Convert.ToString(ddlCat1.SelectedItem.Text) : null;
                objSplQuoteBO.Value2 = (ddlCat2.Visible) ? Convert.ToString(ddlCat2.SelectedItem.Text) : null;
                objSplQuoteBO.Value3 = (ddlCat3.Visible) ? Convert.ToString(ddlCat3.SelectedItem.Text) : null;
                objSplQuoteBO.Value4 = (ddlCat4.Visible) ? Convert.ToString(ddlCat4.SelectedItem.Text) : null;
                objSplQuoteBO.Value5 = (ddlCat5.Visible) ? Convert.ToString(ddlCat5.SelectedItem.Text) : null;
                objSplQuoteBO.Value6 = (ddlCat6.Visible) ? Convert.ToString(ddlCat6.SelectedItem.Text) : null;
                objSplQuoteBO.Value7 = (ddlCat7.Visible) ? Convert.ToString(ddlCat7.SelectedItem.Text) : null;
                objSplQuoteBO.Value8 = (ddlCat8.Visible) ? Convert.ToString(ddlCat8.SelectedItem.Text) : null;
                ddlCustomerType.Enabled = false;
                ddlCustomers.Enabled = false;
                ddlCPCustomers.Enabled = false;
                dt = new DataTable();
                dt = objQuoteBL.GetOfferPriceSplItemBL(objSplQuoteBO);
                if (dt.Rows.Count > 0)
                {
                    if(Convert.ToInt32(Session["Manual_Flag"])>0)
                    {
                        hdnQty.Value = "";
                    }
                    else
                    {
                        hdnQty.Value = Convert.ToString(dt.Rows[0]["QTY"]);
                    }
                    //hdnOfferPrice.Value = Convert.ToString(dt.Rows[0]["OFFER_PRICE"]);
                   
                    hdnCatID.Value = Convert.ToString(dt.Rows[0]["ID"]);
                    txtPrice.Text = Convert.ToString(dt.Rows[0]["OFFER_PRICE"]);
                }
              
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private static List<PDFOutput> callAPI(string ref_number)
        {
            CommonFunctions objCom = new CommonFunctions();
            List<PDFOutput> myojb = new List<PDFOutput>();
            try
            {
                string ReportGenerateServiceURL = Convert.ToString(ConfigurationManager.AppSettings["ReportGenerateServiceURL"]);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ReportGenerateServiceURL);
                request.Method = "POST";
                //SampleModel model = new SampleModel();
                //model.PostData = "Test";
                request.ContentType = "application/json";

                //JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (var sw = new StreamWriter(request.GetRequestStream()))
                {
                    //string json = serializer.Serialize(model);
                    string json = "{\"Ref_Number\": \"" + ref_number + "\"}";
                    sw.Write(json);
                    sw.Flush();
                }
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    var objText = reader.ReadToEnd();
                    myojb = (List<PDFOutput>)js.Deserialize(objText, typeof(List<PDFOutput>));
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return myojb;
        }

        [WebMethod]
        public static List<PaymentTerms> GetPaymentTerms(List<PaymentTerms> obj)
        {
            CommonFunctions objCom = new CommonFunctions();
            List<PaymentTerms> objList = new List<PaymentTerms>();
            QuoteBL objQuoteBL = new QuoteBL();
            PaymentTerms obj1;
            try
            {
                SplQuoteBO objSplQuoteBO = new SplQuoteBO();
                objSplQuoteBO.CustomerNumber = Convert.ToString(obj[0].customer_number);
                objSplQuoteBO.Payment_SourceType = "GAL";
                DataTable dt = new DataTable();
                dt = objQuoteBL.GetPaymentTermsBL(objSplQuoteBO);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        obj1 = new PaymentTerms();
                        obj1.PaymenTerms = Convert.ToString(dt.Rows[i]["Payment_Terms"]);
                        objList.Add(obj1);
                    }
                }
                else
                {
                    obj1 = new PaymentTerms();
                    objList.Add(obj1);
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
                obj1 = new PaymentTerms();
                objList.Add(obj1);
            }
            return objList;
        }

        protected void ddlFamily_SelectedIndexChanged(object sender, EventArgs e)
        {
            objQuoteBL = new QuoteBL();
            objSplQuoteBO = new SplQuoteBO();
            DataTable dt = new DataTable();
            try
            {
                objSplQuoteBO.DropdwonValue = "";
                objSplQuoteBO.DropdwonNumber = 1;
                if(Convert.ToString(ddlFamily.SelectedValue).Contains("Manual"))
                {
                    Session["Manual_Flag"] = 1;
                    txtPrice.Enabled = true;
                    txtPrice.Text = "";
                    hdnManualFlag.Value = "1";
                    if (Convert.ToString(ddlFamily.SelectedValue).Contains("Others - Manual"))
                    {
                        if (RadioButton2.Checked)
                        {
                            txtItem.Visible = false;
                            ddlitem.Visible = true;
                        }
                        else
                        {
                            txtItem.Visible = true;
                            ddlitem.Visible = false;
                        }
                        RadioButton1.Enabled = true;
                        RadioButton2.Enabled = true;
                    }
                    else
                    {
                        //if (RadioButton2.Checked)
                        //{
                        //    txtItem.Visible = false;
                        //    ddlitem.Visible = true;
                        //}
                        //else
                        //{
                        //    txtItem.Visible = true;
                        //    ddlitem.Visible = false;
                        //}
                        RadioButton1.Checked = true;
                        RadioButton2.Checked = false;
                        txtItem.Visible = true;
                        ddlitem.Visible = false;
                        txtMOQ.Text = "";
                        RadioButton1.Enabled = false;
                        RadioButton2.Enabled = false;
                    }
                }
                else
                {
                    //if (RadioButton2.Checked)
                    //{
                    //    txtItem.Visible = false;
                    //    ddlitem.Visible = true;
                    //}
                    //else
                    //{
                    //    txtItem.Visible = true;
                    //    ddlitem.Visible = false;
                    //}
                    RadioButton1.Checked = true;
                    RadioButton2.Checked = false;
                    txtItem.Visible = true;
                    ddlitem.Visible = false;
                    txtMOQ.Text = "";
                    RadioButton1.Enabled = false;
                    RadioButton2.Enabled = false;
                    Session["Manual_Flag"] = 0;
                    txtPrice.Enabled = false;
                    hdnManualFlag.Value = "0";
                    RadioButton1.Enabled = false;
                    RadioButton2.Enabled = false;

                }
                objSplQuoteBO.Family = Convert.ToString(ddlFamily.SelectedValue);
                dt = objQuoteBL.GetSplQuoteDropdownsBL(objSplQuoteBO);
                if (dt.Rows.Count > 0)
                {
                    ddlCat1.DataSource = dt;
                    ddlCat1.DataTextField = "Value";
                    ddlCat1.DataValueField = "Value";
                    ddlCat1.DataBind();
                    ddlCat1.Items.Insert(0, "-- Select --");
                    lblCat1.Text = Convert.ToString(dt.Rows[0]["Text"]);
                    hdnCat1.Value = Convert.ToString(dt.Rows[0]["Text"]);
                    ddlCat1.Visible = true;
                    lblCat1.Visible = true;
                    ddlCat2.Visible = false;
                    lblCat2.Visible = false;
                    ddlCat3.Visible = false;
                    lblCat3.Visible = false;
                    ddlCat4.Visible = false;
                    lblCat4.Visible = false;
                    ddlCat5.Visible = false;
                    lblCat5.Visible = false;
                    ddlCat6.Visible = false;
                    lblCat6.Visible = false;
                    ddlCat7.Visible = false;
                    lblCat7.Visible = false;
                    ddlCat8.Visible = false;
                    lblCat8.Visible = false;
                }
                else
                {
                    ddlCat1.DataSource = null;
                    ddlCat1.DataBind();
                    ddlCat1.Visible = false;
                    lblCat1.Visible = false;
                    ddlCat2.Visible = false;
                    lblCat2.Visible = false;
                    ddlCat3.Visible = false;
                    lblCat3.Visible = false;
                    ddlCat4.Visible = false;
                    lblCat4.Visible = false;
                    ddlCat5.Visible = false;
                    lblCat5.Visible = false;
                    ddlCat6.Visible = false;
                    lblCat6.Visible = false;
                    ddlCat7.Visible = false;
                    lblCat7.Visible = false;
                    ddlCat8.Visible = false;
                    lblCat8.Visible = false;
                }
                divItem.Visible = false;
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        protected void RadioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (RadioButton2.Checked)
            {
                txtItem.Visible = false;
                ddlitem.Visible = true;
                ScriptManager.RegisterStartupScript(Page, this.GetType(), "Javascript", "javascript:LoadDropdowns(); ", true);
            }
            else
            {
                txtItem.Visible = true;
                ddlitem.Visible = false;
            }
        }
    }
}