﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TTDeskBO;

namespace TT_Desk
{
    public partial class QuoteSummaryDetails : System.Web.UI.Page
    {
        #region GlobalDeclaration
        CommonFunctions objCom = new CommonFunctions();
        #endregion

        #region Events

        /// <summary>
        /// Author : Anamika Kumari
        /// Date : Feb 19, 2020
        /// Desc : To Load all the details of selected quote item and escalated quote item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["LoginMailId"]))) { Response.Redirect("Login.aspx"); return; }

            if (!IsPostBack)
            {
                try
                {
                    string id = Request.QueryString["id"];
                    string flag = Request.QueryString["flag"];
                    if (string.IsNullOrEmpty(id))
                    {
                        lblmsg.Text = "There is a problem fetching details for selected quote. Please try again.";
                    }
                    else
                    {
                        DataTable dt = new DataTable();

                        if (flag == "Summary")
                        {
                            dt = (DataTable)Session["dtQuoteDetails"];

                            if (dt != null)
                            {
                                if (dt.Rows.Count > 0)
                                {
                                    var rows = from row in dt.AsEnumerable()
                                               where row.Field<int>("ID") == Convert.ToInt32(id)
                                               select row;
                                    dt = rows.CopyToDataTable();
                                    if (dt.Rows.Count > 0)
                                    {
                                        hdnitem.Value = Convert.ToString(dt.Rows[0]["Item_code"]);
                                        hdnID.Value = Convert.ToString(dt.Rows[0]["ID"]);
                                        lblref.Text = Convert.ToString(dt.Rows[0]["Ref_number"]);
                                        lblItem.Text = String.Concat(Convert.ToString(dt.Rows[0]["Item_Desc"]));
                                        lblWHS.Text = Convert.ToString(dt.Rows[0]["WHS"]);
                                        //lblOrderType.Text = Convert.ToString(dt.Rows[0]["Order_type"]);
                                        //lblOrderFrequency.Text = Convert.ToString(dt.Rows[0]["Order_frequency"]);
                                        lblTotalQty.Text = Convert.ToString(dt.Rows[0]["Total_QTY"]);
                                        lblLP.Text = Convert.ToString(dt.Rows[0]["List_Price"]);
                                        //lblEP.Text = Convert.ToString(dt.Rows[0]["Expected_price"]);
                                        //lblDR.Text = Convert.ToString(dt.Rows[0]["DC_rate"]);
                                        lblCP.Text = Convert.ToString(dt.Rows[0]["CP_Name"]);
                                        lblCustomer.Text = Convert.ToString(dt.Rows[0]["Cust_Name"]);
                                        lblSP.Text = Convert.ToString(dt.Rows[0]["Cust_SP"]);
                                        //lblComC.Text = Convert.ToString(dt.Rows[0]["Comp_Name"]);
                                        //lblComP.Text = Convert.ToString(dt.Rows[0]["Comp_Desc"]);
                                        //lblComSP.Text = Convert.ToString(dt.Rows[0]["Comp_SP"]);
                                        //lblReqMOQ.Text = Convert.ToString(dt.Rows[0]["QTY_perOrder"]);
                                        lblaveSell.Text = Convert.ToString(dt.Rows[0]["AverageSellingPrice"]);
                                        lblUnitPrice.Text = Convert.ToString(dt.Rows[0]["UnitPrice"]);
                                        lblStock.Text = Convert.ToString(dt.Rows[0]["Stock"]);
                                        lblStockCode.Text = Convert.ToString(dt.Rows[0]["StockCode"]);
                                        lblOrdValidity.Text = Convert.ToString(dt.Rows[0]["Order_Validity"]);
                                        lblGP.Text = Convert.ToString(dt.Rows[0]["GP"]);
                                        //lbltotalOrderValue.Text = Convert.ToString(dt.Rows[0]["TotOrderValue"]);
                                        lblOfferPrice.Text = Convert.ToString(dt.Rows[0]["Offer_price"]);
                                        //lblMOQ.Text = Convert.ToString(dt.Rows[0]["MOQ"]);
                                        lblNewOfferPrice.Text = Convert.ToString(dt.Rows[0]["New_OfferPrice"]);
                                        lblOrderQuant.Text = Convert.ToString(dt.Rows[0]["Approved_OrderQty"]);
                                        lblCom_head.Text = "Last Upadated Comment: ";
                                        lblComment.Text = Convert.ToString(dt.Rows[0]["StatusChange_Comment"]);
                                    }
                                }

                            }
                        }
                        else
                        {
                            dt = (DataTable)Session["EscalatedQuotes"];

                            if (dt != null)
                            {
                                if (dt.Rows.Count > 0)
                                {
                                    var rows = from row in dt.AsEnumerable()
                                               where row.Field<int>("ID") == Convert.ToInt32(id)
                                               select row;
                                    dt = rows.CopyToDataTable();
                                    if (dt.Rows.Count > 0)
                                    {
                                        hdnitem.Value = Convert.ToString(dt.Rows[0]["Item_code"]);
                                        hdnID.Value = Convert.ToString(dt.Rows[0]["ID"]);
                                        lblref.Text = Convert.ToString(dt.Rows[0]["Ref_number"]);
                                        lblItem.Text = String.Concat(Convert.ToString(dt.Rows[0]["Item_Desc"]));
                                        lblWHS.Text = Convert.ToString(dt.Rows[0]["WHS"]);
                                        lblTotalQty.Text = Convert.ToString(dt.Rows[0]["Total_QTY"]);
                                        lblLP.Text = Convert.ToString(dt.Rows[0]["List_Price"]);
                                        //lblEP.Text = Convert.ToString(dt.Rows[0]["Expected_price"]);
                                        //lblDR.Text = Convert.ToString(dt.Rows[0]["DC_rate"]);
                                        lblCP.Text = Convert.ToString(dt.Rows[0]["CP_Name"]);
                                        lblCustomer.Text = Convert.ToString(dt.Rows[0]["Cust_Name"]);
                                        lblSP.Text = Convert.ToString(dt.Rows[0]["Cust_SP"]);
                                       // lblReqMOQ.Text = Convert.ToString(dt.Rows[0]["QTY_perOrder"]);
                                        lblaveSell.Text = Convert.ToString(dt.Rows[0]["AverageSellingPrice"]);
                                        lblUnitPrice.Text = Convert.ToString(dt.Rows[0]["UnitPrice"]);
                                        lblStock.Text = Convert.ToString(dt.Rows[0]["Stock"]);
                                        lblStockCode.Text = Convert.ToString(dt.Rows[0]["StockCode"]);
                                        lblOrdValidity.Text = Convert.ToString(dt.Rows[0]["OrderValidity"]);
                                        lblGP.Text = Convert.ToString(dt.Rows[0]["GP"]);
                                        lblOfferPrice.Text = Convert.ToString(dt.Rows[0]["Offer_price"]);
                                        //lblMOQ.Text = Convert.ToString(dt.Rows[0]["MOQ"]);
                                        lblNewOfferPrice.Text = Convert.ToString(dt.Rows[0]["New_OfferPrice"]);
                                        lblOrderQuant.Text = Convert.ToString(dt.Rows[0]["Approved_OrderQty"]);
                                        lblCom_head.Text = "Last Upadated Comment: ";
                                        lblComment.Text = Convert.ToString(dt.Rows[0]["StatusChange_Comment"]);
                                    }
                                }

                            }
                        } 
                    }
                }
                catch (Exception ex)
                {
                    lblmsg.Text = "Error in loading. Please try again.";
                    objCom.ErrorLog(ex);
                }

            }

        }
        #endregion
    }
}