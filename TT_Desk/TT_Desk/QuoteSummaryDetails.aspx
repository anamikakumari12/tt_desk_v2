﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QuoteSummaryDetails.aspx.cs" Inherits="TT_Desk.QuoteSummaryDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

	<link href="css/style.css" rel="stylesheet" />
	<link href="vendor/bootstrap/css/bootstrap.css" rel="stylesheet" />
	<link href="css/jquery-ui.css" rel="stylesheet" />
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="js/jquery-ui.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<title>Quote Details</title>
	<style type="text/css">
		.title
		{
			margin: 5px 0;
			width: 100%;
			font-size: large;
			font-weight: bold;
			padding: 10px;
			display: block;
			text-align: center;
		}

		.subtitle
		{
			margin: 5px 0;
			width: 100%;
			font-size: large;
			font-weight: bold;
			display: block;
			text-align: center;
		}

		table
		{
			width: 100%;
		}

		.divhead
		{
			font-weight: bold;
			/*float: right;*/
		}

		.quete_details table td
		{
			border: solid 1px #ddd;
			padding: 2px;
		}

		.quote_d_middle
		{
			margin: 10px 0;
			background: #8cffc6;
			padding: 0px 0;
		}

		.quete_comment
		{
			width: 100%;
		}

			.quete_comment textarea
			{
				width: 100%;
				margin: 5px 0;
				height: 80px;
			}

		.quete_details input
		{
			width: 100%;
		}

		.quete_details select
		{
			width: 100%;
		}

		.cp_name
		{
			line-height: 24px;
		}
	</style>
	<script type="text/javascript">
	    $(document).ready(function () {
	        debugger;
	        
	    });

	</script>
</head>

<body>
	<form id="form1" runat="server">
		<div style="background: #3fab76;">
			<asp:Label ID="lblTitle" CssClass="title" Text="QUOTE DETAILS" runat="server"></asp:Label>
		</div>
		<div>
			<asp:HiddenField runat="server" ID="hdnparam" />
			<asp:HiddenField runat="server" ID="hdnID" />
			<asp:HiddenField runat="server" ID="hdnitem" />
		</div>
		<div class="cp_name">
			<div class="col-sm-12">
				<div class="row">
					<div class="col-sm-3 divhead">CP Name : </div>
					<div class="col-sm-9">
						<asp:Label ID="lblCP" runat="server"></asp:Label>
					</div>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="row">
					<div class="col-sm-3 divhead">CP Customer : </div>
					<div class="col-sm-9">
						<asp:Label ID="lblCustomer" runat="server"></asp:Label>
					</div>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="row">
					<div class="col-sm-3 divhead">RFQ Number :</div>
					<div class="col-sm-9">
						<asp:Label ID="lblref" runat="server"></asp:Label>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-12 quete_details">
			<table>
				<tr>
					<td width="25%" class="divhead">TTIL ITEM NUMBER : </td>
					<td width="25%" class="">
						<asp:Label ID="lblItem" runat="server"></asp:Label>
					</td>
                    <td class=" divhead">PREV.RFQ QTY : </td>
					<td class="">
						<asp:Label ID="lblTotalQty" runat="server"></asp:Label>
					</td>

				</tr>

				<tr>
					<td class=" divhead">STOCK CODE : </td>
					<td class="">
						<asp:Label ID="lblStockCode" runat="server"></asp:Label>
					</td>
					<td class=" divhead">LAST. SUPP.PRICE : </td>
					<td class="">
						<asp:Label ID="lblLSP" runat="server"></asp:Label>
					</td>
                   <%-- <td class=" divhead">ORDER PER QUANTITY : </td>
					<td class="">
						<asp:Label ID="lblReqMOQ" runat="server"></asp:Label>
					</td>--%>

				</tr>

				<tr>
					<td class=" divhead">WHS : </td>
					<td class="">
						<asp:Label ID="lblWHS" runat="server"></asp:Label>
					</td>
					 <td class=" divhead">DLP : </td>
					<td class="">
						<asp:Label ID="lblLP" runat="server"></asp:Label>
					</td>
					
				</tr>
				<tr>
					<td class=" divhead">STOCK : </td>
					<td class="">
						<asp:Label ID="lblStock" runat="server"></asp:Label>
					</td>
					<td class=" divhead">END CUSTOMER PRICE : </td>
					<td class="">
						<asp:Label ID="lblSP" runat="server"></asp:Label>
					</td>
					<%-- <td class=" divhead">EXP.PRICE  : </td>
					<td class="">
						<asp:Label ID="lblEP" runat="server"></asp:Label>
					</td>--%>
				</tr>
				<tr>
					<td class=" divhead">AVG.SELL PRICE : </td>
					<td class="">
						<asp:Label ID="lblaveSell" runat="server"></asp:Label>
					</td>
					<td class="divhead">UNIT COST : </td>
					<td class="">
						<asp:Label ID="lblUnitPrice" runat="server"></asp:Label>
					</td>
                  <%-- <td class=" divhead">EXP. DISC % : </td>
					<td class="">
						<asp:Label ID="lblDR" runat="server"></asp:Label>
					</td>--%>
				</tr>
				
                <tr>
                   <%-- <td class=" divhead">DLP : </td>
					<td class="">
						<asp:Label ID="lblLP" runat="server"></asp:Label>
					</td>--%>
                    <td class=" divhead"><asp:Label ID="lblCom_head" runat="server"></asp:Label></td>
					<td class="">
                        <asp:Label ID="lblComment" runat="server"></asp:Label>
					</td>
                </tr>
                
			</table>
		</div>
		<div class="col-sm-12 text-center quote_d_middle">
			<asp:Label ID="lblSubTitle" CssClass="subtitle" Text="TTIL OFFER" runat="server"></asp:Label>
		</div>
		<div class="col-sm-12 quete_details">
			<table>
				<tr>
					<%--<td width="25%" class=" divhead">MOQ : </td>
					<td width="25%" class="">
						<asp:Label ID="lblMOQ" runat="server"></asp:Label>
					</td>--%>
					<td class=" divhead">Break Quantity : </td>
					<td class="">
						<asp:Label ID="lblOrderQuant" runat="server"></asp:Label>
					</td>
					 <td class=" divhead">Order Validity : </td>
					<td class="">
						<asp:Label ID="lblOrdValidity" runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					
                   

                  <td width="25%" class="divhead">SYSTEM PRICE : </td>
					<td width="25%" class="">
						<asp:Label ID="lblOfferPrice" runat="server"></asp:Label>
					</td>
					<td class=" divhead">SPECIAL PRICE : </td>
					<td class="">
						<asp:Label ID="lblNewOfferPrice" runat="server"></asp:Label>
					</td>
				</tr>
				<tr>
					
                      <td class=" divhead">GP % : </td>
					<td class="">
						<asp:Label ID="lblGP" runat="server"></asp:Label>
					</td>					
				</tr>
			
			</table>
		</div>
		  <div>
            <asp:Label ID="lblmsg" ForeColor="Red" runat="server"></asp:Label>
        </div>
        
	</form>
</body>
</html>

