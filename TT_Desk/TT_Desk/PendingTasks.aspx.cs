﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TTDeskBO;
using TTDeskBL;
using System.Data;
using System.Web.Services;
using System.Text;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System.Configuration;
using System.Reflection;
using System.Globalization;

namespace TT_Desk
{
    public partial class PendingTasks : System.Web.UI.Page
    {
        #region GlobalDeclaration
        CommonFunctions objCom = new CommonFunctions();
        #endregion

        #region Events
        /// <summary>
        /// Author : Anamika Kumari
        /// Date : Nov 02, 2020
        /// Desc : To call a function for Loading pending quotes and accepted quote
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["LoginMailId"]))) { Response.Redirect("Login.aspx"); return; }

            if (!IsPostBack)
            {
                BindGrid();
            }
        }

        /// <summary>
        /// Author : Anamika Kumari
        /// Date : Nov 02, 2020
        /// Desc : to load pending quotes and accepted quote based on filters.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnFilter_Click(object sender, EventArgs e)
        {
            lblresult.Text = "";
            BindGrid();
        }

        /// <summary>
        /// Author : Monika
        /// Date : Jul 14, 2020
        /// Desc : On click of download button, the Accepted quotes will be downloaded.
        /// Modified By : Anamika
        /// Date : September 22, 2020
        /// Desc : Excel format is changed so created a function GetAcceptedQuotesForDownloadBL
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btndownload_Click(object sender, EventArgs e)
        {
            QuoteBO objQuoteBO = new QuoteBO();
            QuoteBL objQuoteBL = new QuoteBL();
            string start_date = string.Empty;
            string end_date = string.Empty;
            string selectedDate = string.Empty;
            try
            {

                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "ApprovedQuotes.xls"));
                Response.ContentType = "application/ms-excel";

                //selectedDate = txtDateRange.Text;
                //if (!string.IsNullOrEmpty(selectedDate))
                //{
                //    if (selectedDate.Contains("/"))
                //    {
                //        string[] splittedDates = selectedDate.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                //        start_date = Convert.ToString(splittedDates[0].TrimEnd().Trim());
                //        end_date = Convert.ToString(splittedDates[1].TrimStart().Trim());
                //    }
                //}
                //objQuoteBO.start_date = start_date;
                //objQuoteBO.end_date = end_date;
                //DataTable dt = objQuoteBL.GetAcceptedQuotesForDownloadBL(objQuoteBO);
                DataTable dt = (DataTable)Session["AcceptedQuotes"];
                DataTable copyDataTable;
                copyDataTable = dt.Copy();
                //copyDataTable.Columns.Remove("ID");
                copyDataTable.Columns.Remove("Ref_number");
                //copyDataTable.Columns.Remove("CP_number");
                //copyDataTable.Columns.Remove("CP_Name");
                copyDataTable.Columns.Remove("Expected_price");
                copyDataTable.Columns.Remove("New_OfferPrice");
                copyDataTable.Columns.Remove("Approved_OrderQty");
                copyDataTable.Columns.Remove("Item_Desc");
                copyDataTable.Columns.Add("Y - DELETE RECORD");
                DataRow[] drList = copyDataTable.Select("ID IN  (" + Convert.ToString(hdnQuoteIds.Value) + ")");

                string str = string.Empty;
                foreach (DataColumn dtcol in copyDataTable.Columns)
                {
                    if (!(Convert.ToString(dtcol.ColumnName) == "ID"))
                    {
                        string colName = string.Empty;

                        colName = dtcol.ColumnName.Replace("_", " ");
                        if (Convert.ToString(dtcol.ColumnName) == "Item_code")
                        {
                            colName = "KEY VALUE-CATALOG OR GRP";
                        }
                        Response.Write(str + colName);
                        str = "\t";
                    }
                }

                Response.Write("\n");
                foreach (DataRow dr in drList)
                {
                    str = "";
                    for (int j = 1; j < copyDataTable.Columns.Count; j++)
                    {
                        Response.Write(str + Convert.ToString(dr[j]));
                        str = "\t";
                    }
                    Response.Write("\n");
                }
                //  HttpContext.Current.Response.End();
                HttpContext.Current.Response.Flush(); // Sends all currently buffered output to the client.
                HttpContext.Current.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
                HttpContext.Current.ApplicationInstance.CompleteRequest(); // C

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                objQuoteBO = new QuoteBO();
                objQuoteBL = new QuoteBL();
            }
        }
        #endregion

        #region Methods
        /// <summary>
        ///  Author : Anamika Kumari
        ///  Date : Jan 6, 2020
        ///  Desc : This is webmethod which will be called by ajax call from front-end and this is to update the item status with details 
        /// </summary>
        /// <param name="objList">all the selected quote items which need to be updated</param>
        /// <returns></returns>
        [WebMethod]
        public static string UpdateQuoteStatus(List<QuoteStatus> objList)
        {
            string output = string.Empty;
            string status = string.Empty;
            CommonFunctions objCom = new CommonFunctions();
            List<QuoteStatusDB> quoteList = new List<QuoteStatusDB>();
            try
            {
                QuoteStatusDB objQuoteBO = new QuoteStatusDB();
                QuoteBL objQuoteBL = new QuoteBL();
                string UIpattern = "MM/dd/yyyy";
                string DBPattern = "MM-dd-yyyy";
                DateTime parsedDate;
                string Expirydate;
                foreach (QuoteStatus obj in objList)
                {
                    objQuoteBO = new QuoteStatusDB();
                    objQuoteBO.ID = Convert.ToInt32(obj.id);
                    objQuoteBO.Reason = Convert.ToString(obj.comment);
                    objQuoteBO.changeby = Convert.ToString(HttpContext.Current.Session["UserId"]);
                    objQuoteBO.MOQ = Convert.ToString(obj.MOQ);
                    objQuoteBO.OfferPrice = Convert.ToString(obj.offerPrice);
                    objQuoteBO.RefNumber = Convert.ToString(obj.ref_no);
                    objQuoteBO.item = Convert.ToString(obj.item);
                    objQuoteBO.MultiOrderFlag = Convert.ToInt32(obj.multiorder_flag);
                    objQuoteBO.Status = Convert.ToString(obj.status);
                    objQuoteBO.Order_Validity = Convert.ToString(obj.Order_Validity);
                    objQuoteBO.Approved_OrderQty = Convert.ToString(obj.Approved_OrderQty);
                    objQuoteBO.Approved_OrderFreq = Convert.ToString(obj.Approved_OrderFreq);
                    objQuoteBO.flag = "DESK";
                    status = obj.status;
                    //if (DateTime.TryParseExact(obj.expiry_date, UIpattern, null, DateTimeStyles.None, out parsedDate))
                    //{
                    //    Expirydate = parsedDate.ToString(DBPattern);
                    //    objQuoteBO.expiry_date=DateTime.ParseExact(Expirydate, "MM-dd-yyyy", null);
                    //}
                    quoteList.Add(objQuoteBO);
                }
                DataTable dtQuote = new DataTable();
                dtQuote = ToDataTable<QuoteStatusDB>(quoteList);
                List<QuoteBO> objQuoteList = new List<QuoteBO>();
                objQuoteList = objQuoteBL.updateQuoteStatusBL(dtQuote);
                //DataTable dt = new DataTable();
                //dt.Columns.Add("Message");
                //DataRow dr = dt.NewRow();
                //dr["Message"] = objOutputBO.Error_msg;
                //dt.Rows.Add(dr);
                if (objQuoteList.Count > 0)
                {
                    foreach (QuoteBO objOutputBO in objQuoteList)
                    {
                        if (status.Contains("Approved"))
                        {
                            if (!String.IsNullOrEmpty(Convert.ToString(objOutputBO.to)))
                            {
                                EmailDetails objEmail = new EmailDetails();
                                string attachment = string.Empty;
                                if (objOutputBO.pdf_flag == 1)
                                {
                                    attachment = GenerateQuoteFormat(objOutputBO.Ref_Number, objOutputBO.Quotation_no);
                                }
                                objEmail.toMailId = objOutputBO.to;
                                objEmail.ccMailId = objOutputBO.cc;
                                objEmail.subject = objOutputBO.subject;
                                objEmail.body = objOutputBO.message;
                                objEmail.attachment = attachment;
                                CommonFunctions.SendGridMail(objEmail).Wait();
                            }
                        }
                        else
                        //if (status.Contains("Added In PA"))
                        {
                            objCom.IntoFile(status);
                            objCom.IntoFile(objOutputBO.to);

                            if (!String.IsNullOrEmpty(Convert.ToString(objOutputBO.to)))
                            {
                                EmailDetails objEmail = new EmailDetails();
                                objEmail.toMailId = objOutputBO.to;
                                objEmail.ccMailId = objOutputBO.cc;
                                objEmail.subject = objOutputBO.subject;
                                objEmail.body = objOutputBO.message;
                                CommonFunctions.SendGridMail(objEmail).Wait();
                            }

                        }
                        output = "{\"code\":\"" + objOutputBO.Error_code + "\",\"msg\":\"" + objOutputBO.Error_msg + "\"}";
                    }
                }
            }
            catch (Exception ex)
            {
                output = "{\"code\":\"105\",\"msg\":\"" + ex.Message + "\"}";
                objCom.ErrorLog(ex);
            }
            finally
            {
                status = string.Empty;
                objCom = null;
                quoteList = null;
            }
            return output;
        }

        /// <summary>
        /// Author : Anamika Kumari
        ///  Date : Jan 6, 2020
        ///  Desc : GENERIC METHOD TO CONVERT CLASS object list to datatable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <returns></returns>
        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name, type);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        /// <summary>
        /// Author : Anamika Kumari
        /// Date : Nov 02, 2020
        /// Desc : To fetch pending quotes and accepted quote from database, store in session for later use and bind the gridviews
        /// </summary>
        private void BindGrid()
        {
            string start_date = string.Empty;
            string end_date = string.Empty;
            string selectedDate = string.Empty;
            try
            {
                selectedDate = txtDateRange.Text;
                if (!string.IsNullOrEmpty(selectedDate))
                {
                    if (selectedDate.Contains("/"))
                    {
                        string[] splittedDates = selectedDate.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                        start_date = Convert.ToString(splittedDates[0].TrimEnd().Trim());
                        end_date = Convert.ToString(splittedDates[1].TrimStart().Trim());
                    }
                }
                QuoteBO objQuoteBO = new QuoteBO();
                objQuoteBO.start_date = start_date;
                objQuoteBO.end_date = end_date;
                if (Convert.ToString(Session["LoginMailId"]).Contains("duracarb-india.com"))
                //|| Convert.ToString(Session["LoginMailId"]).Contains("knstek.com"))
                {
                    Session["cter"] = "DUR";
                    objQuoteBO.cter = "DUR";
                }
                else
                {
                    Session["cter"] = "TTA";
                    objQuoteBO.cter = "TTA";
                }
                QuoteBL objQuoteBL = new QuoteBL();
                DataTable dt = objQuoteBL.GetPendingTasksBL(objQuoteBO);
                Session["PendingQuotes"] = dt;
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        grdPendingQuote.DataSource = dt;
                        grdPendingQuote.DataBind();
                    }
                    else
                    {
                        lblresult.Text = "No pending quote is available.";
                        grdPendingQuote.DataSource = null;
                        grdPendingQuote.DataBind();
                    }
                }
                else
                {
                    lblresult.Text = "No pending quote is available.";
                    grdPendingQuote.DataSource = null;
                    grdPendingQuote.DataBind();
                }
                DataTable dtAccepted = objQuoteBL.GetAcceptedQuotesBL(objQuoteBO);
                Session["AcceptedQuotes"] = dtAccepted;
                if (dtAccepted != null)
                {
                    if (dtAccepted.Rows.Count > 0)
                    {
                        grdAcceptedQuote.DataSource = dtAccepted;
                        grdAcceptedQuote.DataBind();
                    }
                    else
                    {
                        lblresult.Text = "No accepted quote is available.";
                        btndownload.Visible = false;
                        grdAcceptedQuote.DataSource = null;
                        grdAcceptedQuote.DataBind();
                    }
                }
                else
                {
                    lblresult.Text = "No accepted quote is available.";
                    btndownload.Visible = false;
                    grdAcceptedQuote.DataSource = null;
                    grdAcceptedQuote.DataBind();
                }
                DataTable dtEscalated = objQuoteBL.GetEscalatedQuotesBL(objQuoteBO);
                Session["EscalatedQuotes"] = dtEscalated;
                if (dtEscalated != null)
                {
                    if (dtEscalated.Rows.Count > 0)
                    {
                        grdEscalatedQuote.DataSource = dtEscalated;
                        grdEscalatedQuote.DataBind();
                    }
                    else
                    {
                        lblresult.Text = "No escalated quote is available.";
                        grdEscalatedQuote.DataSource = null;
                        grdEscalatedQuote.DataBind();
                    }
                }
                else
                {
                    lblresult.Text = "No escalated quote is available.";
                    btndownload.Visible = false;
                    grdEscalatedQuote.DataSource = null;
                    grdEscalatedQuote.DataBind();
                }
            }
            catch (Exception ex)
            {
                lblresult.Text = "Error in loading. Please try again.";
                btndownload.Visible = false;
                objCom.ErrorLog(ex);
            }
            finally
            {
                start_date = string.Empty;
                end_date = string.Empty;
                selectedDate = string.Empty;
            }
        }


        #region reportGenerate

        private static string GenerateQuoteFormat(string ref_number, string quote_no)
        {
            string file = string.Empty;
            CommonFunctions objCom = new CommonFunctions();
            try
            {
                string html = string.Empty;
                string filename = string.Empty;
                string filepath = string.Empty;
                QuoteBL objQuoteBL = new QuoteBL();
                QuoteBO objQuoteBO = new QuoteBO();
                DataTable dt = new DataTable();

                html = "<!DOCTYPE html><html><head><style>table td{border:solid 1px #ddd;padding:5px;}</style></head><body>";
                objQuoteBO.Ref_Number = ref_number;
                objQuoteBO.Quotation_no = quote_no;
                dt = objQuoteBL.getQuoteFormatBL(objQuoteBO);

                html += Getheading(dt, quote_no);

                html += "</body></html>";
                filename = "Quote_" + Convert.ToString(quote_no.Replace('/', '_')) + ".pdf";
                filepath = ConfigurationManager.AppSettings["PDF_Folder"].ToString();
                convertPDF(html, filepath, filename);
                file = String.Concat(filepath, filename);
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return file;
        }

        private static void convertPDF(string html, string filepath, string filename)
        {
            CommonFunctions objCom = new CommonFunctions();
            try
            {
                //using (var stream = new MemoryStream())
                //{
                //    using (var document = new iTextSharp.text.Document())
                //    {
                //        PdfWriter writer = PdfWriter.GetInstance(document, stream);
                //        document.Open();
                //        using (var stringReader = new StringReader(html))
                //        {
                //            XMLWorkerHelper.GetInstance().ParseXHtml(
                //                writer, document, stringReader
                //            );
                //        }
                //        //HtmlConverter.ConvertToPdf(htmlStream, document);
                //    }
                //    File.WriteAllBytes(string.Concat(filepath, filename), stream.ToArray());
                //}
                using (FileStream fs = new FileStream(Path.Combine(filepath, "test.htm"), FileMode.Create))
                {
                    using (StreamWriter w = new StreamWriter(fs, Encoding.UTF8))
                    {
                        w.WriteLine(html);
                    }
                }


                GeneratePdfFromHtml(filepath, filename, html);

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private static void GeneratePdfFromHtml(string filepath, string filename, string html)
        {
            string outputFilename = Path.Combine(filepath, filename);
            string inputFilename = Path.Combine(filepath, "test.htm");

            using (var input = new FileStream(inputFilename, FileMode.Open))
            using (var output = new FileStream(outputFilename, FileMode.Create))
            {
                CreatePdf(filepath, filename, input, output, html);
            }
        }

        private static void CreatePdf(string filepath, string filename, FileStream htmlInput, FileStream pdfOutput, string html)
        {
            string imageURL;
            CommonFunctions objCom = new CommonFunctions();
            try
            {
                using (var document = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 30, 30, 30, 30))
                {
                    var writer = PdfWriter.GetInstance(document, pdfOutput);
                    var worker = XMLWorkerHelper.GetInstance();
                    TextReader tr = new StreamReader(htmlInput);
                    document.Open();
                    worker.ParseXHtml(writer, document, htmlInput, null, Encoding.UTF8);
                    //worker.ParseXHtml(writer, document, new StringReader(html));
                    document.Close();
                }

            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        private static string Getheading(DataTable dt, string quote_no)
        {
            CommonFunctions objCom = new CommonFunctions();
            string output = string.Empty;
            StringBuilder strHTMLBuilder = new StringBuilder();
            string imageURL = string.Empty;
            string Heading = string.Empty;
            string taegutec_add = string.Empty;
            string Customer_Name = string.Empty;
            string Customer_Address = string.Empty;
            string Customer_Number = string.Empty;
            string Quotation_No = string.Empty;
            string Date = string.Empty;
            string Due_On = string.Empty;
            string Tender_No = string.Empty;
            string Valid_For = string.Empty;
            string Remarks = string.Empty;
            try
            {
                Heading = Convert.ToString(ConfigurationManager.AppSettings["QuoteFormat_Heading"]);
                if (Convert.ToString(HttpContext.Current.Session["cter"]) == "DUR")
                {
                    imageURL = Convert.ToString(ConfigurationManager.AppSettings["LogoDUR"]);
                }
                else
                    imageURL = Convert.ToString(ConfigurationManager.AppSettings["Logo"]);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        Customer_Name = Convert.ToString(dt.Rows[0]["Customer_Name"]);

                        Customer_Address = Convert.ToString(dt.Rows[0]["Customer_Address"]);
                        Customer_Number = Convert.ToString(dt.Rows[0]["Customer_Number"]);
                        Quotation_No = Convert.ToString(dt.Rows[0]["Quotation_No"]);
                        Date = Convert.ToString(dt.Rows[0]["Date"]);
                        Due_On = Convert.ToString(dt.Rows[0]["Due_On"]);
                        Tender_No = Convert.ToString(dt.Rows[0]["Tender_No"]);
                        Valid_For = Convert.ToString(dt.Rows[0]["Valid_For"]);
                        taegutec_add = Convert.ToString(dt.Rows[0]["Taegutec_Address"]);
                        Remarks = Convert.ToString(dt.Rows[0]["Remarks"]);

                        // strHTMLBuilder.Append("<table style='border: 1px solid darkgray; font-family: Helvetica Neue, HelveticaNeue, Helvetica, Arial, sans-serif; border-collapse:collapse;'>");

                        strHTMLBuilder.Append("<table width='100%' style='border: 1px solid darkgray; font-family: Helvetica Neue, HelveticaNeue, Helvetica, Arial, sans-serif; border-collapse:collapse;'>");
                        strHTMLBuilder.Append("<tr style=' background-color: #5faae6c7;'>");
                        strHTMLBuilder.Append("<td colspan='9' style='text-align:center; font-size:30px; font-weight:bold; color:black;'>");
                        strHTMLBuilder.Append(Heading);
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("</tr>");
                        strHTMLBuilder.Append("<tr>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 10px;' colspan='7'><img style='float:left; width:100%;' src='");
                        strHTMLBuilder.Append(imageURL);
                        strHTMLBuilder.Append("'/>");
                        if (Convert.ToString(HttpContext.Current.Session["cter"]) == "DUR")
                        {
                            strHTMLBuilder.Append("<br/>(A Division of TaeguTec India P.Ltd.)");
                        }

                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("<td colspan='2' style='font-weight:bold; font-size: 10px;'>");
                        strHTMLBuilder.Append(taegutec_add);
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("</tr>");
                        strHTMLBuilder.Append("<tr>");
                        strHTMLBuilder.Append("<td colspan='7' style='font-weight:bold; font-size: 10px;'>");
                        strHTMLBuilder.Append(Customer_Name);
                        strHTMLBuilder.Append("<br/>");
                        strHTMLBuilder.Append(Customer_Address);
                        strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("<td colspan='2' style='font-weight:bold; font-size: 10px; padding:0px;'>");

                        //strHTMLBuilder.Append("</td>");
                        //strHTMLBuilder.Append("</tr>");
                        //strHTMLBuilder.Append("<tr >");
                        //strHTMLBuilder.Append("<td colspan='4' >");
                        //strHTMLBuilder.Append("</td>");
                        //strHTMLBuilder.Append("<td colspan='5'  style='font-weight:bold; font-size: 10px;'>");
                        //strHTMLBuilder.Append("REMARKS:");
                        //strHTMLBuilder.Append("</td>");
                        //strHTMLBuilder.Append("</tr>");
                        //strHTMLBuilder.Append("<tr >");
                        // strHTMLBuilder.Append("<td colspan='4' style='padding:0px'>");
                        strHTMLBuilder.Append("<table width='100%'>");
                        strHTMLBuilder.Append("<tr><td style='font-weight:bold; font-size: 10px;'>CUSTOMER NO : ");
                        strHTMLBuilder.Append(Customer_Number);
                        strHTMLBuilder.Append("</td></tr>");
                        strHTMLBuilder.Append("<tr><td style='font-weight:bold; font-size: 10px;'>QUOTATION NO : ");
                        strHTMLBuilder.Append(quote_no);
                        strHTMLBuilder.Append("</td></tr>");
                        strHTMLBuilder.Append("<tr><td style='font-weight:bold; font-size: 10px;'>RFQ NO : ");
                        strHTMLBuilder.Append(Quotation_No);
                        strHTMLBuilder.Append("</td></tr>");
                        strHTMLBuilder.Append("<tr><td style='font-weight:bold; font-size: 10px;'>DUE ON : ");
                        strHTMLBuilder.Append(Due_On);
                        strHTMLBuilder.Append("</td></tr>");
                        strHTMLBuilder.Append("<tr><td style='font-weight:bold; font-size: 10px;'>VALID FOR : ");
                        strHTMLBuilder.Append(Valid_For);
                        strHTMLBuilder.Append("</td></tr>");
                        strHTMLBuilder.Append("<tr><td style='font-weight:bold; font-size: 10px;'>DATE : ");
                        strHTMLBuilder.Append(Date);
                        strHTMLBuilder.Append("</td></tr>");
                        strHTMLBuilder.Append("</table>");
                        strHTMLBuilder.Append("</td>");
                        //strHTMLBuilder.Append("<td colspan='5'>");
                        //strHTMLBuilder.Append(Remarks);
                        //strHTMLBuilder.Append("</td>");
                        strHTMLBuilder.Append("</tr>");

                        strHTMLBuilder.Append("<tr>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 9px; '>SL<br/>NO</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 9px; '>CAT. NO.</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 9px; width: 100px;'>ITEM DESCRIPTION</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 9px; '>SC</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 9px; '>QTY</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 9px; '>UNIT<br/>RATE</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 9px; '>LINE<br/>VALUE</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 9px; '>DELIVERY<br/>DATE</td>");
                        strHTMLBuilder.Append("<td style='font-weight:bold; font-size: 9px;'>END<br/>CUSTOMER</td>");
                        strHTMLBuilder.Append("</tr>");
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            strHTMLBuilder.Append("<tr>");
                            strHTMLBuilder.Append("<td  style='font-size: 8px; text-align:center;'>");
                            strHTMLBuilder.Append(Convert.ToString(i + 1));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 8px;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Catalogue_No"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 8px;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Item"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 8px; text-align:center;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Stock_Code"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 8px;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Item_Qty"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 8px; text-align:right;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Unit_Price"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 8px; text-align:right;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Line_Value"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 8px;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["Delivery_Date"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("<td style='font-size: 8px;'>");
                            strHTMLBuilder.Append(Convert.ToString(dt.Rows[i]["End_Customer"]));
                            strHTMLBuilder.Append("</td>");
                            strHTMLBuilder.Append("</tr>");
                            Remarks = Convert.ToString(dt.Rows[i]["Remarks"]);
                            if (!string.IsNullOrEmpty(Remarks))
                            {
                                strHTMLBuilder.Append("<tr>");
                                strHTMLBuilder.Append("<td  style='font-size: 8px; text-align:center;'>");
                                strHTMLBuilder.Append(" ");
                                strHTMLBuilder.Append("</td>");
                                strHTMLBuilder.Append("<td style='font-size: 8px;' colspan='8'>");
                                strHTMLBuilder.Append(Convert.ToString(Remarks));
                                strHTMLBuilder.Append("</td>");
                                strHTMLBuilder.Append("</tr>");
                            }
                        }

                        strHTMLBuilder.Append("</table>");
                    }
                }

                output = strHTMLBuilder.ToString();
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            return output;
        }


        #endregion

        #endregion



        //protected void grdPendingQuote_RowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    QuoteBO objQuoteBO = new QuoteBO();
        //    QuoteBL objQuoteBL = new QuoteBL();
        //    try
        //    {
        //        GridViewRow row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
        //        //TextBox txtComment=(TextBox)row.FindControl("txtComment");
        //        //TextBox txtOfferPrice = (TextBox)row.FindControl("txtOfferPrice");
        //        //TextBox txtMOQ = (TextBox)row.FindControl("txtMOQ");
        //        HiddenField txtComment = (HiddenField)row.FindControl("hdnComment");
        //        HiddenField txtOfferPrice = (HiddenField)row.FindControl("hdnOfferPrice");
        //        HiddenField txtMOQ = (HiddenField)row.FindControl("hdnMOQ");
        //        Label lblref = (Label)row.FindControl("lblref");
        //        Label lblitem = (Label)row.FindControl("lblitem");
        //        CheckBox chkMulti = (CheckBox)row.FindControl("chkMultiFlag");
        //        string Comment = Convert.ToString(txtComment.Value);
        //        string OfferPrice = Convert.ToString(txtOfferPrice.Value);
        //        string MOQ = Convert.ToString(txtMOQ.Value);
        //        string refnum = Convert.ToString(lblref.Text);
        //        string item = Convert.ToString(lblitem.Text);

        //        //string Comment = ((System.Web.UI.WebControls.TextBox)row.FindControl("txtComment")).Text;
        //        //string OfferPrice = ((System.Web.UI.WebControls.TextBox)row.FindControl("txtOfferPrice")).Text;
        //        //string MOQ = ((System.Web.UI.WebControls.TextBox)row.FindControl("txtMOQ")).Text;
        //        //string refnum = ((System.Web.UI.WebControls.Label)row.FindControl("lblref")).Text;
        //        //string item = ((System.Web.UI.WebControls.Label)row.FindControl("lblitem")).Text;
        //        String reason = row.Cells[16].Text;
        //        objQuoteBO.ID = Convert.ToInt32(e.CommandArgument);
        //        objQuoteBO.Reason = Convert.ToString(Comment);
        //        objQuoteBO.ChangeBy = Convert.ToString(Session["UserId"]);
        //        objQuoteBO.MOQ = Convert.ToString(MOQ);
        //        objQuoteBO.Offer_Price = Convert.ToString(OfferPrice);
        //        objQuoteBO.Ref_Number = Convert.ToString(refnum);
        //        objQuoteBO.Item_Number = Convert.ToString(item);
        //        objQuoteBO.MultiOrderFlag = chkMulti.Checked ? 1 : 0;
        //        if (e.CommandName == "Approve")
        //        {
        //            objQuoteBO.Status = "Approved";
        //           // objQuoteBO = objQuoteBL.updateQuoteStatusBL(objQuoteBO);
        //            if (objQuoteBO.Error_code == 0)
        //            {
        //                if (!String.IsNullOrEmpty(Convert.ToString(objQuoteBO.to)))
        //                {
        //                    EmailDetails objEmail = new EmailDetails();
        //                    objEmail.toMailId = objQuoteBO.to;
        //                    objEmail.ccMailId = objQuoteBO.cc;
        //                    objEmail.subject = objQuoteBO.subject;
        //                    objEmail.body = objQuoteBO.message;
        //                    objCom.SendMail(objEmail);
        //                }
        //                BindGrid();
        //                ScriptManager.RegisterStartupScript(this, this.GetType(), "LoadTable", "alert('" + objQuoteBO .Error_msg+ "');LoadTable();", true);
        //            }
        //            else
        //            {
        //                ScriptManager.RegisterStartupScript(this, this.GetType(), "LoadTable", "alert('" + objQuoteBO.Error_msg + "');", true);
        //            }
        //        }
        //        else if (e.CommandName == "Reject")
        //        {


        //                objQuoteBO.Status = "Rejected";
        //               // objQuoteBO = objQuoteBL.updateQuoteStatusBL(objQuoteBO);
        //                if (objQuoteBO.Error_code == 200)
        //                {
        //                    if (!String.IsNullOrEmpty(Convert.ToString(objQuoteBO.to)))
        //                    {
        //                        EmailDetails objEmail = new EmailDetails();
        //                        objEmail.toMailId = objQuoteBO.to;
        //                        objEmail.ccMailId = objQuoteBO.cc;
        //                        objEmail.subject = objQuoteBO.subject;
        //                        objEmail.body = objQuoteBO.message;
        //                        objCom.SendMail(objEmail);
        //                    }
        //                    BindGrid();
        //                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LoadTable", "alert('" + objQuoteBO.Error_msg + "');LoadTable();", true);
        //                }
        //                else
        //                {
        //                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LoadTable", "alert('" + objQuoteBO.Error_msg + "');", true);
        //                }
        //            }
        //            else
        //            {
        //                return;
        //            }

        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //}



        //public static string UpdateQuoteStatus(string id, string item, string ref_no, string status, string comment, string MOQ, string offerPrice, string multiorder_flag)


    }
}