﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TTDeskBL;
using TTDeskBO;

namespace TT_Desk
{
    public partial class ResetPassword : System.Web.UI.Page
    {
        CommonFunctions objCom = new CommonFunctions();

        /// <summary>
        /// Author: Monika M S
        /// Date: 30th July 2020
        /// Desc: page load function
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(Convert.ToString(Session["LoginMailId"]))) { Response.Redirect("Login.aspx"); return; }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Monika M S
        /// Date: 29th July 2020
        /// Desc: On click of Change password button it will validate the given credential and Reset the password
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnReset_Click(object sender, EventArgs e)
        {
            ResetBO objResetBO = new ResetBO();
            LoginBL objLoginBL = new LoginBL();
            string scriptString = "";
            btnReset.Enabled = false;
            try
            {

                if (!String.IsNullOrEmpty(Convert.ToString(Session["LoginMailId"])))
                {
                    string strpassword = objCom.Encrypt(Convert.ToString(txtNew.Text));
                    objResetBO.OldPassword = objCom.Encrypt(Convert.ToString(txtOld.Text));
                    objResetBO.Newpassword = strpassword;
                    objResetBO.Engid = Convert.ToString(Session["UserId"]);
                    //Session["LoggedInPassword"] = Convert.ToString(txtNew.Text);
                    ResetBO ErrorMessage = objLoginBL.deskResetBL(objResetBO);
                    if (ErrorMessage.Error_code == 200)
                    {
                        scriptString = "<script type='text/javascript'> alert('Password was successfully reset');window.location='Login.aspx';</script>";

                    }
                    else if (ErrorMessage.Error_code == 0)
                    {
                        scriptString = "<script type='text/javascript'> alert('The old password is not matching please enter correct password');window.location='ResetPassword.aspx';</script>";
                        btnReset.Enabled = true;
                    }
                    else
                    {
                        scriptString = "<script type='text/javascript'> alert('Failed to reset');window.location='Login.aspx';</script>";
                        btnReset.Enabled = true;
                    }
                    ClientScriptManager script = Page.ClientScript;
                    script.RegisterClientScriptBlock(GetType(), "Script", scriptString);

                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }
            catch (Exception ex)
            {
                objCom.ErrorLog(ex);
            }
            finally
            {
                scriptString = null;
            }
        }

    }
}