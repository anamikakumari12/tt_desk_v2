﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TTDeskBL;
using TTDeskBO;

namespace TT_Desk
{
    public partial class EscalatedQuotes : System.Web.UI.Page
    {
        CommonFunctions objCom = new CommonFunctions();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["LoginMailId"]))) { Response.Redirect("Login.aspx"); return; }

            if (!IsPostBack)
            {
                BindGrid();
            }
        }

        private void BindGrid()
        {
            try
            {
                string start_date = string.Empty;
                string end_date = string.Empty;
                string selectedDate = txtDateRange.Text;
                if (!string.IsNullOrEmpty(selectedDate))
                {
                    if (selectedDate.Contains("/"))
                    {
                        string[] splittedDates = selectedDate.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                        start_date = Convert.ToString(splittedDates[0].TrimEnd().Trim());
                        end_date = Convert.ToString(splittedDates[1].TrimStart().Trim());
                    }
                }
                QuoteBO objQuoteBO = new QuoteBO();
                objQuoteBO.start_date = start_date;
                objQuoteBO.end_date = end_date;
                QuoteBL objQuoteBL = new QuoteBL();
                DataTable dt = objQuoteBL.GetEscalatedTasksBL(objQuoteBO);
                Session["EscalatedQuotes"] = dt;
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        grdEscalatedQuote.DataSource = dt;
                        grdEscalatedQuote.DataBind();
                    }
                    else
                    {
                        lblresult.Text = "No escalated quote is available.";
                        grdEscalatedQuote.DataSource = null;
                        grdEscalatedQuote.DataBind();
                    }
                }
                else
                {
                    lblresult.Text = "No escalated quote is available.";
                    grdEscalatedQuote.DataSource = null;
                    grdEscalatedQuote.DataBind();
                }
                
            }
            catch (Exception ex)
            {
                lblresult.Text = "Error in loading. Please try again.";
                objCom.ErrorLog(ex);
            }
        }
        protected void btnFilter_Click(object sender, EventArgs e)
        {
            BindGrid();
        }
    }
}