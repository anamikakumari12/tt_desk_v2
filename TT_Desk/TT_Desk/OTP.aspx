﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OTP.aspx.cs" Inherits="TT_Desk.OTP" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>OTP Desk</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.ico" />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <!--===============================================================================================-->
    <style>
        .login {
            background: #3e7739;
            background: -webkit-linear-gradient(right, #3e7739, #2c632c, #76b068, #2c632c);
            transition: all 0.4s;
        }
    </style>

</head>
<body>

    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <form class="login100-form validate-form" runat="server">
                    <span class="login100-form-title p-b-48">
                        <img src="images/icons/admin.png" style="width: 100px;" />
                        <%--<i class="zmdi zmdi-font"></i>--%>
                    </span>
                    <p>Please Enter Otp that you received on your mobile or Email.</p>
                    <%-- <div class="control-group">
                        <div class="controls">
                            <div class="input-icon left">
                                <asp:TextBox ID="txtotp" class="form-control" placeholder="Otp" name="otp" runat="server" Style="width: 73%; margin-left: 39px;"></asp:TextBox>
                            </div>
                        </div>
                    </div>--%>
                    <div class="wrap-input100 validate-input" data-validate="Valid email is: a@b.c">
                        <asp:TextBox ID="txtotp" runat="server" CssClass="input100"></asp:TextBox>
                        <%--<input class="input100" type="text" name="email">--%>
                        <span class="focus-input100" data-placeholder="Otp"></span>
                    </div>

                    <%--    <div class="form-actions" style="padding-top: 10px;margin-left: 99px;">--%>
                    <%--<asp:Button ID="btnotp" runat="server" Text="Submit" OnClick="BtnOtp_Click" Width="80px" Height="38px" Style="background: green; color: #fff;" OnClientClick=" return  ValidateOtp(); "/>--%>
                    <div class="container-login100-form-btn">
                        <div class="wrap-login100-form-btn">
                            <div class="login100-form-bgbtn"></div>
                            <%-- <button class="login100-form-btn" onclick="Login();">
                                Login
                            </button>--%>
                            <asp:Button ID="btnLogin" runat="server" CssClass="login100-form-btn login" Text="Submit" OnClick="BtnOtp_Click" OnClientClick=" return  ValidateOtp(); " />

                        </div>

                        <br />
                        <asp:Label ID="lblOtpFailure" runat="server" ForeColor="Red"></asp:Label>

                        <div class="form-group-1">
                            <asp:ValidationSummary ID="valSum" runat="server" ValidationGroup="VGLogin" ForeColor="Red" />
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="VGRequest" />
                            <asp:LinkButton ValidationGroup="VGRequest" Text="Resend OTP" ID="resendotp" runat="server" OnClick="ResendOtp_Click" OnClientClick="Clear();"></asp:LinkButton>
                        </div>
                        <%--<asp:LinkButton ID="resendotp" runat="server" OnClick="ResendOtp_Click" Text="Resend OTP" OnClientClick="Clear();" Style="margin-left: 99px;"></asp:LinkButton>--%>
                    </div>


                </form>
            </div>
    </div>
    </div>
	

	<div id="dropDownSelect1"></div>

    <!--===============================================================================================-->
    <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/animsition/js/animsition.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/bootstrap/js/popper.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/select2/select2.min.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/daterangepicker/moment.min.js"></script>
    <script src="vendor/daterangepicker/daterangepicker.js"></script>
    <!--===============================================================================================-->
    <script src="vendor/countdowntime/countdowntime.js"></script>
    <!--===============================================================================================-->
    <script src="js/main.js"></script>


    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/respond.min.js"></script>
    <script type="text/javascript" src="js/app.js"></script>
    <%--<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.js"></script>--%>

    <script type="text/javascript">
        $(document).ready(function () {

        });
        function ValidateOtp() {
            debugger;
            //SpecialCharchk();
            if (document.getElementById('txtotp').value == "") {
                alert("Please provide your OTP");
                return false;
            }
            else if (isNaN(document.getElementById('txtotp').value)) {
                alert("Please provide valid OTP");
                return false;
            }
            else
                return true;
        }

        function Clear() {
            debugger;
            document.getElementById('txtotp').value = '';
        }

        //function SpecialCharchk() {
        //    debugger;
        //    var str = "!@#$%^&*()+=-[]\\\';,./{}|\":<>?";
        //    if ("!@#$%^&*()+=-[]\\\';,./{}|\":<>?".test(str) == false) {
        //             alert("Your Otp has special characters. \nThese are not allowed.\n Please remove them and try again.");
        //    }
        //}



    </script>



</body>
</html>
