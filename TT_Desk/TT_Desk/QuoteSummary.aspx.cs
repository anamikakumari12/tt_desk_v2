﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using TTDeskBL;
using TTDeskBO;

namespace TT_Desk
{
    public partial class QuoteSummary : System.Web.UI.Page
    {
        #region GlobalDeclaration
        CommonFunctions objCom = new CommonFunctions();

        #endregion

        #region Evenets
        /// <summary>
        /// Author : Anamika Kumari
        /// Date : Dec 2, 2019
        /// Desc : 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["LoginMailId"]))) { Response.Redirect("Login.aspx"); return; }

            if (!IsPostBack)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "LoadDateRange();", true);
                BindGrid();
            }
        }

        /// <summary>
        /// Author : Anamika Kumari
        /// Date : Dec 2, 2019
        /// Desc : It will call the Bind grid function to load based on date range
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnFilter_Click(object sender, EventArgs e)
        {
            BindGrid();
        }

        #endregion

        #region Methods
        /// <summary>
        /// Author : Anamika Kumari
        /// Date : Dec 2, 2019
        /// Desc : It will fetch the quote details from backend and bind the gridview 
        /// </summary>
        private void BindGrid()
        {
            try
            {
                string start_date = string.Empty;
                string end_date = string.Empty;
                string selectedDate = txtDateRange.Text;
                if (!string.IsNullOrEmpty(selectedDate))
                {
                    if (selectedDate.Contains("/"))
                    {
                        string[] splittedDates = selectedDate.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                        start_date = Convert.ToString(splittedDates[0].TrimEnd().Trim());
                        end_date = Convert.ToString(splittedDates[1].TrimStart().Trim());
                    }
                }
                else
                {
                    end_date =  DateTime.Now.ToString("MM/dd/yyyy").Replace("-","/");
                     start_date = DateTime.Now.AddDays(-7).ToString("MM/dd/yyyy").Replace("-", "/");
                }
                QuoteBO objQuoteBO = new QuoteBO();
                objQuoteBO.start_date = start_date;
                objQuoteBO.end_date = end_date;
                objQuoteBO.flag = Convert.ToString(ddlcustType.SelectedValue) == "ALL" ? null : Convert.ToString(ddlcustType.SelectedValue);
                objQuoteBO.Status = Convert.ToString(ddlStatus.SelectedValue) == "ALL" ? null : Convert.ToString(ddlStatus.SelectedValue);
                QuoteBL objQuoteBL = new QuoteBL();
                DataTable dt = objQuoteBL.GetQuoteSummaryBL(objQuoteBO);
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        grdQuoteSummary.DataSource = dt;
                        grdQuoteSummary.DataBind();
                    }
                    else
                    {
                        lblresult.Text = "No quote is available.";
                        grdQuoteSummary.DataSource = null;
                        grdQuoteSummary.DataBind();
                    }
                }
                else
                {
                    lblresult.Text = "No quote is available.";
                    grdQuoteSummary.DataSource = null;
                    grdQuoteSummary.DataBind();
                }
                DataTable dtQuote = new DataTable();
                dtQuote = objQuoteBL.GetQuoteDetailsBL(objQuoteBO);
                Session["dtQuoteDetails"] = dtQuote;
                if (dtQuote != null)
                {
                    if (dtQuote.Rows.Count > 0)
                    {
                        grdItemSummary.DataSource = dtQuote;
                        grdItemSummary.DataBind();
                    }
                    else
                    {
                        lblresult.Text = "No quote is available.";
                        grdItemSummary.DataSource = null;
                        grdItemSummary.DataBind();
                    }
                }
                else
                {
                    lblresult.Text = "No quote is available.";
                    grdItemSummary.DataSource = null;
                    grdItemSummary.DataBind();
                }
            }
            catch (Exception ex)
            {
                // lblresult.Text = "Error in loading. Please try again.";
                lblresult.Text = ex.Message;
                objCom.ErrorLog(ex);
            }
        }

        /// <summary>
        /// Author : Anamika Kumari
        /// Date : Dec 2, 2019
        /// Desc : Webmethod is called in ajax call to fetch detailed item for RFQ expand
        /// </summary>
        /// <param name="ref_no"></param>
        /// <returns></returns>
        [WebMethod]
        public static string LoadDetailedGrid(string ref_no)
        {
            string output = string.Empty;
            try
            {
                DataTable dt = (DataTable)HttpContext.Current.Session["dtQuoteDetails"];
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        var rows = from row in dt.AsEnumerable()
                                   where row.Field<string>("Ref_number").Trim() == ref_no
                                   select row;
                        dt = rows.CopyToDataTable();
                        if (dt.Rows.Count > 0)
                        {
                            output = DataTableToJSONWithStringBuilder(dt);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                CommonFunctions.StaticErrorLog(ex);
            }
            return output;
        }

        [WebMethod]
        public static string DownloadFile(string file)
        {
            string output = string.Empty;
            try
            {
                string filePath = ConfigurationManager.AppSettings["Escalation_Folder"].ToString() + Convert.ToString(file);

                //if (File.Exists(filePath))
                //{
                byte[] bytes = File.ReadAllBytes(filePath);

                //Convert File to Base64 string and send to Client.
                return Convert.ToBase64String(bytes, 0, bytes.Length);
                //Response.ContentType = "application/octet-stream";
                //byte[] bts = System.IO.File.ReadAllBytes(filePath);
                //MemoryStream ms = new MemoryStream(bts);
                //HttpContext.Current.Response.Clear();
                //HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=\"" + Path.GetFileName(filePath) + "\"");
                //HttpContext.Current.Response.TransmitFile(filePath);
                //HttpContext.Current.Response.End();
                //}
            }
            catch (Exception ex)
            {
                CommonFunctions.StaticErrorLog(ex);
                return ex.Message;
            }

        }
        /// <summary>
        /// Author : Anamika Kumari
        /// Date : Dec 2, 2019
        /// Desc : Converts datatable to json string
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public static string DataTableToJSONWithStringBuilder(DataTable table)
        {
            var JSONString = new StringBuilder();
            if (table.Rows.Count > 0)
            {
                JSONString.Append("[");
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    JSONString.Append("{");
                    for (int j = 0; j < table.Columns.Count; j++)
                    {
                        //if (Convert.ToString(table.Columns[j].ColumnName) == "SALES_MTD_VALUE")
                        //{
                        //    if (j < table.Columns.Count - 1)
                        //    {
                        //        JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + table.Rows[i][j].ToString() + ",");
                        //    }
                        //    else if (j == table.Columns.Count - 1)
                        //    {
                        //        JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":"  + table.Rows[i][j].ToString() );
                        //    }
                        //}
                        //else
                        //{ 
                        if (j < table.Columns.Count - 1)
                        {
                            JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString().Replace('"', ' ') + "\",");
                        }
                        else if (j == table.Columns.Count - 1)
                        {
                            JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString().Replace('"', ' ') + "\"");
                        }
                        //}
                    }
                    if (i == table.Rows.Count - 1)
                    {
                        JSONString.Append("}");
                    }
                    else
                    {
                        JSONString.Append("},");
                    }
                }
                JSONString.Append("]");
            }
            return JSONString.ToString();
        }

        #endregion

        //[WebMethod]
        //public static string UpdateQuoteStatus(string id, string item, string ref_no, string status, string comment, string MOQ, string offerPrice, string multiorder_flag)
        //{
        //    string output = string.Empty;
        //    CommonFunctions objCom = new CommonFunctions();
        //    try
        //    {
        //        QuoteBO objQuoteBO = new QuoteBO();
        //        QuoteBL objQuoteBL = new QuoteBL();
        //        objQuoteBO.ID = Convert.ToInt32(id);
        //        objQuoteBO.MOQ = Convert.ToString(MOQ);
        //        objQuoteBO.Offer_Price = Convert.ToString(offerPrice);
        //        objQuoteBO.Ref_Number = Convert.ToString(ref_no);
        //        objQuoteBO.Item_Number = Convert.ToString(item);
        //        objQuoteBO.MultiOrderFlag = Convert.ToInt32(multiorder_flag);
        //        objQuoteBO.Reason = comment;
        //        objQuoteBO.Status = status;
        //        objQuoteBO.ChangeBy = Convert.ToString(HttpContext.Current.Session["UserId"]);
        //        //objQuoteBO = objQuoteBL.updateQuoteStatusBL(objQuoteBO);
        //        DataTable dt = new DataTable();
        //        dt.Columns.Add("Message");
        //        DataRow dr=dt.NewRow();
        //        dr["Message"]=objQuoteBO.Error_msg;
        //        dt.Rows.Add(dr);
        //        if (!String.IsNullOrEmpty(Convert.ToString(objQuoteBO.to)))
        //        {
        //            EmailDetails objEmail = new EmailDetails();
        //            objEmail.toMailId = objQuoteBO.to;
        //            objEmail.ccMailId = objQuoteBO.cc;
        //            objEmail.subject = objQuoteBO.subject;
        //            objEmail.body = objQuoteBO.message;
        //            objCom.SendMail(objEmail);
        //        }
        //        output = DataTableToJSONWithStringBuilder(dt);
        //    }
        //    catch (Exception ex)
        //    {
        //        objCom.ErrorLog(ex);
        //    }
        //    return output;
        //}

    }
}