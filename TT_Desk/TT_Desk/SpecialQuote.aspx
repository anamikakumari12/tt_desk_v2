﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SpecialQuote.aspx.cs" Inherits="TT_Desk.SpecialQuote" EnableEventValidation="false" MasterPageFile="~/SiteMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--    <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>--%>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" />
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
    <style>
        tr {
            height: 40px;
        }

        td {
            padding-left: 10px;
        }

            td .control_dropdown {
                width: 100%;
            }

        .modal a.close-modal {
            width: 20px;
            height: 20px;
            top: 0;
            right: 0;
        }

        .mn_section select {
            padding: 1px;
        }

        .mn_filter {
            z-index: 1;
        }

        table.dataTable.no-footer {
            width: 100% !important;
        }

        .dataTables_wrapper {
            /*    margin-top: 2%;*/
            margin-left: 2%;
            margin-right: 2%;
        }

        .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox], .radio input[type=radio], .radio-inline input[type=radio] {
            margin-left: 0px;
            width: 10%;
            float: left;
        }

        input[type=checkbox], input[type=radio] {
            width: 15% !important;
            float: left;
        }

        .control_dropdown {
            width: 180px;
            height: 30px;
            border-radius: 4px !important;
        }

        .label {
            padding-top: 9px;
            width: 100%;
            color: #3c763d;
            font-size: 100%;
            font-weight: bold;
        }

        .control {
            padding-top: 10px;
        }

        .loader_div {
            position: absolute;
            top: 0;
            bottom: 0%;
            left: 0;
            right: 0%;
            z-index: 99;
            opacity: 0.7;
            display: none;
            background: lightgrey url('../../../../images/loader.gif') center center no-repeat;
        }
    </style>
    <script>
        $(document).ready(function () {
            $('.file').change(function () {
                alert("A file has been selected.");
            });

        });
        function fileupload(obj) {
            debugger;
            var hdnfileId = obj.id.replace('file', 'hdnFile');
            var fileUpload = $("#" + obj.id).get(0);
            if (fileUpload != undefined) {
                var files = fileUpload.files;
                if (files.length > 0) {
                    var fileData = new FormData();

                    // Looping over all files and add it to FormData object  
                    for (var i = 0; i < files.length; i++) {
                        fileData.append(files[i].name, files[i]);
                    }
                    var RefID = $('#body_hdnID').val();
                    $.ajax({
                        url: 'FileUpload.ashx?RefID=' + RefID,
                        type: 'POST',
                        data: fileData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (file) {
                            $("#" + hdnfileId).val(file.name);
                        },
                        error: function (err) {
                            console.log(err.statusText);
                            alert("File should not be greater than 5MB.");
                        }
                    });
                }
                else {
                    $("#" + hdnfileId).val("");
                }
            }
            else {
                $("#" + hdnfileId).val("");
            }
        }
        function validateFields() {
            var errorflag = 0;
            if (!isNullorEmpty($('#body_ddlFamily').val())) {
                $('#body_ddlFamily').css("border", 'solid 1px red');
                errorflag++;
            }
            else {
                $('#body_ddlFamily').css("border", 'solid 1px #ccc');
            }
            if (!isNullorEmpty($('#body_ddlCustomerType').val())) {
                $('#body_ddlCustomerType').css("border", 'solid 1px red');
                errorflag++;
            }
            else {
                $('#body_ddlCustomerType').css("border", 'solid 1px #ccc');
            }
            if (!isNullorEmpty($('#body_ddlCustomers').val())) {
                $('#body_ddlCustomers').css("border", 'solid 1px red');
                errorflag++;
            }
            else {
                $('#body_ddlCustomers').css("border", 'solid 1px #ccc');
            }

            errorflag += checkForCat($('#body_ddlCPCustomers'));
            var obj = $('#body_ddlCat1');
            errorflag += checkForCat(obj);
            obj = $('#body_ddlCat2');
            errorflag += checkForCat(obj);
            errorflag += checkForCat($('#body_ddlCat3'));
            errorflag += checkForCat($('#body_ddlCat4'));
            errorflag += checkForCat($('#body_ddlCat5'));
            errorflag += checkForCat($('#body_ddlCat6'));
            errorflag += checkForCat($('#body_ddlCat7'));
            errorflag += checkForCat($('#body_ddlCat8'));
            if (errorflag > 0) {
                $('#divItem').attr("style", "display:none;")

            }
            else {
                $('#divItem').attr("style", "display:block;")
            }
            return errorflag;
        }

        function LoadDropdowns() {
            debugger;
            jQuery(".loader_div").show();
            console.log("LoadItem start : " + Date.now());
            $.ajax({
                url: 'ProjectQuotation.aspx/LoadItems',
                method: 'post',
                datatype: 'json',
                data: '',
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    sessionStorage.setItem("itemdata", JSON.stringify(data.d));
                    BindItems(data.d);
                    jQuery(".loader_div").hide();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.responseText);
                    jQuery(".loader_div").hide();
                }
            });
        }
        function BindItems(msg) {
            if (!$("#body_ddlitem").data('select2')) {
                $("#body_ddlitem").select2({
                    "width": "200px", "minimumInputLength": 3, data: msg, placeholder: "Select an item"
                });
                //var str = $("#body_ddlitem").val();
                //if (isNullorEmpty(str)) {
                //    $("#body_ddlitem").val(str);
                //    $("#body_ddlitem").select2({ text: str, "minimumInputLength": 3, });
                //}

            }
        }

        function validateFilterFields() {
            var errorflag = 0;
            errorflag = validateFields();
            if (errorflag > 0) {
                return false;
            }
            else
                return true;
        }
        function validateAllFields() {
            debugger;
            var Manual_Flag = $('#body_hdnManualFlag').val();
            var errorflag = 0;
            errorflag = validateFields();
            errorflag += checkForCat($('#body_txtItem'));
            errorflag += checkForCat($('#body_ddlDW'));
            errorflag += checkForCat($('#body_txtPrice'));
            if (Manual_Flag>0)
                errorflag += checkForCat($('#body_txtMOQ'));
            if (errorflag > 0) {
                return false;
            }
            else
                return true;
        }
        function checkForCat(obj) {
            if (obj.length > 0)
                if (obj.is(':visible'))
                    if (!isNullorEmpty(jQuery.trim(obj.val()))) {
                        obj.css("border", 'solid 1px red');
                        return 1;
                    }
                    else {
                        obj.css("border", 'solid 1px #ccc');
                        return 0;
                    }
                else
                    return 0;
            else
                return 0;
        }
        function isNullorEmpty(val) {
            if (val == undefined || val == '' || val == null || val == '-- Select Type --' || val == '-- Select --' || val == '0')
                return false;
            else
                return true;
        }

        function Save() {
            debugger;
            jQuery(".loader_div").show();
            var flag = true;

            if ($('#body_hdnCatID').val() > 0) {
                flag = AddRow();
            }
            if (!isNullorEmpty($('#body_ddlOrdVal').val())) {
                $('#body_ddlOrdVal').css("border", 'solid 1px red');
                flag = false;
            }
            else {
                $('#body_ddlOrdVal').css("border", 'solid 1px #ccc');
            }
            if (!isNullorEmpty($('#body_ddlPT').val()) || $('#body_ddlPT').val() =="--Select--" ) {
                $('#body_ddlPT').css("border", 'solid 1px red');
                flag = false;
            }
            else {
                $('#body_ddlPT').css("border", 'solid 1px #ccc');
            }
            //if (!isNullorEmpty($('#body_txtGST').val())) {
            //    $('#body_txtGST').css("border", 'solid 1px red');
            //    flag = false;
            //}
            //else {
            //    $('#body_txtGST').css("border", 'solid 1px #ccc');
            //}
            if (flag) {

                if ($('#grdSplItemDetails').length > 0) {
                    var param;
                    var paramList = [];
                    var data = [];

                    var RefID = $('#body_hdnID').val();
                    var userid = '<%=HttpContext.Current.Session["UserId"]%>';
                    data = tableToJson($('#grdSplItemDetails')[0]);

                    var fileUpload = $("#body_fileUpload").get(0);
                    var files = fileUpload.files;
                    if (files.length > 0) {
                        var fileData = new FormData();

                        // Looping over all files and add it to FormData object  
                        for (var i = 0; i < files.length; i++) {
                            fileData.append(files[i].name, files[i]);
                        }
                        $.ajax({
                            url: 'FileUpload.ashx?RefID=' + RefID,
                            type: 'POST',
                            data: fileData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function (file) {

                                for (var i = 0; i < data.length; i++) {

                                    param = {
                                        ID: RefID,
                                        Category_ID: data[i].CatID,
                                        Cust_Type: data[i].customertype,
                                        Customer_number: data[i].Cust_num,
                                        CPCustomer_number: data[i].CPCust_num,
                                        Item_Type: data[i].itemtype,
                                        Item_Desc: data[i].item_desc,
                                        Ref_number: '',
                                        Item_Code: data[i].Item_Code,
                                        Offer_Price: data[i].offerprice,
                                        Order_validity: $('#body_ddlOrdVal').val(),
                                        Item_File: data[i].fileattached,
                                        Common_File: file.name,
                                        RequestedBy: userid,
                                        RequestedBy_Flag: 'DESK',
                                        Requested_Date: '',
                                        Quotation_number: '',
                                        Qty: data[i].Qty,
                                        Remarks: $('#body_txtRemarks').val(),
                                        Delivery_Weeks: data[i].deliveryweek,
                                        Payment_Terms: $('#body_ddlPT').val(),
                                        CPCustomer_Name: data[i].cpcustomer,
                                        RFQ_Ref_No: $('#body_txtRFQ').val(),
                                        GST: '0',
                                        family: data[i].family,
                                        Item_Remarks: data[i].remarks

                                    }
                                    paramList.push(param);
                                }
                                callCreateRFQ(paramList);
                            },
                            error: function (err) {
                                // alert(err.statusText);
                                alert("Please upload file less than 5MB.");
                                jQuery(".loader_div").hide();
                            }
                        });
                    }
                    else {
                        for (var i = 0; i < data.length; i++) {

                            param = {
                                ID: RefID,
                                Category_ID: data[i].CatID,
                                Cust_Type: data[i].customertype,
                                Customer_number: data[i].Cust_num,
                                CPCustomer_number: data[i].CPCust_num,
                                Item_Type: data[i].itemtype,
                                Item_Desc: data[i].item_desc,
                                Ref_number: '',
                                Item_Code: data[i].Item_Code,
                                Offer_Price: data[i].offerprice,
                                Order_validity: $('#body_ddlOrdVal').val(),
                                Item_File: data[i].fileattached,
                                Common_File: '',
                                RequestedBy: userid,
                                RequestedBy_Flag: 'DESK',
                                Requested_Date: '',
                                Quotation_number: '',
                                Qty: data[i].Qty,
                                Remarks: $('#body_txtRemarks').val(),
                                Delivery_Weeks: data[i].deliveryweek,
                                Payment_Terms: $('#body_ddlPT').val(),
                                CPCustomer_Name: data[i].cpcustomer,
                                RFQ_Ref_No: $('#body_txtRFQ').val(),
                                GST: '0',
                                family: data[i].family,
                                Item_Remarks: data[i].remarks

                            }
                            paramList.push(param);
                        }
                        callCreateRFQ(paramList);
                    }
                }
                else {
                    alert("No data to store");
                    jQuery(".loader_div").hide();
                }
            }


            else {
                alert("Please check error message.");
                jQuery(".loader_div").hide();
            }
        }

        function callCreateRFQ(paramList) {
            var dataparam = "{obj:" + JSON.stringify(paramList) + "}";
            $.ajax({
                url: 'SpecialQuote.aspx/CreateRFQ',
                method: 'post',
                datatype: 'json',
                data: dataparam,
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    alert(data.d.ErrorMsg);
                    location.reload(true);
                    jQuery(".loader_div").hide();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.responseText);
                    jQuery(".loader_div").hide();
                }
            });
        }
        function tableToJson(table) {
           
            var data = [];

            // first row needs to be headers
            var headers = [];
            for (var i = 0; i < table.rows[0].cells.length; i++) {
                headers[i] = table.rows[0].cells[i].innerText.toLowerCase().replace(/ /gi, '');
            }
            headers[table.rows[0].cells.length] = "CatID";
     /*       headers[table.rows[0].cells.length + 1] = "OfferPrice";*/
            headers[table.rows[0].cells.length + 1] = "Qty";
            headers[table.rows[0].cells.length + 2] = "Cust_num";
            headers[table.rows[0].cells.length + 3] = "CPCust_num";
            headers[table.rows[0].cells.length + 4] = "Item_Code";
            // go through cells
            for (var i = 1; i < table.rows.length; i++) {

                var tableRow = table.rows[i];
                var rowData = {};

                for (var j = 0; j < tableRow.cells.length - 2; j++) {

                    rowData[headers[j]] = tableRow.cells[j].innerHTML == undefined ? "" : tableRow.cells[j].innerHTML;

                }
                rowData[headers[table.rows[i].cells.length - 2]] = $("#hdnFile" + i).val();
                rowData[headers[table.rows[i].cells.length]] = $('#hdnCatID' + i).val();
                /*rowData[headers[table.rows[i].cells.length + 1]] = $('#hdnOfferPrice' + i).val();*/
                rowData[headers[table.rows[i].cells.length + 1]] = $('#hdnQty' + i).val();
                rowData[headers[table.rows[i].cells.length + 2]] = $('#hdnCust' + i).val();
                rowData[headers[table.rows[i].cells.length + 3]] = $('#hdnCPCust' + i).val();
                rowData[headers[table.rows[i].cells.length + 4]] = $('#hdnItemCode' + i).val();
                data.push(rowData);
            }

            return data;
        }
        function AddRow() {
            debugger;
            if ($('#body_ddlFamily').val().includes("Manual")) {
                $('#body_hdnQty').val($('#body_txtMOQ').val());
            }
            var flag = validateAllFields();
            if (flag) {
                var itemtype, itemdesc, itemcode;
                if ($('#body_RadioButton2').is(':checked')) {
                    itemtype = 'Catalogue';
                    itemdesc = $("#body_ddlitem option:selected").text().replace(/\s+/g, " ");
                    itemcode = $("#body_ddlitem option:selected").val();
                }
                else {
                    itemtype = 'Special';
                    itemdesc = $('#body_txtItem').val();
                    itemcode = '';
                }
                var param;
                var paramList = [];
                param = {
                    Cust_type: $('#body_ddlCustomerType').val(),
                    Cust_number: $('#body_ddlCustomers').val(),
                    CPCust_number: $('#body_ddlCPCustomers').val(),
                    Cat1: $('#body_ddlCat1').val() == undefined ? '' : $('#body_ddlCat1').val(),
                    Cat2: $('#body_ddlCat2').val() == undefined ? '' : $('#body_ddlCat2').val(),
                    Cat3: $('#body_ddlCat3').val() == undefined ? '' : $('#body_ddlCat3').val(),
                    Cat4: $('#body_ddlCat4').val() == undefined ? '' : $('#body_ddlCat4').val(),
                    Cat5: $('#body_ddlCat5').val() == undefined ? '' : $('#body_ddlCat5').val(),
                    Cat6: $('#body_ddlCat6').val() == undefined ? '' : $('#body_ddlCat6').val(),
                    Cat7: $('#body_ddlCat7').val() == undefined ? '' : $('#body_ddlCat7').val(),
                    Cat8: $('#body_ddlCat8').val() == undefined ? '' : $('#body_ddlCat8').val(),
                    Item_type: itemtype,
                    Item_Code: itemcode,
                    Item_name: itemdesc,
                    ref_item: $('#body_txtRFQ').val(),
                    CatID: $('#body_hdnCatID').val(),
                    OfferPrice: $('#body_txtPrice').val(),
                    Qty: $('#body_hdnQty').val(),
                    DW: $('#body_ddlDW').val(),
                    Cust_name: $('#body_ddlCustomers option:selected').text(),
                    CPCustomer_Name: $('#body_ddlCPCustomers option:selected').text(),
                    Family: $('#body_ddlFamily').val(),
                    Item_remarks: $('#body_txtItemRemarks').val(),
                    
                }
                paramList.push(param);
                //}
                var dataParam = {
                    obj: paramList
                }
                LoadData(dataParam);
                ClearData();
            }
            else {
                alert("Please check error message.");
            }

            return flag;
        }
        function ClearData() {
            $('#body_ddlCat1').val("-- Select --");
            $('#body_ddlCat2').val("-- Select --");
            $('#body_ddlCat3').val("-- Select --");
            $('#body_ddlCat4').val("-- Select --");
            $('#body_ddlCat5').val("-- Select --");
            $('#body_ddlCat6').val("-- Select --");
            $('#body_ddlCat7').val("-- Select --");
            $('#body_ddlCat8').val("-- Select --");
            $('#body_txtItem').val("");
            $('#body_ddlItem').val("");
            $('#body_hdnCatID').val("");
            $('#body_txtPrice').val("");
            $('#body_ddlFamily').val("-- Select --");
            $('#body_txtItemRemarks').val("");
  /*          $('#body_hdnOfferPrice').val("");*/
        }
        function LoadData(msg) {
            debugger;
            console.log(msg);
            console.log(msg.obj);
            msg = msg.obj;
            var counter;
            for (var i = 0; i < msg.length; i++) {
                counter = $('#grdSplItemDetails')[0].children[1].children.length + i + 1;

                var div = document.createElement('DIV');

                div.innerHTML = '<input id="file' + counter + '" name = "file' + counter +

                    '" type="file" />' +

                    '<input id="Button' + counter + '" type="button" ' +

                    'value="Remove" onclick = "RemoveFileUpload(this)" />';

                $('#divTable').attr("style", "display:block;");
                console.log(msg[i].Item_type);
                console.log(msg[i].Item_name);
                console.log(msg[i].Cust_type);
                console.log(msg[i].Cust_number);
                console.log(msg[i].CPCust_number);
                $("#grdSplItemDetails tbody ").append(" <tr>  <td>" +

                    msg[i].Family + "</td>  <td>" +
                    msg[i].Item_type + "</td>  <td>" +
                    msg[i].Item_name + "</td>  <td>" +
                    msg[i].Cust_type + "</td>  <td>" +
                    msg[i].Cust_name + "</td>  <td>" +
                    msg[i].CPCustomer_Name + "</td>  <td>" +
                    msg[i].DW + "</td>  <td>" +
                    msg[i].Qty + "</td>  <td>" +
                    msg[i].OfferPrice + "</td>  <td>" +
                    msg[i].Cat1 + "</td>  <td>" +
                    msg[i].Cat2 + "</td>  <td>" +
                    msg[i].Cat3 + "</td>  <td>" +
                    msg[i].Cat4 + "</td>  <td>" +
                    msg[i].Cat5 + "</td>  <td>" +
                    msg[i].Cat6 + "</td>  <td>" +
                    msg[i].Cat7 + "</td>  <td>" +
                    msg[i].Cat8 + "</td>  <td>" +
                    msg[i].Item_remarks+ "</td>  <td>" +
                    //"<label id='lblFile" + counter + "'></label>" +
                    "<input type='hidden' id='hdnCust" + counter + "' value='" + msg[i].Cust_number + "'/>" +
                    "<input type='hidden' id='hdnCPCust" + counter + "' value='" + msg[i].CPCust_number + "'/>" +
                    "<input type='hidden' id='hdnQty" + counter + "' value='" + msg[i].Qty + "'/>" +
                    "<input type='hidden' id='hdnCatID" + counter + "' value='" + msg[i].CatID + "'/>" +
                    "<input type='hidden' id='hdnItemCode" + counter + "' value='" + msg[i].Item_Code + "'/>" +
                    "<input type='hidden' id='hdnItem" + counter + "' value='" + msg[i].Item_name + "'/>" +
                 /*   "<input type='hidden' id='hdnOfferPrice" + counter + "' value='" + msg[i].OfferPrice + "'/>" +*/
                    "<input type='hidden' id='hdnFile" + counter + "' value=''/>" +

                    "<button id='btnRemove'>Remove</button>"
                    + "</td>  <td>" +
                    '   <input type="image" id="imgbtnDel_' + counter + '" class="delbtn" src="images/delete.png" onclick="delete_row(event,this);" onclick="return false;"  style="width: 25px;">' +
                    "</td>  </tr>");
                var x = $("#body_file"),
                    y = x.clone();
                y.attr("id", "file" + counter);
                y.attr("onchange", "fileupload(this)");
                y.insertAfter("button");
              document.getElementById("btnRemove").remove();

                var fileUpload = $("#file" + counter).get(0);
                if (fileUpload != undefined) {
                    var files = fileUpload.files;
                    if (files.length > 0) {
                        var fileData = new FormData();

                        // Looping over all files and add it to FormData object  
                        for (var i = 0; i < files.length; i++) {
                            fileData.append(files[i].name, files[i]);
                        }
                        var RefID = $('#body_hdnID').val();
                        $.ajax({
                            url: 'FileUpload.ashx?RefID=' + RefID,
                            type: 'POST',
                            data: fileData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function (file) {
                                $("#hdnFile" + counter).val(file.name);
                            },
                            error: function (err) {
                                //alert(err.statusText);
                                console.log("Please upload files less than 5MB");
                            }
                        });
                    }
                }
            }
            if ($.fn.dataTable.isDataTable('#grdSplItemDetails')) {
                //$('#grdDetailedPriceSummary1').DataTable().destroy();
                //table1.destroy();
                //table1 = $('#grdDetailedPriceSummary1').DataTable({
                //    //destroy: true,
                //});
                //$('#grdSplItemDetails').DataTable().destroy();
                //$('#grdSplItemDetails').empty(); // empty in case the columns change
                //table = $('#myTable').DataTable({});
            }
            else {
                //table1.destroy();
                table1 = $('#grdSplItemDetails').DataTable({
                    paging: false,
                    "info": false,
                    "scrollX": true,
                    "searching": false,
                    "sorting": false,
                   
                    // destroy: true
                });
            }

        }


        function Deletepopup(evt, obj) {
            debugger;
            var id = obj.id;
            var item_id = id.replace("imgbtnDel_", "hdnItem");
            var item = $("#" + item_id).val().replace(/\s+/g, " ");
            if (!confirm("Do you want to delete " + item + "?")) { return false; }
            else {
                //return true;
                //$.ajax({
                //    url: 'QuoteRequest.aspx/DeleteQuote',
                //    method: 'post',
                //    datatype: 'json',
                //    data: '{"item":"' + item + '"}',
                //    contentType: "application/json; charset=utf-8",
                //    success: function (msg) {
                var $row = jQuery(obj).closest('tr');
                $row.remove();
                return true;
                //    },
                //    error: function (xhr, ajaxOptions, thrownError) {
                //        alert(xhr.responseText);
                //    }
                //});
            }
        }
        function delete_row(evt, obj) {
            debugger;
            if (Deletepopup(evt, obj)) {
                var $row = jQuery(obj).closest('tr');
                $row.remove();
                //if (parseInt($("[id*=grdPriceRequest] tr").length) <= 3) {
                //    add_new_row(0);
                //}
            }
        }

        function GetDropDownData() {
            debugger;
            var param;
            var paramList = [];

            var cust = $('#body_ddlCustomers').val();
            param = {
                customer_number: cust
            }
            paramList.push(param);
            var dataparam = "{obj:" + JSON.stringify(paramList) + "}";
            $.ajax({
                url: 'SpecialQuote.aspx/GetPaymentTerms',
                method: 'post',
                datatype: 'json',
                data: dataparam,
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                   // alert(data.d);
                    $("#body_ddlPT").empty();
                    $("#body_ddlPT").append($("<option     />").val("--Select--").text("--Select--"));
                    $.each(data.d, function () {
                        $("#body_ddlPT").append($("<option     />").val(this.PaymenTerms).text(this.PaymenTerms));
                    });

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.responseText);

                }
            });

            //$.ajax({
            //    type: "POST",
            //    url: "SpecialQuote.aspx/GetPaymenTerms",
            //    data: '{customer_number: "' + cust + '" }',
            //    contentType: "application/json; charset=utf-8",
            //    dataType: "json",
            //    success: function (data)
            //{
            //    $.each(data.d, function () {
            //        $("#body_ddlPT").append($("<option     />").val(this.PaymenTerms).text(this.PaymenTerms));
            //    });
            //},
            //failure: function () {
            //    alert("Failed!");
            //}
            /*   });*/
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div id="loader_div" class="loader_div"></div>
    <asp:ScriptManager ID="SM1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>
    <asp:UpdatePanel ID="panel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="container1">
                <div class="col-md-12">
                    <div class="mn_view_stock">
                        <h4>Special Quotes </h4>
                    </div>
                    <div class="mn_section mn_filter">
                        <div class="col-md-2 mn_view_stock">
                            <asp:Label runat="server" ID="lblRefNo" CssClass="label">Cust Reference No.</asp:Label>
                            <asp:TextBox runat="server" ID="txtRFQ" CssClass="control_dropdown">
                            </asp:TextBox>
                        </div>
                        <div class="col-md-2 mn_view_stock">
                            <asp:Label runat="server" ID="lblCType" CssClass="label">Customer Type</asp:Label>
                            <asp:DropDownList runat="server" ID="ddlCustomerType" CssClass="control_dropdown" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerType_SelectedIndexChanged">
                                <asp:ListItem Text="-- Select Type --" Value=""></asp:ListItem>
                                <asp:ListItem Text="Customers" Value="C"></asp:ListItem>
                                <asp:ListItem Text="Distributors" Value="D"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="col-md-2 mn_view_stock" id="divCustomers" visible="false" runat="server">
                            <asp:Label runat="server" ID="lblCustomers" CssClass="label"></asp:Label>
                            <asp:DropDownList runat="server" ID="ddlCustomers" CssClass="control_dropdown" Visible="false" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomers_SelectedIndexChanged"></asp:DropDownList>
                        </div>
                        <div class="col-md-2 mn_view_stock" id="divCPCustomers" visible="false" runat="server">
                            <asp:Label runat="server" ID="lblCPCustomers" CssClass="label">Customers of Distributors</asp:Label>
                            <asp:DropDownList runat="server" ID="ddlCPCustomers" CssClass="control_dropdown"></asp:DropDownList>
                        </div>
                        
                        <div class="col-md-2 mn_view_stock">
                            <asp:Label runat="server" ID="Label3" CssClass="label">Product Family</asp:Label>
                            <asp:DropDownList runat="server" ID="ddlFamily" CssClass="control_dropdown" AutoPostBack="true" OnSelectedIndexChanged="ddlFamily_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
 
                        <div class="col-md-2 col-md-2 mn_view_stock">
                            <asp:Label runat="server" ID="lblCat1" Visible="false" CssClass="label"></asp:Label>
                            <asp:HiddenField runat="server" ID="hdnCat1" />
                            <asp:DropDownList runat="server" ID="ddlCat1" CssClass="control_dropdown" Visible="false" AutoPostBack="true" OnSelectedIndexChanged="ddlCat1_SelectedIndexChanged"></asp:DropDownList>
                        </div>
                        <div class="col-md-2 mn_view_stock">
                            <asp:Label runat="server" ID="lblCat2" Visible="false" CssClass="label"></asp:Label>
                            <asp:HiddenField runat="server" ID="hdnCat2" />
                            <asp:DropDownList runat="server" ID="ddlCat2" CssClass="control_dropdown" Visible="false" AutoPostBack="true" OnSelectedIndexChanged="ddlCat2_SelectedIndexChanged"></asp:DropDownList>
                        </div>
                        <div class="col-md-2 mn_view_stock">
                            <asp:Label runat="server" ID="lblCat3" Visible="false" CssClass="label"></asp:Label>
                            <asp:HiddenField runat="server" ID="hdnCat3" />
                            <asp:DropDownList runat="server" ID="ddlCat3" CssClass="control_dropdown" Visible="false" AutoPostBack="true" OnSelectedIndexChanged="ddlCat3_SelectedIndexChanged"></asp:DropDownList>
                        </div>
                        <div class="col-md-2 mn_view_stock">
                            <asp:Label runat="server" ID="lblCat4" Visible="false" CssClass="label"></asp:Label>
                            <asp:HiddenField runat="server" ID="hdnCat4" />
                            <asp:DropDownList runat="server" ID="ddlCat4" CssClass="control_dropdown" Visible="false" AutoPostBack="true" OnSelectedIndexChanged="ddlCat4_SelectedIndexChanged"></asp:DropDownList>
                        </div>
                        <div class="col-md-2 mn_view_stock">
                            <asp:Label runat="server" ID="lblCat5" Visible="false" CssClass="label"></asp:Label>
                            <asp:HiddenField runat="server" ID="hdnCat5" />
                            <asp:DropDownList runat="server" ID="ddlCat5" CssClass="control_dropdown" Visible="false" AutoPostBack="true" OnSelectedIndexChanged="ddlCat5_SelectedIndexChanged"></asp:DropDownList>
                        </div>
                        <div class="col-md-2 mn_view_stock">
                            <asp:Label runat="server" ID="lblCat6" Visible="false" CssClass="label"></asp:Label>
                            <asp:HiddenField runat="server" ID="hdnCat6" />
                            <asp:DropDownList runat="server" ID="ddlCat6" CssClass="control_dropdown" Visible="false" AutoPostBack="true" OnSelectedIndexChanged="ddlCat6_SelectedIndexChanged"></asp:DropDownList>
                        </div>
                        <div class="col-md-2 mn_view_stock">
                            <asp:Label runat="server" ID="lblCat7" Visible="false" CssClass="label"></asp:Label>
                            <asp:HiddenField runat="server" ID="hdnCat7" />
                            <asp:DropDownList runat="server" ID="ddlCat7" CssClass="control_dropdown" Visible="false" AutoPostBack="true" OnSelectedIndexChanged="ddlCat7_SelectedIndexChanged"></asp:DropDownList>
                        </div>
                        <div class="col-md-2 mn_view_stock">
                            <asp:Label runat="server" ID="lblCat8" Visible="false" CssClass="label"></asp:Label>
                            <asp:HiddenField runat="server" ID="hdnCat8" />
                            <asp:DropDownList runat="server" ID="ddlCat8" CssClass="control_dropdown" Visible="false"></asp:DropDownList>
                        </div>
                    

                        <div class="col-md-12" style="width: 10%; float: right;">
                            <asp:HiddenField ID="hdnID" runat="server" />
                            <asp:Label ID="lblErrorMsg" runat="server" Style="color: red;"></asp:Label>
                            <asp:Button ID="btnFilter" runat="server" Text="Filter" CssClass="btn btn-success button" OnClientClick="return validateFilterFields();" OnClick="btnFilter_Click" />
                        </div>
                    </div>
                    <div class="mn_section mn_filter" id="divItem" runat="server" visible="false">
                        <div class="col-md-1 mn_view_stock" style="padding-left: 0; padding-right: 0;">

                            <div class="col-md-12 mn_view_stock" style="padding: 0; color: #3c763d; font-weight: 700;">
                                <asp:RadioButton ID="RadioButton1" Checked="true" runat="server" OnCheckedChanged="RadioButton2_CheckedChanged" AutoPostBack="true" Text="Special Item" GroupName="Itemtype" TextAlign="Left" />
                            </div>
                            <div class="col-md-12 mn_view_stock" style="padding: 0; color: #3c763d; font-weight: 700;">
                                <asp:RadioButton ID="RadioButton2" runat="server" OnCheckedChanged="RadioButton2_CheckedChanged" AutoPostBack="true" Text="Catalogue Item" GroupName="Itemtype" TextAlign="Left" />
                            </div>

                        </div>
                        <div class="col-md-2 mn_view_stock">
                            <asp:Label runat="server" ID="lblItem" CssClass="label">Special Item</asp:Label>
                            <asp:TextBox CssClass="control_dropdown" runat="server" ID="txtItem"></asp:TextBox>
                          <%--  <div runat="server" id="KITComponent">
                                <p>If KIT item, Add component</p><img alt="add" src="#" /><br />

                            </div>--%>
                            <asp:DropDownList CssClass="ddl_item" ID="ddlitem" runat="server" Style="width: 200px!important;"></asp:DropDownList>
                        </div>
                        <div class="col-md-2 mn_view_stock">
                            <asp:Label runat="server" ID="Label1" CssClass="label">Delivery Weeks</asp:Label>
                            <asp:DropDownList CssClass="control_dropdown" runat="server" ID="ddlDW"></asp:DropDownList>
                        </div>
                              <% if(Session["Manual_Flag"].ToString() =="1") { %>
                         <div class="col-md-2 mn_view_stock">
                            <asp:Label runat="server" ID="Label4" CssClass="label">MOQ</asp:Label>
                            <asp:TextBox runat="server" ID="txtMOQ" CssClass="control_dropdown" TextMode="Number"></asp:TextBox>
                        </div>
                         <% } %>
                        <div class="col-md-2 mn_view_stock">
                            <asp:Label runat="server" ID="lblOfferPrice" CssClass="label">Offer Price</asp:Label>
                            <asp:TextBox runat="server" ID="txtPrice" CssClass="control_dropdown" TextMode="Number"></asp:TextBox>
                        </div>
                        <div class="col-md-4 mn_view_stock">

                            <asp:FileUpload runat="server" ID="file" Style="border: none; margin-top: 3%;" />
                            <asp:Label ID="Label2" runat="server" Style="color: red;"></asp:Label>
                            <asp:HiddenField ID="hdnCatID" runat="server" />
                  <%--          <asp:HiddenField ID="hdnOfferPrice" runat="server" />--%>
                            <asp:HiddenField ID="hdnQty" runat="server" />
                            <asp:HiddenField ID="hdnManualFlag" runat="server" />

                        </div>
                         <div class="col-md-4 mn_view_stock">
                            <asp:Label runat="server" ID="lblIRemarks" CssClass="label">Remarks</asp:Label>
                             <asp:TextBox runat="server" TextMode="MultiLine" ID="txtItemRemarks" Rows="3" Columns="30"></asp:TextBox>
                             </div>
                        <div class="col-md-1 mn_view_stock" style="padding-left: 0; padding-top:40px;">
                            <asp:Button runat="server" Style="float: right;" ID="btnAdd" CssClass="btn btn-success button add" Text="Add Item" OnClientClick="return AddRow();" />
                        </div>
                        <div class="col-md-1 mn_view_stock" style="padding-left: 0; padding-top:40px;">

                            <a href="#mdReason" rel="modal:open">
                                <asp:Button runat="server" Visible="false" Style="float: right;" ID="btnSave" CssClass="btn btn-success button save" Text="Submit" OnClientClick="return false;" />
                            </a>

                        </div>
                    </div>
                </div>
            </div>

        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnFilter" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="ddlCat1" EventName="SelectedIndexChanged" />
        </Triggers>
    </asp:UpdatePanel>
    <div id="divTable" style="display: none;">
        <div class="col-md-12" style="padding-left: 0; float: right; padding-right: 3.5%; padding-bottom: 0.3%;">

            <a href="#mdReason" rel="modal:open">
                <asp:Button runat="server" Style="float: right; width: 8%; font-size: 14px;" ID="Button1" CssClass="btn btn-success button save" Text="Submit" OnClientClick="GetDropDownData(); return false;" />
            </a>

        </div>
        <table id="grdSplItemDetails" style="width: 50%;" class="display responsive nowrap" cellpadding="0" cellspacing="0">
            <thead style="font-weight: bold">
                <tr style="border: solid 1px #000000">
                    <td style="width:200px;">Family</td>
                    <td>Item Type</td>
                    <td>Item_Desc</td>
                    <td>Customer Type</td>
                    <td>Customer</td>
                    <td>CP Customer</td>
                    <td>Delivery Week</td>
                    <td>MOQ</td>
                    <td>Offer Price</td>
                    <td>Category1</td>
                    <td>Category2</td>
                    <td>Category3</td>
                    <td>Category4</td>
                    <td>Category5</td>
                    <td>Category6</td>
                    <td>Category7</td>
                    <td>Category8</td>
                    <td>Remarks</td>
                    <td>File Attached</td>
                    <td>Action</td>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>

   <%-- <div id="" class="modal" style="border: solid 1px #008a8a;">
        <table>
            <tr>
                <td style="text-align: right;">Component Item :  </td>
                <td>
                            <asp:DropDownList CssClass="ddl_item" ID="ddl_comp_item" runat="server" Style="width: 200px!important;"></asp:DropDownList>

                </td>
                </tr>
            <tr>
                <td style="text-align: right;"> Quantity : </td>
                <td>
                    <asp:TextBox TextMode="Nu"
                </td>
            </tr>
            </table>
        </div>--%>

    <div id="mdReason" class="modal" style="border: solid 1px #008a8a;">
        <table>
            <tr>
                <td style="text-align: right;">Payment Terms Source :  </td>
                <td>
                    <div class="col-md-12 mn_view_stock" style="padding-left: 0; padding-right: 0;">

                        <div class="col-md-3 mn_view_stock" style="padding: 0; font-weight: 700;">
                            <asp:RadioButton ID="RadioButton3" Checked="true" Enabled="false" runat="server" Text="GAL" GroupName="Source" TextAlign="Left" />
                        </div>
                        <div class="col-md-9 mn_view_stock" style="padding: 0; font-weight: 700;">
                            <asp:RadioButton ID="RadioButton4" runat="server" Enabled="false" Text="Manual" GroupName="Source" TextAlign="Left" />
                        </div>

                    </div>
                </td>
            </tr>
            <tr>
                <td style="text-align: right;">Payment Terms :  </td>
                <td>
                    <asp:DropDownList ID="ddlPT" runat="server" CssClass="control_dropdown">
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td style="text-align: right;">Order Validity :  </td>
                <td>
                    <asp:DropDownList ID="ddlOrdVal" runat="server" CssClass="control_dropdown">
                        <asp:ListItem Text="-- Select --" Value="0" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="30" Value="30"></asp:ListItem>
                        <asp:ListItem Text="60" Value="60"></asp:ListItem>
                        <asp:ListItem Text="90" Value="90"></asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <%--  <tr>
                <td style="text-align:right;">  GST applicable :  </td>
                <td> <asp:TextBox ID="txtGST"  runat="server"  CssClass="control_dropdown" ></asp:TextBox></td>
            </tr>--%>
            <tr>
                <td style="text-align: right;">Remarks :  </td>
                <td>
                    <asp:TextBox ID="txtRemarks" Rows="4" Columns="40" TextMode="MultiLine" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:FileUpload runat="server" ID="fileUpload" Style="margin-left: 20%;" /></td>
            </tr>
            <tr>
                <td>
                    <input type="button" id="btnSubmitReason" style="float: right;" class="btn btn-success button" onclick="Save();" title="Submit" value="Submit" /></td>
                <td><a href="#" rel="modal:close" style="float: right; padding: 10px;">Close</a></td>
            </tr>
        </table>


        <%--     <div class="col-md-12">
            <div class="col-md-12" style="padding: 5px;">
                Payment Terms : 
                <asp:DropDownList ID="ddlPT" runat="server" CssClass="control_dropdown" Style="width: 200px; margin-left: 10%;">
                </asp:DropDownList>
            </div>
            <div class="col-md-12" style="padding: 5px;">
                Order Validity :
                <asp:DropDownList ID="ddlOrdVal" runat="server" CssClass="control_dropdown" Style="width: 200px; margin-left: 12%;">
                    <asp:ListItem Text="30" Value="30" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="60" Value="60"></asp:ListItem>
                    <asp:ListItem Text="90" Value="90"></asp:ListItem>
                </asp:DropDownList>
            </div>
             <div class="col-md-12" style="padding: 5px;">
                GST applicable :
                <asp:TextBox ID="txtGST"  runat="server" Style="margin-left: 20%;"></asp:TextBox>
            </div>
            <div class="col-md-12" style="padding: 5px;">
                Remarks : 
                <asp:TextBox ID="txtRemarks" Rows="4" Columns="40" TextMode="MultiLine" runat="server" Style="margin-left: 20%;"></asp:TextBox>
            </div>
            <div class="col-md-12" style="padding: 5px;">
                <asp:FileUpload runat="server" ID="fileUpload" Style="margin-left: 20%;" />
            </div>
        </div>
        <div class="col-md-12">
                <input type="button" id="btnSubmitReason" style="float: right;" class="btn btn-success button" onclick="Save();" title="Submit" value="Submit" />
   

                <a href="#" rel="modal:close" style="float:right; padding:10px;">Close</a>
           
        </div>--%>
    </div>


</asp:Content>
