﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using TTDeskDAL;
using TTDeskBO;

namespace TTDeskBL
{
    public class QuoteBL
    {
        QuoteDAL objQuoteDAL = new QuoteDAL();
        public DataTable GetPendingTasksBL(QuoteBO objQuoteBO)
        {
            return objQuoteDAL.GetPendingTasksDAL(objQuoteBO);
        }

        public List<QuoteBO> updateQuoteStatusBL(DataTable dtQuote)
        {
            return objQuoteDAL.updateQuoteStatusDAL(dtQuote);
        }

        public DataTable GetQuoteSummaryBL(QuoteBO objQuoteBO)
        {
            return objQuoteDAL.GetQuoteSummaryDAL(objQuoteBO);
        }

        public DataTable GetQuoteDetailsBL(QuoteBO objquoteBO)
        {
            return objQuoteDAL.GetQuoteDetailsDAL(objquoteBO);
        }

        public DataTable getQuoteFormatBL(QuoteBO objQuoteBO)
        {
            return objQuoteDAL.getQuoteFormatDAL(objQuoteBO);
        }

       

        public DataTable GetAcceptedQuotesBL(QuoteBO objQuoteBO)
        {
            return objQuoteDAL.GetAcceptedQuotesDAL(objQuoteBO);
        }

        public DataTable GetEscalatedTasksBL(QuoteBO objQuoteBO)
        {
            return objQuoteDAL.GetEscalatedTasksDAL(objQuoteBO);
        }

        public DataTable GetQuoteStatusLogBL(QuoteBO objBO)
        {
            return objQuoteDAL.GetQuoteStatusLogDAL(objBO);
        }

        public DataTable GetAcceptedQuotesForDownloadBL(QuoteBO objQuoteBO)
        {
            return objQuoteDAL.GetAcceptedQuotesForDownloadDAL(objQuoteBO);
        }


        public DataTable GetEscalatedQuotesBL(QuoteBO objQuoteBO)
        {
            return objQuoteDAL.GetEscalatedQuotesDAL(objQuoteBO);
        }

        public DataTable GetSplQuoteDropdownsBL(SplQuoteBO objQuoteBO)
        {
            return objQuoteDAL.GetSplQuoteDropdownsDAL(objQuoteBO);
        }

        public DataTable GetCustomerDetailsBL(SplQuoteBO objQuoteBO)
        {
            return objQuoteDAL.GetCustomerDetailsDAL(objQuoteBO);
        }

        public DataTable GetCPCustomerDetailsBL(SplQuoteBO objQuoteBO)
        {
            return objQuoteDAL.GetCPCustomerDetailsDAL(objQuoteBO);
        }
        public DataTable GetItemDetailsBL(SplQuoteBO objQuoteBO)
        {
            return objQuoteDAL.GetItemDetailsDAL(objQuoteBO);
        }

        public SplQuoteBO SaveSplQuotesBL(DataTable dt)
        {
            return objQuoteDAL.SaveSplQuotesDAL(dt);
        }
        public DataTable GetOfferPriceSplItemBL(SplQuoteBO objQuoteBO)
        {
            return objQuoteDAL.GetOfferPriceSplItemDAL(objQuoteBO);
        }

        public int GetMaxQuoteIDBL()
        {
            return objQuoteDAL.GetMaxQuoteIDDAL();
        }
        public DataTable GetDeliveryWeekBL()
        {
            return objQuoteDAL.GetDeliveryWeekDAL();
        }
        public DataTable GetPaymentTermsBL(SplQuoteBO objSplQuoteBO)
        {
            return objQuoteDAL.GetPaymentTermsDAL(objSplQuoteBO);
        }
        public DataTable GetFamilyBL()
        {
            return objQuoteDAL.GetFamilyDAL();
        }
        public DataTable GetSPLQuotesBL(SplQuoteBO objQuoteBO)
        {
            return objQuoteDAL.GetSPLQuotesDAL(objQuoteBO);
        }
        public static DataTable GetSPLItemDetailsBL(SplQuoteBO objQuoteBO)
        {
            return QuoteDAL.GetSPLItemDetailsDAL(objQuoteBO);
        }

        public DataTable GetItemDescBL(SplQuoteBO objSplQuoteBO)
        {
            return objQuoteDAL.GetItemDescDAL(objSplQuoteBO);
        }


        public DataTable GetIndustryListBL()
        {
            return objQuoteDAL.GetIndustryListDAL();
        }

        public SplQuoteBO SaveProjectQuotesBL(DataTable dt, string industry)
        {
            return objQuoteDAL.SaveProjectQuotesDAL(dt, industry);
        }

        public int GetProjMaxQuoteIDBL()
        {
            return objQuoteDAL.GetProjMaxQuoteIDDAL();
        }
    }
}