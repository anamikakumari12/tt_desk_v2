﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using TTDeskBO;
using TTDeskDAL;
namespace TTDeskBL
{
    public class LoginBL
    {
        LoginDAL objLoginDAL = new LoginDAL();
        public LoginBO deskLoginBL(LoginBO objLoginBO)
        {
            return objLoginDAL.deskLoginDAL(objLoginBO);
        }

        public ResetBO deskResetBL(ResetBO objResetBO)
        {
            return objLoginDAL.changeOrresetpwd(objResetBO);
        }

        public object GetConfiguredValue(string v)
        {
            return objLoginDAL.GetConfiguredValueDAL(v);
        }

        public DataSet GetAllMenusBL(LoginBO objLoginBO)
        {
            return objLoginDAL.GetAllMenusDAL(objLoginBO);
        }

        //public ResetBO ChangePasswordBL(ResetBO objLoginBO)
        //{
        //    return objLoginDAL.ChangePasswordDAL(objLoginBO);
        //}
    }
}