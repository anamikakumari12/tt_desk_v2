﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TTDeskBO
{
    public class LoginBO
    {
        public string emailid { get; set; }
        public string password { get; set; }
        public string FullName { get; set; }
        public string UserId { get; set; }
        public int Error_code { get; set; }
        public string Error_msg { get; set; }
        public string Menu_URL { get; set; }
    }

    public class ResetBO
    {
        public string Engid { get; set; }
        public string Newpassword { get; set; }
        public string OldPassword { get; set; }
        public int Error_code { get; set; }
        public string Error_msg { get; set; }
    }
    public class AuthClass
    {
        public int ErrorNum { get; set; }
        public string LoginMailID { get; set; }
    }
}