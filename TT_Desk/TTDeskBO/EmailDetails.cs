﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TTDeskBO
{
    public class EmailDetails
    {
        public string toMailId { get; set; }
        public string ccMailId { get; set; }
        public string subject { get; set; }
        public string body { get; set; }
        public string attachment { get; set; }
    }
}