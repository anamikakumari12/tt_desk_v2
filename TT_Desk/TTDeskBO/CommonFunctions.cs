﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Net.Mail;
using System.Data.SqlClient;
using System.Data;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Threading.Tasks;

namespace TTDeskBO
{
    public class CommonFunctions
    {
        public static void StaticLog(string ex)
        {
            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += string.Format("Message: {0}", ex);
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;


            StreamWriter log;
            string ErrorLogFile = ConfigurationManager.AppSettings["ErrorLogFile"].ToString();
            string ErrorFile = ConfigurationManager.AppSettings["ErrorFile"].ToString();
            if (!File.Exists(ErrorFile))
            {
                log = new StreamWriter(ErrorLogFile, true);
            }
            else
            {
                log = File.AppendText(ErrorLogFile);
            }

            //Write to the file:
            log.WriteLine("Date Time:" + DateTime.Now.ToString());
            log.WriteLine(message);
            // Close the stream:
            log.Close();
        }

        public static void StaticErrorLog(Exception ex)
        {
            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += string.Format("Message: {0}", ex.Message);
            message += Environment.NewLine;
            message += string.Format("StackTrace: {0}", ex.StackTrace);
            message += Environment.NewLine;
            message += string.Format("Source: {0}", ex.Source);
            message += Environment.NewLine;
            message += string.Format("TargetSite: {0}", ex.TargetSite.ToString());
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;


            StreamWriter log;
            string ErrorLogFile = ConfigurationManager.AppSettings["ErrorLogFile"].ToString();
            string ErrorFile = ConfigurationManager.AppSettings["ErrorFile"].ToString();
            if (!File.Exists(ErrorFile))
            {
                log = new StreamWriter(ErrorLogFile, true);
            }
            else
            {
                log = File.AppendText(ErrorLogFile);
            }

            //Write to the file:
            log.WriteLine("Date Time:" + DateTime.Now.ToString());
            log.WriteLine(message);
            // Close the stream:
            log.Close();
        }
        /// <summary>
        /// Author : Anamika Kumari
        /// Date : Nov 2, 2020
        /// Desc : Store the exception message in a file 
        /// </summary>
        /// <param name="ex">Exception</param>
        public void ErrorLog(Exception ex)
        {
            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += string.Format("Message: {0}", ex.Message);
            message += Environment.NewLine;
            message += string.Format("StackTrace: {0}", ex.StackTrace);
            message += Environment.NewLine;
            message += string.Format("Source: {0}", ex.Source);
            message += Environment.NewLine;
            message += string.Format("TargetSite: {0}", ex.TargetSite.ToString());
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;


            StreamWriter log;
            string ErrorLogFile = ConfigurationManager.AppSettings["ErrorLogFile"].ToString();
            string ErrorFile = ConfigurationManager.AppSettings["ErrorFile"].ToString();
            if (!File.Exists(ErrorFile))
            {
                log = new StreamWriter(ErrorLogFile, true);
            }
            else
            {
                log = File.AppendText(ErrorLogFile);
            }

            //Write to the file:
            log.WriteLine("Date Time:" + DateTime.Now.ToString());
            log.WriteLine(message);
            // Close the stream:
            log.Close();
        }

        /// <summary>
        /// Author : Anamika Kumari
        /// Date : Nov 2, 2020
        /// Desc : Encrypt the text with AES algorithm using encription key
        /// </summary>
        /// <param name="clearText"></param>
        /// <returns></returns>
        public string Encrypt(string clearText)
        {
            try
            {
                string EncryptionKey = "MAKV2SPBNI99212";
                byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(clearBytes, 0, clearBytes.Length);
                            cs.Close();
                        }
                        clearText = Convert.ToBase64String(ms.ToArray());
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            return clearText;
        }

        /// <summary>
        /// Author : Anamika Kumari
        /// Date : Nov 2, 2020
        /// Desc : The Method is used to send mail using SMTP server with detail stored in web config file.
        /// </summary>
        /// <param name="objEmail"></param>
        public void SendMail(EmailDetails objEmail)
        {
            string ErrorMessage = string.Empty;
            string DestinationEmail = string.Empty;
            MailMessage email;
            SmtpClient smtpc;
            try
            {
                email = new MailMessage();
                if (objEmail.toMailId.Contains(";"))
                {
                    List<string> names = objEmail.toMailId.Split(';').ToList<string>();
                    for (int i = 0; i < names.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(names[i].TrimStart().TrimEnd()))
                            email.To.Add(names[i]);
                    }
                }
                else
                {
                    email.To.Add(objEmail.toMailId);
                }

                // email.To.Add(objEmail.toMailId); //Destination Recipient e-mail address.
                if (!string.IsNullOrEmpty(objEmail.ccMailId))
                {
                    if (!string.IsNullOrEmpty(objEmail.ccMailId))
                    {
                        if (objEmail.ccMailId.Contains(";"))
                        {
                            List<string> names = objEmail.ccMailId.Split(';').ToList<string>();
                            for (int i = 0; i < names.Count; i++)
                            {
                                if (!string.IsNullOrEmpty(names[i].TrimStart().TrimEnd()))
                                    email.CC.Add(names[i]);
                            }
                        }
                        else
                        {
                            email.CC.Add(objEmail.ccMailId);
                        }
                    }
                }
                // email.CC.Add(objEmail.ccMailId);
                email.Subject = objEmail.subject;
                email.Body = objEmail.body;
                email.IsBodyHtml = true;
                if (!string.IsNullOrEmpty(objEmail.attachment))
                    if (objEmail.attachment.Contains(";"))
                    {
                        List<string> names = objEmail.attachment.Split(';').ToList<string>();
                        for (int i = 0; i < names.Count; i++)
                        {
                            if (!string.IsNullOrEmpty(names[i].TrimStart().TrimEnd()))
                                email.Attachments.Add(new System.Net.Mail.Attachment(names[i]));
                        }
                    }
                    else
                    {
                        email.Attachments.Add(new System.Net.Mail.Attachment(objEmail.attachment));
                    }
                smtpc = new SmtpClient();
                smtpc.Send(email);
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            finally
            {
                email = new MailMessage();
            }
        }

        public static async Task SendGridMail(EmailDetails email)
        {
            try
            {
              //  var sendGridClient = new SendGridClient("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");
                var sendGridClient = new SendGridClient(ConfigurationManager.AppSettings["SendGridKey"].ToString());

                var sendGridMessage = new SendGridMessage()
                {
                    From = new EmailAddress("ttilalerts@taegutec-india.com", "TT Alerts"),
                    Subject = email.subject,
                    HtmlContent = email.body
                };
                //sendGridMessage.AddTo(new EmailAddress(email.toMailId));
                List<EmailAddress> objList = new List<EmailAddress>();
                if (!string.IsNullOrEmpty(email.toMailId))
                {
                    if (email.toMailId.Contains(";"))
                    {
                        List<string> names = email.toMailId.Split(';').ToList<string>();
                        for (int i = 0; i < names.Count; i++)
                        {
                            if (!string.IsNullOrEmpty(names[i].TrimStart().TrimEnd()))
                                //email.CC.Add(names[i]);
                                objList.Add(new EmailAddress(names[i]));
                        }
                    }
                    else
                    {
                        objList.Add(new EmailAddress(email.toMailId));
                    }
                    sendGridMessage.AddTos(objList);
                }
                objList = new List<EmailAddress>();
                if (!string.IsNullOrEmpty(email.ccMailId))
                {
                    if (email.ccMailId.Contains(";"))
                    {
                        List<string> names = email.ccMailId.Split(';').ToList<string>();
                        for (int i = 0; i < names.Count; i++)
                        {
                            if (!string.IsNullOrEmpty(names[i].TrimStart().TrimEnd()))
                                //email.CC.Add(names[i]);
                                objList.Add(new EmailAddress(names[i]));
                        }
                    }
                    else
                    {
                        objList.Add(new EmailAddress(email.ccMailId));
                    }
                    sendGridMessage.AddCcs(objList);
                }
                if (!string.IsNullOrEmpty(email.attachment))
                {
                    if (email.attachment.Contains(";"))
                    {
                        List<string> attachments = email.attachment.Split(';').ToList<string>();
                        for (int i = 0; i < attachments.Count; i++)
                        {
                            if (!string.IsNullOrEmpty(attachments[i].TrimStart().TrimEnd()))
                            {
                                byte[] fileContent = null;
                                System.IO.FileStream fs = new System.IO.FileStream(attachments[i], System.IO.FileMode.Open, System.IO.FileAccess.Read);
                                System.IO.BinaryReader binaryReader = new System.IO.BinaryReader(fs);
                                long byteLength = new System.IO.FileInfo(attachments[i]).Length;
                                fileContent = binaryReader.ReadBytes((Int32)byteLength);
                                fs.Close();
                                fs.Dispose();
                                binaryReader.Close();

                                sendGridMessage.AddAttachment(new SendGrid.Helpers.Mail.Attachment()
                                {
                                    Content = Convert.ToBase64String(fileContent),
                                    Filename = Path.GetFileName(attachments[i]),
                                    Type = "application/pdf",
                                    Disposition = "attachment"
                                });
                            }
                        }
                    }
                    else
                    {
                        //byte[] byteData = Encoding.ASCII.GetBytes(File.ReadAllText(email.attachment));
                        byte[] fileContent = null;
                        System.IO.FileStream fs = new System.IO.FileStream(email.attachment, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                        System.IO.BinaryReader binaryReader = new System.IO.BinaryReader(fs);
                        long byteLength = new System.IO.FileInfo(email.attachment).Length;
                        fileContent = binaryReader.ReadBytes((Int32)byteLength);
                        fs.Close();
                        fs.Dispose();
                        binaryReader.Close();

                        sendGridMessage.AddAttachment(new SendGrid.Helpers.Mail.Attachment()
                        {
                            Content = Convert.ToBase64String(fileContent),
                            Filename = Path.GetFileName(email.attachment),
                            Type = "application/pdf",
                            Disposition = "attachment"
                        });
                        StaticLog("attachment start");
                        using (var fileStream = File.OpenRead(email.attachment))
                        {
                            StaticLog("attachment read");
                            try
                            {
                                StaticLog("attachment add start");
                                //await sendGridMessage.AddAttachmentAsync(Path.GetFileName(email.attachment), fileStream, "application/pdf", "attachment").ConfigureAwait(false);
                                //sendGridMessage.AddAttachment(fileStream, Path.GetFileName(email.attachment));
                                StaticLog("attachment add end");
                            }
                            catch (Exception ex)
                            {
                                StaticErrorLog(ex);
                            }

                        }
                        StaticLog("attachment end");
                    }
                }
                var response = await sendGridClient.SendEmailAsync(sendGridMessage).ConfigureAwait(false);
                // var response =  sendGridClient.SendEmailAsync(sendGridMessage).ConfigureAwait(false);
                //if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
                //{
                //    //Console.WriteLine("Email sent");
                //}
            }
            catch (Exception ex)
            {
                StaticErrorLog(ex);
            }
        }

        /// <summary>
        /// Author: Monika M S
        /// Date: 29th July 2020
        /// Desc: It will send the OTP to mail
        /// </summary>
        /// <returns></returns>
        public void SendOtpToMail(string StrOtp, string loginmailId)
        {
            CommonFunctions comobj = new CommonFunctions();
            string ErrorMessage = string.Empty;
            string DestinationEmail = string.Empty;
            EmailDetails objEmail;
            try
            {
                objEmail = new EmailDetails();
                objEmail.body = "<br/><br/>" + StrOtp + Convert.ToString(ConfigurationManager.AppSettings["OtpMessage"]) + "<br/><br/>";
                objEmail.toMailId = loginmailId;
                objEmail.subject = "tt_Desk Login OTP";//Subject for your request

                CommonFunctions.SendGridMail(objEmail).Wait();
            }
            catch (Exception ex)
            {
                comobj.ErrorLog(ex);
            }
            finally
            {
                objEmail = new EmailDetails();
            }
        }

        public void IntoFile(string Msg)
        {
            try
            {
                string appPath = @"C:/MannHummelLog/tt_desklog.txt";
                using (StreamWriter writer = new StreamWriter(appPath, true)) //true => Append Text
                {
                    writer.WriteLine(System.DateTime.Now + ":  " + Msg + "\r \n");
                }
            }
            catch (Exception e7)
            {
                ErrorLog(e7);
            }

        }
    }
}